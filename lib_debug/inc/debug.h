#ifndef DEBUG_H
# define DEBUG_H

#include "libft.h"
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

# define DEBUG_FILE "/tmp/debug.txt"
# define DEBUG_ALL_ON 1
# define DEBUG_ALL 1
# define D_BLTN_ON -2
# define D_BLTN -2
# define D_CL_ON -3
# define D_CL -3
# define D_P_ON -4
# define D_P -4

# define C_BLACK   "\x1b[30m"
# define C_RED     "\x1b[31m"
# define C_GREEN   "\x1b[32m"
# define C_YELLOW  "\x1b[33m"
# define C_BLUE    "\x1b[34m"
# define C_MAGENTA "\x1b[35m"
# define C_CYAN    "\x1b[36m"
# define C_WHITE   "\x1b[37m"
# define C_RESET   "\x1b[0m"


/*
** Debug
*/
int		debug_ini();
void	debug_close(int fd);
void	debug_print_str(char *str, int fd);
void	debug_print_arr(char **arr, int fd);
char	*debug_rand_str(size_t len);
void	logs(int fd, char *debug_1, char *debug_2, char *debug_3);
void	logs_array(int fd, char **array, char *debug_1);
#endif
