/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/08/08 17:16:46 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

char		*debug_rand_str(size_t length)
{
	static char charset[]	= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	char *rand_str			= NULL;
	size_t i				= 0;
	size_t key				= 0;
	size_t size_tab;

	if (length)
	{
		if (!(rand_str = (char *)malloc(sizeof(char) * (length + 1))))
			return (NULL);
		size_tab = lib_strlen(charset) - 1;
		while (i < length)
		{
			key = arc4random() % size_tab;
			rand_str[i] = charset[key];
			i++;
		}
		rand_str[length] = '\0';
	}
	return (rand_str);
}

void		debug_print_arr(char **arr, int fd)
{
	if (fd < 1)
		fd = 1;
	while (arr && *arr)
	{
		write(fd, C_RED, lib_strlen(C_RED));
		write(fd, *arr, lib_strlen(*arr));
		write(fd, C_RESET, lib_strlen(C_RESET));
		write(fd, "\n", 1);
		arr++;
	}
}

void		debug_print_str(char *str, int fd)
{
	if (fd < 1)
		fd = 1;
	if (str)
	{
		write(fd, C_YELLOW, lib_strlen(C_YELLOW));
		write(fd, str, lib_strlen(str));
		write(fd, C_RESET, lib_strlen(C_RESET));
		write(fd, "\n", 1);
	}
}

void	logs_array(int fd, char **array, char *debug_1)
{
	int	i;
	char	*debug;

	if (DEBUG_ALL == DEBUG_ALL_ON || fd == D_CL_ON || fd == D_BLTN_ON
		|| fd == D_P_ON)
	{
		if (fd <= 0)
			fd = open(DEBUG_FILE, O_CREAT | O_APPEND | O_RDWR, S_IWUSR | S_IRUSR);
		debug = lib_strjoin("[DEBUG_LOGS]: ", "{check} :");
		debug = lib_strjoinfree(debug, debug_1, 1);
		debug = lib_strjoinfree(debug, "\n", 1);
		i = -1;
		while (array[++i])
		{
			debug = lib_strjoin(debug, array[i]);
			if (i > 4 && i % 5 == 0)
				debug = lib_strjoinfree(debug, "\n", 1);
			else
				debug = lib_strjoinfree(debug, "\t", 1);
		}
		lib_putstr_fd(debug, fd);
		lib_putstr_fd("", fd);
		close(fd);
		free(debug);
	}
}

void	logs(int fd, char *debug_1, char *debug_2, char *debug_3)
{
	char	*debug;

	if (DEBUG_ALL == DEBUG_ALL_ON || fd == D_CL_ON || fd == D_BLTN_ON
		|| fd == D_P_ON)
	{
		if (fd <= 0)
			fd = open(DEBUG_FILE, O_CREAT | O_APPEND | O_RDWR, S_IWUSR | S_IRUSR);
		debug = lib_strdup("[DEBUG_LOGS]: ");
		if (fd > 0)
		{
			if (debug_1 && debug_1[0])
				debug = lib_strjoinfree(debug, debug_1, 1);
			if (debug_2 && debug_2[0])
				debug = lib_strjoinfree(debug, debug_2, 1);
			if (debug_3 && debug_3[0])
				debug = lib_strjoinfree(debug, debug_3, 1);
		}
		lib_putstr_fd(debug, fd);
		if (fd > 2 || fd < 0)
			close(fd);
		free(debug);
	}
}

int debug_ini()
{
	int fd;

	fd = open(DEBUG_FILE, O_CREAT | O_APPEND | O_RDWR, S_IWUSR | S_IRUSR);
	return (fd);
}

void	debug_close(int fd)
{	
	close(fd);
}
