/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/23 19:08:21 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

/*
** exe_shell_bltn(t_command cmd)
** -
** Search and execute a bltn commands if it found it,
**  save the return value of the function and return 1.
** Return:
** ______  0 if no correspndant builtin function was found.
** ______  1 if a builtin function was found.
*/

int			exe_shell_bltn(t_command *cmd)
{
	int			ret;
	t_builtin	fnc;

	ret = 1;
	if (!cmd || !cmd->options)
		return (0);
	fnc = get_shell_fn_bltns(cmd->sh, cmd->options[0]);
	if (fnc)
	{
		cmd_do_redir(cmd);
		ret = fnc(cmd);
		cmd->sh->exit_status = ret;
		cmd->ret = ret;
		return (1);
	}
	cmd->ret = 1;
	return (0);
}

/*
** exe_shell_cmd(t_command cmd)
** -
** Search and execute a commands and save the return value.
** Return:
** _______ what the cmd returned.
*/

int			exe_shell_cmd(t_command *cmd)
{
	int		ret;
	char	*exe;

	ret = 1;
	if (!cmd || !cmd->options)
		return (1);
	exe = lib_arr_to_str(cmd->options);
	ret = exec_cmds(cmd, NULL);
	cmd->sh->exit_status = ret;
	cmd->ret = ret;
	if (exe)
		free(exe);
	return (ret);
}

/*
** shell(t_command cmd)
** -
** Check the command operator, do the associated execution then
**  execute the bltn or command.
** return:
** _______ what the execution returned.
*/

static int	shell(t_command *cmd)
{
	int	ret;

	ret = 1;
	if (((cmd->type == OP_IF && cmd->sh->exit_status)
			|| (cmd->type == OP_ELSE && !cmd->sh->exit_status))
			&& !(cmd->type == OP_REG || cmd->heredoc))
		return (0);
	else if (cmd->right && cmd->right->type == OP_PIPE)
	{
		exec_pipe(cmd);
		return (0);
	}
	if (!(exe_shell_bltn(cmd)))
		ret = exe_shell_cmd(cmd);
	return (0);
}

/*
** shell_switch(t_command cmd)
** -
** Launch the execution of the binary branche according to priority.
** return:
** _______ true if something wron append.
*/

t_shell		*shell_switch(t_shell *sh, t_command *cmd)
{
	if (!cmd)
		return (sh);
	shell(cmd);
	if (cmd->left)
	{
		shell_switch(sh, cmd->left);
	}
	return (sh);
}

int			main(int ac, char **av)
{
	int		ret;
	t_shell *sh;

	ret = 0;
	use_shell_struct((sh = new_shell()));
	sh->ac = ac;
	sh->av = av;
	sh->ac_i = 0;
	while (sh && !ret)
	{
		sh->history = cmd_line(sh->history, "$> ", 0);
		if (!sh->history)
			sh->history = lib_arr_push_front(NULL, "exit");
		pars_parser(sh);
		sh->history_cmd_nb++;
		shell_switch(sh, sh->cmds);
		del_all_cmds(sh->cmds);
		sh->cmds = NULL;
		if (!sh)
			break ;
	}
}
