/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 16:19:20 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/02 16:22:36 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include "shell_wrap.h"
# include "cmd_line.h"
# include "operator.h"
# include "parser.h"
# include "builtin.h"

# define HISTORY sh->history

/*
** debug only
*/

t_shell	*shell_parser(t_shell *sh, char *line);

#endif
