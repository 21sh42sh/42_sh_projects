/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_tree.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 14:42:25 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 18:49:15 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef B_TREE_H
# define B_TREE_H

# include "libft.h"
# include "shell_wrap.h"

typedef int	(*t_fn_tree)(t_command *ref, t_command *item);

void		btree_insert_elem(t_command *elem, t_command **root, t_fn_tree fn);
size_t		btree_size(t_command *root);
void		b_tree_mirror(t_command *node);

#endif
