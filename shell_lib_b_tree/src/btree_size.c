/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 15:13:51 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 18:49:57 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "b_tree.h"

size_t			btree_size(t_command *root)
{
	if (!root)
		return (0);
	return (1 + btree_size(root->left) + btree_size(root->right));
}
