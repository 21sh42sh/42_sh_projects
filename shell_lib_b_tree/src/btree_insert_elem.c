/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_elem.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 14:59:32 by mafagot           #+#    #+#             */
/*   Updated: 2016/06/29 20:52:56 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "b_tree.h"

void		btree_insert_elem(t_command *elem, t_command **root, t_fn_tree fn)
{
	t_command	*tmp;
	t_command	*parent;

	if (!elem)
		return ;
	tmp = *root;
	if (!tmp || (!tmp->options && !tmp->left && !tmp->right))
	{
		*root = elem;
		return ;
	}
	while (tmp)
	{
		parent = tmp;
		tmp = (fn(tmp, elem) > 0) ? tmp->left : tmp->right;
	}
	if (fn(parent, elem) > 0)
		parent->left = elem;
	else
		parent->right = elem;
	elem->parent = parent;
	return ;
}
