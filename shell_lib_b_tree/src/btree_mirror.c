/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_mirror.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 14:52:32 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 18:47:20 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "b_tree.h"

void			b_tree_mirror(t_command *node)
{
	t_command	*tmp;

	if (!node)
		return ;
	b_tree_mirror(node->left);
	b_tree_mirror(node->right);
	tmp = node->left;
	node->left = node->right;
	node->right = tmp;
}
