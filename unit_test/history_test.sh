valf="valgrind --leak-check=yes --leak-check=full --show-leak-kinds=all"
valf=""
errors="2>&-"
#error=" "

rm /tmp/.42sh_history
$valf $1/./42sh "exit"
echo "expecting: 1"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

echo "\ntesting HIST env variable\n"
$valf $1/./42sh "env | grep HIST" a b c d "exit" $error
echo "expecting: 7"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

$valf $1/./42sh "unsetenv HIST_FILE" a b c d "exit" $error
echo "expecting: 7"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

$valf $1/./42sh "setenv HIST_SIZE=2" a b c d "exit" $error
echo "expecting: 2"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

$valf $1/./42sh "setenv HIST_FILE=./del_me" a b c d "exit" $error
echo "expecting: 7"
wc -l "./del_me" | cut -d' ' -f 8
rm ./del_me

echo "\ntesting history builtin\n"

rm /tmp/.42sh_history
$valf $1/./42sh "setenv HIST_FILE=./del_me" a b c d "history save" "echo expecting: 6" "wc -l ./del_me | cut -d ' ' -f 8" $error
rm ./del_me

rm /tmp/.42sh_history
$valf $1/./42sh "setenv HIST_FILE=./del_me" a b c d "history save" "echo expecting: 6" "wc -l ./del_me | cut -d ' ' -f 8" "history load" t "exit" $error
echo "expecting: 8"
wc -l "./del_me" | cut -d' ' -f 8
rm ./del_me

rm /tmp/.42sh_history 2>&-
$valf $1/./42sh "setenv HIST_FILE=" a b c d "history save" " " "history load" t "exit" | grep -v "not" $error
echo "expecting: no error except not a command\n"

rm /tmp/.42sh_history 2>&-
$valf $1/./42sh "unsetenv HIST_FILE" a b c d "history save" " " "history load" t "exit"
echo "expecting: no error except not a command\n"

rm /tmp/.42sh_history 2>&-
$valf $1/./42sh "unsetenv HIST_SIZE" a b c d "history save" " " "history load" t "exit"
echo "expecting: 8"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

rm /tmp/.42sh_history 2>&-
$valf $1/./42sh "setenv HIST_SIZE=0" a b c d "history save" " " "history load" t "exit" $error
echo "expecting: 1"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

rm /tmp/.42sh_history 2>&-
rm test_del_me 2>&-
rm del_me 2>&-
$valf $1/./42sh a b c d "history save del_me" "touch test_del_me" "history load test_del_me" t "exit" $error
echo "expecting: 9"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8

rm /tmp/.42sh_history 2>&-
rm test_del_me 2>&-
rm del_me 2>&-
$valf $1/./42sh a b c d "history save del_me" "echo \"toto\" > test_del_me" "history load test_del_me" "exit" $error
echo "expecting: 2"
wc -l "/tmp/.42sh_history" | cut -d' ' -f 8