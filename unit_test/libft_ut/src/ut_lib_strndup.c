#include "fn_libft.h"
#include <string.h>

char	*lib_strndup(const char *s, size_t n);

static int	ut_tmpl_lib_strndup(int fd, int id, char *msg, char *expected,
			char *str, size_t n)
{
	char	*ret;

	ret = lib_strndup(str, n);
	expected = strndup(str, n);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd("char *fn(const char *s, size_t n)", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_str(fd, 0, "str", str);
		error_para_int(fd, 1, "n", (int)n);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		lib_strdel(&ret);
		lib_strdel(&expected);
		return (1);
	}
	lib_strdel(&ret);
	lib_strdel(&expected);
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_lib_strndup(int fd)
{
	int	id = 1;
	int	error = 0;

	lib_putendl_fd("->Test: ut_lib_strndup", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_lib_strndup(fd, id++, "not handling NULL para: str", "",
			NULL, 0);
//
	error += ut_tmpl_lib_strndup(fd, id++, "not handling neg n", "",
			"toto", -24);
//
	error += ut_tmpl_lib_strndup(fd, id++, "not handling n too big", "toto",
			"toto", 24);
// normal use
	error += ut_tmpl_lib_strndup(fd, id++, "error", "toto",
			"toto", 4);
//
	error += ut_tmpl_lib_strndup(fd, id++, "error", "to",
			"toto", 2);
// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_strndup | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}