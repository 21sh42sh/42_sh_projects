#include "fn_libft.h"

char		*lib_arr_joini(char **arr, char *insert, int flag);

static int	ut_tmpl_lib_arr_joini(int fd, int id, char *msg, char *expected,
			char **arr, char *insert, int flag)
{
	char	*ret;

	ret = lib_arr_joini(arr, insert, flag);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_char_2(fd, 0, "arr", arr);
		error_para_str(fd, 0, "insert", insert);
		error_para_int(fd, 1, "flag", flag);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_lib_arr_joini(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**arr;

	lib_putendl_fd("->Test: ut_lib_arr_joini", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_lib_arr_joini(fd, id++, "not handling NULL para: all", NULL,
			NULL, NULL, 1);
//
	arr = make_arr("l1", "l2", "l3", "l4");
	error += ut_tmpl_lib_arr_joini(fd, id++, "not handling NULL para: all", "l1l2l3l4",
			arr, NULL, 1);
//
	error += ut_tmpl_lib_arr_joini(fd, id++, "not handling NULL para: all", ";",
			NULL, ";", 1);
//
// normal use
//
	arr = make_arr("l1", "l2", "l3", "l4");
	error += ut_tmpl_lib_arr_joini(fd, id++, "error", "l1;l2;l3;l4",
			arr, ";", 1);
//
	arr = make_arr("l1", "l2", "l3", "l4");
	error += ut_tmpl_lib_arr_joini(fd, id++, "error", "l1l2l3l4",
			arr, "", 1);
//
/*	arr = make_arr("l1", "l2", NULL, "l4");
	error += ut_tmpl_lib_arr_joini(fd, id++, "error", "l1l2",
			arr, "", 1);
*///
/*	arr = make_arr("l1", "l2", NULL, "l4");
	error += ut_tmpl_lib_arr_joini(fd, id++, "error", "l1;l2",
			arr, ";", 1);
*/// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_arr_joini | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}