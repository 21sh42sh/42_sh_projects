#include "fn_libft.h"

char		**lib_arr_ndup(char **arr, size_t n, int flag);

static int	ut_tmpl_lib_arr_ndup(int fd, int id, char *msg, char **expected,
			char **arr, size_t n, int flag)
{
	char	**ret;

	ret = lib_arr_ndup(arr, n, flag);
	if (arrcmp(ret, expected) || (flag && arr))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putendl_fd("char *fn(char *s, int n, int flag)", fd);
		error_margin(fd);
		error_para_char_2(fd, 0, "arr", arr);
		error_para_int(fd, 1, "n", n);
		error_margin(fd);
		error_para_char_2(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_char_2(fd, 1, "\tExpected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_lib_arr_ndup(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**expected;
	char	**arr;

	expected = lib_arr_push_back(NULL, "line1");
	expected = lib_arr_push_back(expected, "line2");
	expected = lib_arr_push_back(expected, "line3");
	lib_putendl_fd("->Test: ut_lib_arr_ndup", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_lib_arr_ndup(fd, id++, "not handling NULL para: arr", NULL,
			NULL, 1, 0);
//
	arr = make_arr("line1", "line2", "line3", "remove");
	error += ut_tmpl_lib_arr_ndup(fd, id++, "error", NULL,
			arr, -1, 0);
	lib_arr_del(arr);
// normal use
	arr = make_arr("line1", "line2", "line3", "remove");
	error += ut_tmpl_lib_arr_ndup(fd, id++, "error", expected,
			arr, 3, 0);
	lib_arr_del(arr);
//
	expected = lib_arr_push_back(expected, "line4");
//
	arr = make_arr("line1", "line2", "line3", "line4");
	error += ut_tmpl_lib_arr_ndup(fd, id++, "error", expected,
			arr, 4, 0);
	lib_arr_del(arr);
//
	arr = make_arr("line1", "line2", "line3", "line4");
	error += ut_tmpl_lib_arr_ndup(fd, id++, "error", expected,
			arr, 8, 0);
	lib_arr_del(arr);
//
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_arr_ndup | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	lib_arr_del(expected);
	return (error);
}