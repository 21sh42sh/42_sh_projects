#include "fn_libft.h"


void	executor(int fd)
{
	int ret = 0;

	lib_putstr_fd("TEST: LIBFT TEST FUNCTION", fd);lib_putendl_fd("", fd);
//
	ret += ut_lib_strins_c(fd);
	ret += ut_lib_strndup(fd);
	ret += ut_lib_arr_del_one(fd);
	ret += ut_lib_arr_ndup(fd);
	ret += ut_lib_arr_joini(fd);
	ret += ut_lib_strsplit(fd);
//
	lib_putstr_fd("DONE: LIBFT TEST FUNCTION", fd);lib_putendl_fd("", fd);
	lib_putstr_fd("ERROR: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
}
