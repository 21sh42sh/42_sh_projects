#include "fn_libft.h"

char	**lib_strsplit(char const *s, char c);

static int	ut_tmpl_lib_strsplit(int fd, int id, char *msg, char **expected,
			char const *s, char c)
{
	char **ret;

	ret = lib_strsplit(s, c);
	if (arrcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putendl_fd("char *fn(char const *s, char c)", fd);
		error_margin(fd);
		error_para_str(fd, 0, "s", (char *)s);
		error_para_char(fd, 1, "c", c);
		error_margin(fd);
		error_para_char_2(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_char_2(fd, 1, "\tExpected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_lib_strsplit(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**expected;

	lib_putendl_fd("->Test: ut_lib_strsplit", fd);
	lib_putstr(">case: ");
// parameters non-existence
	expected = NULL;
	error += ut_tmpl_lib_strsplit(fd, id++, "not handling NULL para", expected,
			NULL, ':');
// normal use
	expected = make_arr("l1", "l2", "l3", "l4");
	error += ut_tmpl_lib_strsplit(fd, id++, "error", expected,
			"l1:l2:l3:l4", ':');
	lib_arr_del(expected);
//
	expected = make_arr("l1", NULL, NULL, NULL);
	error += ut_tmpl_lib_strsplit(fd, id++, "error", expected,
			"l1", ':');
	lib_arr_del(expected);
// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_strsplit | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}