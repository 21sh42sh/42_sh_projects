#include "fn_libft.h"

char		*lib_strins_c(char *str, const char c, int pos, int flag);

static int	ut_tmpl_lib_strins_c(int fd, int id, char *msg, char *expected,
			char *str, char c, int pos, int flag)
{
	char	*ret;

	ret = lib_strins_c(str, c, pos, flag);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putendl_fd("char *fn(char *str, const char c, int pos, int flag)", fd);
		error_margin(fd);
		error_para_str(fd, 0, "str", str);
		error_para_char(fd, 0, "c", c);
		error_para_int(fd, 0, "pos", pos);
		error_para_int(fd, 1, "flag", flag);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_lib_strins_c(int fd)
{
	int	id = 1;
	int	error = 0;

	lib_putendl_fd("->Test: ut_lib_strins_c", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_lib_strins_c(fd, id++, "not handling NULL para: str", "a",
			NULL, 'a', 2, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "not handling NULL para: str and falg at 1", "a",
			NULL, 'a', 2, 1);
// normal use
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "toto",
			"tto", 'o', 1, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "toto",
			"tot", 'o', 3, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "toto",
			"too", 't', 2, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "toto",
			"tot", 'o', -1, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "toto",
			"tot", 'o', 80, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "anticonstitutionnellement",
			"anticonstitutionnelement", 'l', 19, 0);
//
	error += ut_tmpl_lib_strins_c(fd, id++, "error", "anticonstitutionnellement",
			"anticonstitutionnellemet", 'n', 23, 0);
// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_strins_c | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}