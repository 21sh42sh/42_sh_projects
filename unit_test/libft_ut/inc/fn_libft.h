#ifndef TEST_CMD_LINE_H
# define TEST_CMD_LINE_H
# include "libft.h"
# include <unistd.h>


// PROTOTYPE

void	executor(int fd);

/*
** ut_tools
*/

void	error_margin(int fd);
void	print_int_tab(int fd, int* ret, int size);
void	raise_error(int fd, char *msg);
void	raise_error_2(int fd, char *msg, int ret);
void	raise_error_int_tab(int fd, char *msg, int* ret, int size);

void	error_para_int(int fd, int bn, char *name, int content);
void	error_para_char(int fd, int bn, char *name, char content);
void	error_para_str(int fd, int bn, char *name, char *content);
void	error_para_char_2(int fd, int bn, char *name, char **content);


char	**make_arr(char *line1, char *line2, char *line3, char *line4);
int		arrcmp(char **ref, char **arr);
/*
** tests
*/

int	ut_lib_strins_c(int fd);
int	ut_lib_strndup(int fd);
int	ut_lib_arr_del_one(int fd);
int	ut_lib_arr_ndup(int fd);
int	ut_lib_arr_joini(int fd);
int	ut_lib_strsplit(int fd);

#endif