#include "ut_toolbox.h"

int arrcmp(char **ref, char **arr)
{
	int i;

	if ((!ref && arr)
			|| (ref && !arr))
		return (1);
	if (!ref && !arr)
		return (0);
	i = 0;
	while (ref && arr && ref[i] && arr[i])
	{
		if (lib_strcmp(ref[i], arr[i]))
			return (1);
		i++;
	}
	if ((!ref && arr)
			|| (ref && !arr))
		return (1);
	return (0);
}

char **make_arr(char *line1, char *line2, char *line3, char *line4)
{
	char **arr;

	arr = lib_arr_new(0);
	if (line1)
		arr = lib_arr_push_back(arr, line1);
	if (line2)
		arr = lib_arr_push_back(arr, line2);
	if (line3)
		arr = lib_arr_push_back(arr, line3);
	if (line4)
		arr = lib_arr_push_back(arr, line4);
	return (arr);
}
