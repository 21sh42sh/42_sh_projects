#include "ut_toolbox.h"

void	error_para_str(int fd, int bn, char *name, char *content)
{
	char *sanitized;

	lib_putstr_fd(name, fd);
	sanitized = lib_str_sanitize(content);
	lib_putstr_fd(sanitized, fd);
	lib_strdel(&sanitized);
	lib_putstr_fd(" ", fd);
	if (bn)
		lib_putendl_fd("", fd);
}

void	error_para_char(int fd, int bn, char *name, char content)
{
	char *str;
	char *sanitized;

	str = lib_char_to_str(content);
	lib_putstr_fd(name, fd);
	lib_putstr_fd(" = ", fd);
	sanitized = lib_str_sanitize(str);
	lib_putstr_fd(sanitized, fd);
	lib_strdel(&sanitized);
	lib_putstr_fd(" ", fd);
	if (bn)
		lib_putendl_fd("", fd);
	lib_strdel(&str);
}

void	error_para_int(int fd, int bn, char *name, int content)
{
	lib_putstr_fd(name, fd);
	lib_putstr_fd(" = ", fd);
	lib_putnbr_fd(content, fd);
	lib_putstr_fd(" ", fd);
	if (bn)
		lib_putendl_fd("", fd);
}

void	error_para_char_2(int fd, int bn, char *name, char **content)
{
	char	*sanitized;

	lib_putstr_fd(name, fd);
	lib_putstr_fd(" = {", fd);
	while (content && *content)
	{
		sanitized = lib_str_sanitize(*content);
		lib_putstr_fd(sanitized, fd);
		lib_strdel(&sanitized);
		if (*(content + 1))
			lib_putstr_fd(" # ", fd);
		content += 1;
	}
	lib_putstr_fd("} ", fd);
	if (bn)
		lib_putendl_fd("", fd);
}
