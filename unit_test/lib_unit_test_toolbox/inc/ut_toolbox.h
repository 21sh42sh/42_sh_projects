#ifndef UT_TOOLBOX_H
# define UT_TOOLBOX_H
# include <unistd.h>
# include "libft.h"

# define TAB_ON 0

// PROTOTYPE

/*
** ut_tools.c
*/

void	error_margin(int fd);
void	print_int_tab(int fd, int* ret, int size);
void	raise_error(int fd, char *msg);
void	raise_error_2(int fd, char *msg, int ret);
void	raise_error_int_tab(int fd, char *msg, int* ret, int size);

/*
** ut_tools.c => depreciated
*/

char	*char_to_str(char c);
char	*str_clean(char *str);

/*
** ut_tools_error_para.c
*/

void	error_para_int(int fd, int bn, char *name, int content);
void	error_para_char(int fd, int bn, char *name, char content);
void	error_para_str(int fd, int bn, char *name, char *content);
void	error_para_char_2(int fd, int bn, char *name, char **content);

/*
** ut_tools_arr.c
*/

int		arrcmp(char **ref, char **arr);
char	**make_arr(char *line1, char *line2, char *line3, char *line4);


/*
** ut_tools_rmrf.c
*/

int		rmrf(char *path);

#endif