#include "parser_tests.h"

static size_t	print_options(t_command *cmd, int fd)
{
	char	*print;
	size_t	print_size;

	print = lib_strdup(" [");
	if (cmd->type == OP_REG)
		print = lib_strjoinfree(print, "REG", 1);
	if (cmd->type == OP_PIPE)
		print = lib_strjoinfree(print, "PIPE", 1);
	if (cmd->type == OP_IF)
		print = lib_strjoinfree(print, "IF", 1);
	if (cmd->type == OP_ELSE)
		print = lib_strjoinfree(print, "ELSE", 1);
	print = lib_strjoinfree(print, "]", 1);
	lib_putstr_fd(print, fd);
	print_size = lib_strlen(print);
	free(print);
	return (print_size);
}

static size_t	print_fds(t_command *cmd, int fd)
{
	char	*print;
	size_t	print_size;

	print = lib_strdup(" [");
	print = lib_strjoinfree(print, lib_itoa(cmd->fd[0]), 3);
	print = lib_strjoinfree(print, "]", 1);
	print = lib_strjoinfree(print, "[", 1);
	print = lib_strjoinfree(print, lib_itoa(cmd->fd[1]), 3);
	print = lib_strjoinfree(print, "]", 1);
	print = lib_strjoinfree(print, "[", 1);
	print = lib_strjoinfree(print, lib_itoa(cmd->fd[2]), 3);
	print = lib_strjoinfree(print, "]", 1);
	lib_putstr_fd(print, fd);
	print_size = lib_strlen(print);
	free(print);
	return (print_size);
}\

/*
** 1st call: print_binary_tree(root, fd, NULL, 1)
*/
void	tool_print_binary_tree(t_command *elem, int fd, char *prompt, int flag)
{
	size_t	i;
	char	*new_prompt;
	char	*cmd;
	size_t	options_size;

	cmd = lib_arr_to_str(elem->options);
	cmd++;
	fd = 1;
	if (!prompt)
		prompt = "";
	if (flag == 0)
	{
		lib_putstr_fd("\n", fd);
		lib_putstr_fd(prompt, fd);
		lib_putstr_fd("|\n", fd);
		lib_putstr_fd(prompt, fd);
	}
	lib_putstr_fd(cmd, fd);
	options_size = 0;
	options_size = print_options(elem, fd);
	options_size += print_fds(elem, fd);
	if (elem->right)
	{
		if (elem->left)
			new_prompt = lib_strjoin(prompt, "|");
		else
			new_prompt = lib_strjoin(prompt, " ");
		i = 0;
		while (i < strlen(cmd) + options_size + 4)
		{
			new_prompt = lib_strjoin(new_prompt, " ");
			i++;
		}
		lib_putstr_fd(" --- ",fd);
		tool_print_binary_tree(elem->right, fd, new_prompt, 1);
	}
	if (elem->left)
		tool_print_binary_tree(elem->left, fd, prompt, 0);
}