#include "parser_tests.h"

static void	parser_ut_1()
{
	char	line[] = "pwd; ls -la && echo go || echo nogo";
	t_shell	*sh;

	lib_putstr("\n\nparser_ut_1: \nLine:");
	sh = new_shell();
	sh->history = lib_arr_push_front(sh->history, line);
	lib_putendl(line);
	sh = pars_parser(sh->history[0], sh);
	tool_print_binary_tree(sh->cmds, 0, NULL, 1);
	del_shell(sh);
}

static void	parser_ut_2()
{
	char	line[] = "pwd ; ls -la | grep r";
	t_shell	*sh;

	lib_putstr("\n\nparser_ut_2: \nLine:");
	sh = new_shell();
	sh->history = lib_arr_push_front(sh->history, line);
	lib_putendl(line);
	sh = pars_parser(sh->history[0], sh);
	tool_print_binary_tree(sh->cmds, 0, NULL, 1);
	del_shell(sh);
}

static void	parser_ut_3()
{
	char	line[] = "echo toto >> toto.txt";
	t_shell	*sh;

	lib_putstr("\n\nparser_ut_3: \nLine:");
	sh = new_shell();
	sh->history = lib_arr_push_front(sh->history, line);
	lib_putendl(line);
	sh = pars_parser(sh->history[0], sh);
	tool_print_binary_tree(sh->cmds, 0, NULL, 1);
	del_shell(sh);
}

int			main(int argc, char **argv)
{
	if (argc == 1 || !lib_strcmp(argv[1], "REG"))
		parser_ut_1();
	if (argc == 1 || !lib_strcmp(argv[1], "PIPE"))
		parser_ut_2();
	if (argc == 1 || !lib_strcmp(argv[1], "REDIR"))
		parser_ut_3();
	
}