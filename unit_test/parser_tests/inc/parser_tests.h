#ifndef PARSER_TEST_H
# define PARSER_TEST_H

#include "libft.h"
#include "debug.h"
#include "parser.h"
#include "shell_wrap.h"

void	tool_print_binary_tree(t_command *elem, int fd, char *prompt, int flag);

#endif
