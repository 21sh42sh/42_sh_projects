#include "test_cmd_line.h"

void	executor()
{
	char	**cmds;
	int		k;

	cmds = NULL;
	cmds = lib_arr_push_front(cmds, lib_strdup("ls"));
	cmds = lib_arr_push_front(cmds, lib_strdup("ls -a"));
	cmds = lib_arr_push_front(cmds, lib_strdup("ls -Rdp"));
	cmds = lib_arr_push_front(cmds, lib_strdup("cd"));
	cmds = lib_arr_push_front(cmds, lib_strdup("help"));
	k = 0;
	while (k++ < 20 && (cmds == NULL || lib_strcmp(cmds[0], "exit") != 0)) {
		cmds = cmd_line(cmds, "tests$> ", 1);
		lib_putstr("return: ");
		lib_putstr(lib_str_sanitize(cmds[0]));
		lib_putstr("\n");
	}
}
