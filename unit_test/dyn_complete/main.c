/*
** Compile using :
** gcc -I ../../libft/inc -L ../../libft/ -lft dyn_complete.c main.c
*/

#include <dyn_complete.h>
#include <stdio.h>
#include <libft.h>

int main(int argc, char *argv[])
{
	char **ret;
	if (argc == 2)
	{
		ret = get_dyn_complete_arr(argv[1]);
		lib_arr_putstr((const char **)ret);
		lib_arr_del(ret);
	}
	return 0;
}