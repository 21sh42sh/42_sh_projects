//#include <shell_wrap.h>
#include <libft.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>

static int	dyn_get_dirfile_separator(char *s)
{
	int			i;
	struct stat	st;

	if ((i = lib_strlen(s)))
	{
		while (i--)
		{
			if (s[i] == '/')
			{
				s[i] = '\0';
				if (stat(s, &st) == -1)
					return (-1);
				if ((st.st_mode & S_IFDIR) != S_IFDIR || access(s, R_OK) != 0)	
					return (-1);
				s[i] = '/';
				return (i);
			}
		}
	}
	return (-1);
}

static t_list	*lst_remove_non_match(t_list *lst, char *to_complete)
{
	t_list	*beg;
	t_list	*prev;

	beg = lst;
	if (lst && to_complete && to_complete[0])
	{
		prev = NULL;
		while (lst)
		{
			if (lib_strncmp(((struct dirent *)lst->content)->d_name, to_complete, lib_strlen(to_complete)))
			{
				if (prev)
				{
					prev->next = lst->next;
					free(lst);
					lst = prev->next;
				}
				else
				{
					beg = lst->next;
					free(lst);
					lst = beg;
				}
			}
			else
			{
				prev = lst;
				lst = lst->next;				
			}
		}
	}
	return (beg);
}

static char	**lst_dirent_to_arr(t_list *lst)
{
	char	**ret;
	int 	count;
	t_list	*prev;

	ret = NULL;
	if (lst)
	{
		count = lib_lst_len(lst);
		ret = lib_arr_new(count);
		count = 0;
		while (lst)
		{
			ret[count] = lib_strdup(((struct dirent *)lst->content)->d_name);
			prev = lst;
			lst = lst->next;
			count++;
		}
	}
	return (ret);
}

static char **get_matching_entries(char *s, int separator)
{
	char			**ret;
	t_list			*lst;
	t_list			*lst_last;
	DIR 			*my_dir;
	struct dirent	*curdirent;

	ret = NULL;
	if (s && separator > 0)
	{
		s[separator] = '\0';
		if ((my_dir = opendir(s)))
		{
			lst = NULL;
			lst_last = NULL;
			while ((curdirent = readdir(my_dir)))
			{
				if (lst_last)
				{
					lst_last->next = lib_lst_create_elem(curdirent, sizeof(struct dirent));
					lst_last = lst_last->next;
				}
				else
				{
					lst = lib_lst_create_elem(curdirent, sizeof(struct dirent));
					lst_last = lst;
				}
			}
			lst = lst_remove_non_match(lst, s + separator + 1);
			ret = lst_dirent_to_arr(lst);
		}
	}
	return (ret);
}

char	**get_dyn_complete_arr(char *name)
{
	char	**ret;
	int		i;

	ret = NULL;
	if (name && (i = dyn_get_dirfile_separator(name)) != -1)
	{
		ret = get_matching_entries(name, i);
	}
	if (!ret)
		ret = lib_arr_new(0);
	return (ret);
}