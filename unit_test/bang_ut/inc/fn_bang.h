#ifndef FN_BANG
# define FN_BANG
# include "libft.h"
# include "ut_toolbox.h"
# include "bang.h"
# include <unistd.h>

// PROTOTYPE

void	executor(int fd);

/*
** tests
*/

int	ut_bang(int fd);
int	ut_bang_last_with(int fd);
int	ut_bang_last_word(int fd);
int	ut_bang_not_last_word(int fd);
int	ut_bang_get_nth(int fd);
int	ut_bang_replace(int fd);

#endif