#include "fn_bang.h"

char	*bang_last_word(char **history, char *pattern);
int		is_bang_exit_status(char *status);

static int ut_tmpl_bang_last_word(int fd, int id, char *msg, char *expected, char *status,
			char **history, char *pattern)
{
	char	*ret;

	ret = bang_last_word(history, pattern);
	if (1 == 0 || (lib_strcmp(ret, expected) || !is_bang_exit_status(status)))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_char_2(fd, 1, "history", history);
		error_margin(fd);
		error_para_str(fd, 1, "pattern", pattern);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_para_str(fd, 1, "\tReturned sglt: ", print_status(bang_return_status(0, NULL)));
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		error_para_str(fd, 1, "\tExpected sglt: ", status);
		lib_strdel(&ret);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", 0);
	lib_strdel(&ret);
	return (0);
}

static char	**test_history(char *line1)
{
	char **history = NULL;

	history = lib_arr_push_back(history, "curr cmd not to be returned");
	history = lib_arr_push_back(history, line1);
	history = lib_arr_push_back(history, "line2");
	history = lib_arr_push_back(history, "line3");
	history = lib_arr_push_back(history, "line4");
	history = lib_arr_push_back(history, "line5");
	history = lib_arr_push_back(history, "line6");
	return (history);
}

int	ut_bang_last_word(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**history;

	lib_putendl_fd("->Test: ut_tmpl_bang_last_word", fd);
	lib_putstr(">case: ");
	history = test_history("line1 word_to_get");
// parameters non-existence
	error += ut_tmpl_bang_last_word(fd, id++, "not handling NULL para: all", NULL, "error",
			NULL, NULL);
//
	error += ut_tmpl_bang_last_word(fd, id++, "not handling NULL para: history", NULL, "error",
			history, NULL);
//
	error += ut_tmpl_bang_last_word(fd, id++, "not handling NULL para: pattern", NULL, "error",
			NULL, "pattern");
//
	error += ut_tmpl_bang_last_word(fd, id++, "not handling wrong format: pattern", NULL, "error",
			history, "!lin");
//
	error += ut_tmpl_bang_last_word(fd, id++, "not handling wrong format: pattern", NULL, "error",
			history, "!");
//
	lib_arr_del(history);
//
// normal use
//
	history = test_history("line1");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "line1", "run",
			history, "!$");
	lib_arr_del(history);
//
	history = test_history("line1 word_to_get");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "word_to_get", "run",
			history, "!$");
	lib_arr_del(history);
//
	history = test_history("line1 word_to_get");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "word_to_get", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("bad_word bad_word bad_word bad_word word_to_get");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "word_to_get", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \" word to      get\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\" word to      get\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 ` word to      get`");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "` word to      get`", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 `\"\' word to      get\'\"`");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "`\"\' word to      get\'\"`", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \'");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\' word to get \'", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \'; ls");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "ls", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'; ls\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'; ls\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \';       ls         \"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \';       ls         \"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \'&& ls");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "ls", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'&& ls\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'&& ls\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'&&       ls         \"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'&&       ls         \"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \'||ls");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "ls", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'|| ls\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'|| ls\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'||       ls         \"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'||       ls         \"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \'|ls");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "ls", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'| ls\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'| ls\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'|       ls         \"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'|       ls         \"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \' word to get \';ls");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "ls", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \'; ls\"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \'; ls\"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	history = test_history("line1 \"\' word to get \';       ls         \"");
	error += ut_tmpl_bang_last_word(fd, id++, "error", "\"\' word to get \';       ls         \"", "print",
			history, "!$:p");
	lib_arr_del(history);
//
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_tmpl_bang_last_word | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}