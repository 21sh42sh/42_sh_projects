#include "fn_bang.h"

char	*bang(char **history, char *pattern);

static int ut_tmpl_bang(int fd, int id, char *msg, char *expected,
			char **history, char *pattern)
{
	char	*ret;

	ret = bang(history, pattern);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_char_2(fd, 0, "history", history);
		error_para_str(fd, 1, "pattern", pattern);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		lib_strdel(&ret);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", 0);
	lib_strdel(&ret);
	return (0);
}

static char	**test_history()
{
	char **history = NULL;

	history = lib_arr_push_back(history, "ligne1");
	history = lib_arr_push_back(history, "ligne2");
	history = lib_arr_push_back(history, "ligne3");
	history = lib_arr_push_back(history, "ligne4");
	history = lib_arr_push_back(history, "ligne5");
	history = lib_arr_push_back(history, "ligne6");
	return (history);
}

int	ut_bang(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**history;

	lib_putendl_fd("->Test: ut_ut_tmpl_bang", fd);
	lib_putstr(">case: ");
	history = test_history();
// parameters non-existence
	error += ut_tmpl_bang(fd, id++, "not handling NULL para: all", NULL,
			NULL, NULL);
//
	error += ut_tmpl_bang(fd, id++, "not handling NULL para: history", NULL,
			history, NULL);
//
	error += ut_tmpl_bang(fd, id++, "not handling NULL para: pattern", NULL,
			NULL, "pattern");
//
// normal use
//
	error += ut_tmpl_bang(fd, id++, "not handling NULL para: pattern", "undefined yet",
			history, "pattern");
//
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_strins_c | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}