#include "fn_bang.h"

void	executor(int fd)
{
	int ret = 0;

	lib_putstr_fd("TEST: BUILTIN TEST FUNCTION", fd);lib_putendl_fd("", fd);
//
//	ret += ut_bang(fd);
	//ret += ut_bang_replace(fd);
	ret += ut_bang_last_with(fd);
	ret += ut_bang_last_word(fd);
	ret += ut_bang_not_last_word(fd);
	ret += ut_bang_get_nth(fd);
	ret += ut_bang_replace(fd);
//
	lib_putstr_fd("DONE: BUILTIN TEST FUNCTION", fd);lib_putendl_fd("", fd);
	lib_putstr_fd("ERROR: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
}
