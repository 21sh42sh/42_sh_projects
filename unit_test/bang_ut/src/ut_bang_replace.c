#include <fn_bang.h>
#include <bang.h>

static int ut_tmpl_bang_replace(int fd, int id, char *msg, char *expected,
			char **history, char *pattern)
{
	char	*ret;

	ret = bang(history, pattern);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_char_2(fd, 0, "history", history);
		error_para_str(fd, 1, "pattern", pattern);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		lib_strdel(&ret);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", 0);
	lib_strdel(&ret);
	return (0);
}

static char	**test_history()
{
	char **history = NULL;

	history = lib_arr_push_back(history, "line0");
	history = lib_arr_push_back(history, "line1");
	history = lib_arr_push_back(history, "line2");
	history = lib_arr_push_back(history, "line3");
	history = lib_arr_push_back(history, "line4");
	history = lib_arr_push_back(history, "line5");
	history = lib_arr_push_back(history, "line6");
	return (history);
}

int	ut_bang_replace(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**history;

	lib_putendl_fd("->Test: ut_tmpl_bang_replace", fd);
	lib_putstr(">case: ");
	history = test_history();
// parameters non-existence
	error += ut_tmpl_bang_replace(fd, id++, "not handling NULL para: all", NULL,
			NULL, NULL);
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling NULL para: history", NULL,
			history, NULL);
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling NULL para: pattern", NULL,
			NULL, "pattern");
//
// normal use
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling NULL para: pattern", NULL,
			history, "pattern");
//
//	Whole parameter replacement : same size
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling whole parameter replacement : same size replacement", "tata1", history, "^line^tata");
//
//	Whole parameter replacement : bigger replacement
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling whole parameter replacement : bigger replacement", "supertata1", history, "^line^supertata");
//
//	Whole parameter replacement : smaller replacement
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling whole parameter replacement : smaller replacement", "u1", history, "^line^u");
//
//	Partial parameter replacement : same size replacement
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling partial parameter replacement : same size replacement", "lune1", history, "^i^u");
//
//	Partial parameter replacement : bigger replacement
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling partial parameter replacement : bigger replacement", "laline1", history, "^i^ali");
//
//	Partial parameter replacement : smaller replacement
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling partial parameter replacement : smaller replacement", "la1", history, "^ine^a");
//
//	Empty parameter replacement : to be replaced empty
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling empty parameter replacement : to be replaced empty", NULL, history, "^^toto");
//
//	Empty parameter replacement : substitute empty
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling empty parameter replacement : substitute empty", "l1", history, "^ine^");
//
//	Empty parameter replacement : both empty
//
	error += ut_tmpl_bang_replace(fd, id++, "not handling empty parameter replacement : both empty", NULL, history, "^^");


	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_bang_replace | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}