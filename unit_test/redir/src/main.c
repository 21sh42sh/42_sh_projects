#include "redir_ut.h"

static void	redir_ut_1()
{
	t_shell	*sh;
	int		fd[3];

	fd[0] = 0;
	fd[1] = open("tr_sR.txt", O_CREAT | O_RDWR | O_TRUNC, 0644);
	fd[2] = 2;
	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "ls ", OP_REG, fd);
	lib_putendl("cmd: ls  > tr_sR.txt");
	exec_cmds(sh->cmds);
	close(fd[1]);
	del_shell(sh);
}

static void	redir_ut_2()
{
	t_shell	*sh;
	int		fd[3];

	fd[0] = 0;
	fd[1] = open("tr_dR.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
	fd[2] = 2;
	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "ls ", OP_REG, fd);
	lib_putendl("cmd: ls  >> tr_dR.txt");
	write(fd[1], "i represent what shouldn't have been ereased\n", 45);
	exec_cmds(sh->cmds);
	close(fd[1]);
	del_shell(sh);
}

static void	redir_ut_3()
{
	t_shell	*sh;
	int		fd[3];
	int		tmp;

	tmp = open("ressource.txt", O_CREAT | O_RDWR | O_TRUNC, 0644);
	lib_putendl_fd("hello\ntoto\nproto\nloko\nplido\nblop\nwerdsark\nhaerd\n", tmp);
	close(tmp);
	fd[0] = open("ressource.txt", O_RDONLY);
	fd[1] = 1;
	fd[2] = 2;
	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "grep r ", OP_REG, fd);
	lib_putendl("cmd: grep r < ressource.txt");
	exec_cmds(sh->cmds);
	close(fd[1]);
	del_shell(sh);
}

int	main(int ac, char **av)
{
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "sR"))
		)
		lib_putendl("Test: redir_ut_1:");
		redir_ut_1();
		lib_putendl("\n");
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "dR"))
		)
		lib_putendl("Test: redir_ut_2:");
		redir_ut_2();
		lib_putendl("\n");
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "sL"))
		)
		lib_putendl("Test: redir_ut_3:");
		redir_ut_3();
		lib_putendl("\n");
	return (0);
}
