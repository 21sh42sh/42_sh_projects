# include "pipe_ut.h"

t_command *new_cmd_debug(t_shell *sh, char *s, enum e_cmd_type type, int fd[3])
{
	t_command *cmd;

	cmd = new_cmd(sh);
	cmd->type = type;
	cmd->options = lib_strsplit(s, ' ');
	cmd->fd[0] = fd[0];
	cmd->fd[1] = fd[1];
	cmd->fd[2] = fd[2];
	return (cmd);
}