#include "fn_test_cmd_line.h"

char	*str_clean(char *str)
{
	return (lib_str_sanitize(str));
}

char	*char_to_str(char c)
{
	return (lib_char_to_str(c));
}

void	error_margin(int fd)
{
	lib_putstr_fd("\t", fd);
}

void	raise_error(int fd, char *msg)
{
	error_margin(fd);
	lib_putendl_fd(msg, fd);
}

void	raise_error_2(int fd, char *msg, int ret)
{
	char	*ret_str;

	ret_str = lib_itoa(ret);
	lib_putstr_fd("\t", fd);
	lib_putstr_fd(msg, fd);
	lib_putstr_fd(" Returned: ", fd);
	lib_putendl_fd(ret_str, fd);
	free(ret_str);
}

void	raise_error_int_tab(int fd, char *msg, int* ret, int size)
{
	lib_putstr_fd("\t", fd);
	lib_putstr_fd(msg, fd);
	lib_putstr_fd(" Returned: ", fd);
	print_int_tab(fd, ret, size);
	lib_putendl_fd("",fd);
}

void	print_int_tab(int fd, int* ret, int size)
{
	char	*ret_str;
	int		i;

	i = 0;
	while (i < size) {
		ret_str = lib_itoa(ret[i]);
		lib_putstr_fd(ret_str, fd);
		free(ret_str);
		lib_putstr_fd(" ", fd);
		i++;;
	}
}