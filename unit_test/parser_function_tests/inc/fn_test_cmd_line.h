#ifndef TEST_CMD_LINE_H
# define TEST_CMD_LINE_H
# include "libft.h"
# include "parser.h"
# include <unistd.h>

# define TAB_ON 0

// PROTOTYPE

void	executor(int fd);

/*
** ut_tools
*/

void	error_margin(int fd);
void	print_int_tab(int fd, int* ret, int size);
void	raise_error(int fd, char *msg);
void	raise_error_2(int fd, char *msg, int ret);
void	raise_error_int_tab(int fd, char *msg, int* ret, int size);

/*
** tests
*/


#endif