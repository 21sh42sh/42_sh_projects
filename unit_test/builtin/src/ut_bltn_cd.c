/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ut_bltn_cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 09:21:18 by hlequien          #+#    #+#             */
/*   Updated: 2016/08/18 18:37:12 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ut_builtin.h>
#include <shell_wrap.h>
#include <libft.h>

int *do_ut_pipes(t_shell *sh)
{
	int fd_out[2];
	int fd_err[2];
	int *fds;

	fds = NULL;
	if (sh)
	{
		if ((fds = (int *)lib_memalloc(sizeof(int) * 5)))
		{
			pipe(fd_out);
			pipe(fd_err);
			lib_memcpy(fds, fd_out, sizeof(int) * 2);
			lib_memcpy(&(fds[2]), fd_err, sizeof(int) * 2);
		}
	}
	return (fds);
}

int cd_shell_cmp(t_shell *sh1, t_shell *sh2)
{
	if (lib_strcmp(get_shell_env_by_name(sh1, "PWD"), get_shell_env_by_name(sh2, "PWD")))
		return (1);
	if (lib_strcmp(get_shell_env_by_name(sh1, "OLDPWD"), get_shell_env_by_name(sh2, "OLDPWD")))
		return (2);
	return(0);
}

char *read_full_fd(int fd)
{
	char *ret;
	char buf[4096];

	ret = NULL;
	if (fd >= 0)
	{
		ret = lib_strdup("");
		bzero(buf, 4096);
		while (read(fd, buf, 4095) > 0)
		{
			lib_strjoinfree(ret, buf, 1);
			bzero(buf, 4096);
		}
	}
	return (ret);
}

int exec_ut_bltn_cd(t_shell *sh, t_shell *expected_sh, char **expected_out, int *fds)
{
	int ret;
	int i;
	pid_t pid;
	char *str_out;
	char *str_err;

	if (sh && expected_sh && expected_out && fds)
	{
		if ((pid = fork()) > 0)
		{
			close(fds[0]);
			close(fds[2]);
			str_out = read_full_fd(fds[1]);
			str_err = read_full_fd(fds[3]);
			waitpid(pid, NULL, 0);
			ret = 0;
			if ((ret = lib_strcmp(expected_out[0], str_out)))
				lib_putendl_fd("stdout", 2);
			if ((ret += lib_strcmp(expected_out[1], str_err)))
				lib_putendl_fd("stderr", 2);
			if ((ret += cd_shell_cmp(sh, expected_sh)))
				lib_putendl_fd("shell", 2);
			return(ret);
		}
		else if (pid == 0)
		{
			close(fds[1]);
			close(fds[3]);
			dup2(fds[0], 2);
			dup2(fds[2], 2);
			bltn_cd(sh->cmds);
			_exit(-1);
		}
		i = 0;
		while (i < 4)
			close(fds[i++]);
	}
	return (-1);
}

int ut_bltn_cd(char **argv, t_shell *expected_sh, char **expected_out)
{
	t_shell sh;
	int *fds;

	if (argv && expected_sh && expected_out ? expected_out[0] && expected_out[1] : 0)
	{
		lib_bzero(&sh, sizeof(t_shell));
		init_shell(&sh);
		sh.cmds = new_cmd(&sh);
		sh.cmds->options = argv;
		fds = do_ut_pipes(&sh);
		lib_putstr_fd("bltn_cd : ", 2);
		if (exec_ut_bltn_cd(&sh, expected_sh, expected_out, fds))
			lib_putendl_fd("FAIL", 2);
		else
			lib_putendl_fd("OK", 2);
	}
	return (0);
}
