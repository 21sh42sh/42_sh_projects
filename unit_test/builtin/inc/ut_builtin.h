/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ut_builtin.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/17 10:27:45 by hlequien          #+#    #+#             */
/*   Updated: 2016/08/18 09:20:49 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UT_BUILTIN_H
#define UT_BUILTIN_H

#include <shell_wrap.h>

int tests_bltn(t_builtin *bltn, char *argv, char *res);
int ut_bltn_cd(char **argv, t_shell *expected_sh, char **expected_out);

#endif
