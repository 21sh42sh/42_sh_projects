# include "pipe_ut.h"

t_command *new_cmd_debug(t_shell *sh, char *s, enum e_cmd_type type)
{
	t_command *cmd;

	cmd = new_cmd(sh);
	cmd->type = type;
	cmd->options = lib_strsplit(s, ' ');
	return (cmd);
}