#include "pipe_ut.h"

/*
static int	pipe_ut_1()
{
	t_shell	*sh;

	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "pwd", OP_REG);
	sh->cmds->left = new_cmd_debug(sh, "echo go", OP_AND);
}

static int	pipe_ut_2()
{

}
*/
static void	pipe_ut_1()
{
	t_shell	*sh;

	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "/bin/ls /", OP_REG);
	sh->cmds->right = new_cmd_debug(sh, "/bin/cat -e", OP_PIPE);
	lib_putendl("cmd: /bin/ls / | /bin/cat -e");
	exec_pipe(sh->cmds);
	del_shell(sh);
}

static void	pipe_ut_2()
{
	t_shell	*sh;

	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "/bin/ls /", OP_REG);
	sh->cmds->right = new_cmd_debug(sh, "/usr/bin/grep r", OP_PIPE);
	sh->cmds->right->right = new_cmd_debug(sh, "/usr/bin/grep c", OP_PIPE);
	lib_putendl("cmd: /bin/ls / | /usr/bin/grep r | /usr/bin/grep c");
	exec_pipe(sh->cmds);
	del_shell(sh);
}

static void	pipe_ut_3()
{
	t_shell	*sh;

	sh = new_shell();
	sh->cmds = new_cmd_debug(sh, "/bin/ls /", OP_REG);
	sh->cmds->right = new_cmd_debug(sh, "/usr/bin/grep -r", OP_PIPE);
	sh->cmds->right->right = new_cmd_debug(sh, "/usr/bin/grep c", OP_PIPE);
	lib_putendl("cmd: /bin/ls / | /usr/bin/grep -r | /usr/bin/grep c");
	exec_pipe(sh->cmds);
	del_shell(sh);
}

int	main(int ac, char **av)
{
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "PIPE"))
		)
		pipe_ut_1();
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "PIPE_M"))
		)
		pipe_ut_2();	
	if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "PIPE_M_ERR"))
		)
		pipe_ut_3();
	/*
	else if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "AND"))
		)
		pipe_ut_1();
	else if (ac == 1
		|| (av[1] && !lib_strcmp(av[1], "OR"))
		)
		pipe_ut_2();
		*/
	return (0);
}