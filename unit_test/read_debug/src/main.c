#include "read_debug.h"

int	main(int ac, char **av)
{
	int	fd;
	char *line;

	if (ac > 1 && strcmp(av[1], "clear_logs") == 0)
		remove(DEBUG_FILE);
	fd = open(DEBUG_FILE, O_CREAT | O_RDWR);
	chmod(DEBUG_FILE, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
	lib_putstr("debugging file:");
	lib_putstr(DEBUG_FILE);
	lib_putstr("\n");
	lib_putstr("\n");
	while (1)
	{
		get_next_line(fd, &line);
		if (line)
		{
			lib_putstr(line);
			lib_putstr("\n");
		}
	}
	close (fd);
	return (0);
}
