#include "fn_builtin.h"

char	*follow_path_from(char *from, char *path);

static int	ut_tmpl_follow_path_from(int fd, int id, char *msg, char *expected,
			char *from, char	*path)
{
	char	*ret;

	ret = follow_path_from(from, path);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_str(fd, 0, "from", from);
		error_para_str(fd, 1, "path", path);
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		free(ret);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", 0);
	free(ret);
	return (0);
}

int	ut_follow_path_from(int fd)
{
	int	id = 1;
	int	error = 0;

	lib_putendl_fd("->Test: ut_follow_path_from", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_follow_path_from(fd, id++, "not handling NULL para: all", NULL,
			NULL, NULL);
//
	error += ut_tmpl_follow_path_from(fd, id++, "not handling NULL para: path", "from",
			"from", NULL);
//
	error += ut_tmpl_follow_path_from(fd, id++, "not handling NULL para: from", "to",
			NULL, "to");
//
// normal use
	error += ut_tmpl_follow_path_from(fd, id++, "error", "~/to",
			"~", "to");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/to",
			"/", "to");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/tmp/to",
			"/tmp/", "to");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/toto/to",
			"/toto/tata/titi", "../../tata/../to");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/tmp/goal",
			"/toto/tata/titi", "/tmp/goal");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/",
			"/toto/tata/titi", "/");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/toto/tata/titi/lvl1/lvl2",
			"/toto/tata/titi", "././././././././././././lvl1/lvl2/lvl3/../../././lvl2");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/Users/jsebayhi/Desktop/SH_project/dsgfhjdsfljsdhgfLJHSDgf",
			"/Users/jsebayhi/Desktop/SH_project", "dsgfhjdsfljsdhgfLJHSDgf");
//
	error += ut_tmpl_follow_path_from(fd, id++, "error", "/",
			"/", "..");
// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_lib_strins_c | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}