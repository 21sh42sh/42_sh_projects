#include "fn_builtin.h"

char		*bltn_get_options(char **av, char *opts);

static int	ut_tmpl_bltn_get_options(int fd, int id, char *msg, char *expected,
			char **av, char *opts)
{
	char	*ret;

	ret = bltn_get_options(av, opts);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putendl_fd("char *fn(char **av, char *opt)", fd);
		error_margin(fd);
		error_para_char_2(fd, 0, "av", av);
		error_para_str(fd, 0, "opts", opts);
		lib_putendl("");
		error_margin(fd);
		error_para_str(fd, 1, "\tReturned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "\tExpected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_bltn_get_options(int fd)
{
	int		id = 1;
	int		error = 0;
	char	**arr;

	lib_putendl_fd("->Test: ut_bltn_get_options", fd);
	lib_putstr(">case: ");
// parameters non-existence
	error += ut_tmpl_bltn_get_options(fd, id++, "not handling NULL para: str", NULL,
			NULL, "a");
//
	error += ut_tmpl_bltn_get_options(fd, id++, "not handling NULL para: str", NULL,
			NULL, NULL);
// normal use
	arr = make_arr("cmd", "-L", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "L");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-K", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "L");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-L", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", "LP",
			arr, "LP");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-L", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "WR");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-L", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "PR");
	arr = make_arr("cmd", "-LP", "-O", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "L");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-LP", "-T", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", NULL,
			arr, "P");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-LP", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", "LP",
			arr, "LP");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-LPK", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", "LPK",
			arr, "LPK");
	lib_arr_del(arr);
//
	arr = make_arr("cmd", "-LP", "-P", "option1");
	error += ut_tmpl_bltn_get_options(fd, id++, "error", "LP",
			arr, "LPRTGA");
	lib_arr_del(arr);
//
// test avec tab
	lib_putendl_fd("", fd);
	lib_putstr_fd("<-Done: ut_bltn_get_options | errors: ", fd);
	lib_putnbr_fd(error, fd);
	lib_putendl_fd("", fd);
	return (error != 0);
}