#include "fn_builtin.h"


void	executor(int fd)
{
	int ret = 0;

	lib_putstr_fd("TEST: BUILTIN TEST FUNCTION", fd);lib_putendl_fd("", fd);
//
//	ret += ut_bltn_get_options(fd);
	ret += ut_follow_path_from(fd);
//
	lib_putstr_fd("DONE: BUILTIN TEST FUNCTION", fd);lib_putendl_fd("", fd);
	lib_putstr_fd("ERROR: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
}
