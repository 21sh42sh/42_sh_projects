#ifndef TEST_CMD_LINE_H
# define TEST_CMD_LINE_H
# include "libft.h"
# include "cmd_line.h"
# include "ut_toolbox.h"
# include <unistd.h>

# define TAB_ON 0

// PROTOTYPE


char	*char_to_str(char c);
char	*str_clean(char *str);

void	executor(int fd);

void	ut_output(int fd);
void	ut_cursor(int fd);

/*
** tests
*/

int		ut_is_line_ender(int fd);
int		ut_get_char_nb_until_line_n(int fd);
int		ut_get_str_line_n_size(int fd);
int		ut_get_str_lines_nb(int fd);
int		ut_get_current_output(int fd);
int		ut_get_char_screen_size(int fd);
int		ut_input_len(int fd);
int		ut_get_xy_from_z(int fd);
void	ut_get_str_char_at_z(int fd);
void	ut_get_end_line(int fd);
int		ut_get_suggestion(int fd);
int		ut_get_word_at_n(int fd);

#endif