#include "fn_test_cmd_line.h"

t_int_3	get_xy_from_z(char *str, int z, int l_size, char *l_ender);

static int		ut_tmpl_get_xy_from_z(int fd, int id, char *msg, t_int_3 expected,
		char *str, int z, int l_size, char *l_ender)
{
	t_int_3	ret;

	ret = get_xy_from_z(str, z, l_size,l_ender);
	if (ret.x != expected.x || ret.y != expected.y)
	{
		lib_putstr("\n>case: ");
		lib_putnbr(id);
		lib_putendl("");
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd("; z = ", fd);lib_putnbr_fd(z, fd);
		lib_putstr_fd("; l_size = ", fd);lib_putnbr_fd(l_size, fd);
		lib_putstr_fd("; line_ender = ", fd);lib_putstr_fd(str_clean(l_ender), fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned (x, y): ", fd);lib_putnbr_fd(ret.x, fd);
		lib_putstr_fd(" ", fd);lib_putnbr_fd(ret.y, fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected (x, y): ", fd);lib_putnbr_fd(expected.x, fd);
		lib_putstr_fd(" ", fd);lib_putnbr_fd(expected.y, fd);
		lib_putendl_fd("", fd);
		lib_putstr(">case: ");
		return (1);
	}
	lib_putnbr(id);
	lib_putstr(" ");
	return (0);
}

int		ut_get_xy_from_z(int fd)
{
	int	ret = 0;
	int	id = 1;
	t_int_3 expected;

	lib_putendl_fd("->Test: ut_get_xy_from_z", fd);
	lib_putstr(">case: ");
// parameters non-existence
	expected.x = 2;expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling NULL para: str", expected,
		NULL, 2, 6, "\n");
//
	expected.x = 0;expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling wrong para: neg z", expected,
		"string", -1, 2, NULL);
//
	expected.x = 2;expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling wrong para: neg l_size", expected,
		"string", 2, -1, NULL);
//
	expected.x = 2;expected.y = 1;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling wrong para: z bigger than str size", expected,
		"line0", 8, 6, "\n");
//
	expected.x = 2;expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling NULL para: l_ender", expected,
		"string", 2, 6, NULL);
// normal use
	expected.x = 0; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0", 0, 5, "\n");
// 
	expected.x = 1; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0", 1, 5, "\n");
//
	expected.x = 0; expected.y = 1;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0", 6, 6, "\n");
//
	expected.x = 1; expected.y = 1;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0_line1_", 7, 6, "\n");
// 10
	expected.x = 1; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0_line1_", 1, 6, "\n");
//
	expected.x = 5; expected.y = 3;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0_line1_line2_line3_", 23, 6, "\n");
//
	expected.x = 0; expected.y = 4;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"line0_line1_line2_line3_line4_", 24, 6, "\n");
//
	expected.x = 0; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"\n", 0, 6, "\n");
//
	expected.x = 0; expected.y = 1;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"tests$> \n", 9, 11, "\n");
//
	expected.x = 8; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"tests$> \n", 8, 11, "\n");
//
	expected.x = 1; expected.y = 0;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"n\n", 1, 6, "\n");
//
	expected.x = 1; expected.y = 2;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"lin0\nlin1\nlin2\nlin3", 11, 6, "\n");
//
	expected.x = 4; expected.y = 2;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"lin0\nlin1\nlin2\nlin3", 14, 6, "\n");
//
	expected.x = 0; expected.y = 3;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"lin0\nlin1\nlin2\nlin3", 15, 6, "\n");
//
	expected.x = 4; expected.y = 3;
	ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
		"lin0\nlin1\nlin2\nlin3\n", 19, 6, "\n");
// test avec tab
	if (TAB_ON == 1)
	{
//
		expected.x = 6; expected.y = 0;
		ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
			"\t",6, 6, "\n");
//
		expected.x = 0; expected.y = 2;
		ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
			"\tlin2\nline3", 11, 6, "\n");
//
		expected.x = 0; expected.y = 3;
		ut_tmpl_get_xy_from_z(fd, id++, "not handling a normal case", expected,
			"\tlin1\nline2\n", 18, 6, "\n");
	}
	lib_putendl("");
	lib_putendl_fd("<-Done: ut_get_xy_from_z", fd);
	return (ret);	
}
