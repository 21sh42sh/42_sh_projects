#include "fn_test_cmd_line.h"

int		get_char_screen_size(char c);

static int	ut_tmpl_get_char_screen_size(int fd, char *msg, int expected,
		char c)
{
	int	ret = 0;

	ret = get_char_screen_size(c);
	if (ret != expected)
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: c = ", fd);lib_putstr_fd(str_clean(lib_char_to_str(c)), fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);lib_putendl_fd("", fd);
		return (1);
	}
	return (0);
}

int	ut_get_char_screen_size(int fd)
{
	int	ret = -0;

	lib_putendl_fd("->Test: ut_get_char_screen_size", fd);
// parameters non-existance
	ret += ut_tmpl_get_char_screen_size(fd, "not handling wrong negative char", 1,
			(char)(-200));
	ret += ut_tmpl_get_char_screen_size(fd, "not handling wrong positive char", 1,
			(char)(200));
// normal use tests
	ret += ut_tmpl_get_char_screen_size(fd, "not working", 1,
			'c');
//
	ret += ut_tmpl_get_char_screen_size(fd, "not working wrong", 1,
			'\n');
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_get_char_screen_size(fd, "not working", 6,
				'\t');
	}
//
	lib_putendl_fd("<-Done: ut_get_char_screen_size", fd);
	return (ret);
}