#include "fn_test_cmd_line.h"

char	*get_current_output(t_ui *ui);

static int	ut_tmpl_get_current_output(int fd, char *msg, char *expected,
				char *prompt, char *cmd, char *hs_fail, char *hs_prompt, char *hs_search, 
			int search_error, int mode)
{
	t_ui	*ui;
	char	*ret;

//test case ini
	ui = malloc(sizeof(t_ui));
	PROMPT = lib_strdup(prompt);
	ui->cmd_id = 0;
	ui->cmds = NULL;
	if (cmd) {
		ui->cmds = lib_arr_new(0);
		ui->cmds = lib_arr_push_front(ui->cmds, "not the good cmd.");
		ui->cmds = lib_arr_push_front(ui->cmds, "not the good cmd.");
		ui->cmds = lib_arr_push_front(ui->cmds, cmd);
	}
	ui->input_mode = mode;
	t_h_search_ini(&ui->h_search, 0);
	ui->h_search.error_msg = lib_strdup(hs_fail);
	ui->h_search.prompt = lib_strdup(hs_prompt);
	ui->h_search.searching = lib_strdup(hs_search);
	ui->h_search.error = search_error;
//runing test
	ret = get_current_output(ui);
	if (lib_strcmp(ret, expected))
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);lib_putstr_fd("\tcase: prompt = ", fd);lib_putendl_fd(str_clean(prompt), fd);
		error_margin(fd);lib_putstr_fd("\tcmd = ", fd);lib_putendl_fd(str_clean(cmd), fd);
		error_margin(fd);lib_putstr_fd("\ths-fail = ", fd);lib_putendl_fd(str_clean(hs_fail), fd);
		error_margin(fd);lib_putstr_fd("\ths-prompt = ", fd);lib_putendl_fd(str_clean(hs_prompt), fd);
		error_margin(fd);lib_putstr_fd("\ths-search = ", fd);lib_putendl_fd(str_clean(hs_search), fd);
		error_margin(fd);lib_putstr_fd("\tsearch_error = ", fd);lib_putnbr_fd(search_error, fd);lib_putendl_fd("", fd);
		error_margin(fd);lib_putstr_fd("\tmode = ", fd);lib_putnbr_fd(mode, fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: >", fd);lib_putstr_fd(str_clean(ret), fd);
		lib_putendl_fd("<", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: >", fd);lib_putstr_fd(str_clean(expected), fd);
		lib_putendl_fd("<", fd);
		return (1);
	}
	return (0);
}

int		ut_get_current_output(int fd)
{
	int	ret = 0;

	lib_putendl_fd("->Test: ut_get_current_output", fd);
// parameters non existence
	ret += ut_tmpl_get_current_output(fd, "not handling NULL para: prompt", "cmd\nhs-failhs-prompths-search",
				NULL, "cmd", "hs-fail", "hs-prompt", "hs-search", 1, IM_HISTORY_SEARCH);
//
	ret += ut_tmpl_get_current_output(fd, "not handling NULL para: cmd", "prompt\nhs-failhs-prompths-search",
				"prompt", NULL, "hs-fail", "hs-prompt", "hs-search", 1, IM_HISTORY_SEARCH);
//
	ret += ut_tmpl_get_current_output(fd, "not handling NULL para: hs-prompt", "promptcmd\nhs-prompths-search",
				"prompt", "cmd", NULL, "hs-prompt", "hs-search", 1, IM_HISTORY_SEARCH);
//
	ret += ut_tmpl_get_current_output(fd, "not handling NULL para: hs-fail", "promptcmd\nhs-failhs-search",
				"prompt", "cmd", "hs-fail", NULL, "hs-search", 1, IM_HISTORY_SEARCH);
//
	ret += ut_tmpl_get_current_output(fd, "not handling NULL para: hs-search", "promptcmd\nhs-failhs-prompt",
				"prompt", "cmd", "hs-fail", "hs-prompt", NULL, 1, IM_HISTORY_SEARCH);
// normal use
	ret += ut_tmpl_get_current_output(fd, "not working: ", "promptcmd",
				"prompt", "cmd", "hs-fail", "hs-prompt", "hs-search", 0, IM_DEFAULT);
//
	ut_tmpl_get_current_output(fd, "not working: ", "promptcmd\nhs-prompths-search",
				"prompt", "cmd", "hs-fail", "hs-prompt", "hs-search", 0, IM_HISTORY_SEARCH);
	//
	ret += ut_tmpl_get_current_output(fd, "not working: ", "promptcmd\nhs-failhs-prompths-search",
				"prompt", "cmd", "hs-fail", "hs-prompt", "hs-search", 1, IM_HISTORY_SEARCH);
	lib_putendl_fd("<-Done: ut_get_current_output", fd);
	return (ret);

}