#include "fn_test_cmd_line.h"

char	*get_end_line(char *str, char *line_ender, int line_size);

static int	ut_tmpl_get_end_line(int fd, char *msg, char *expected,
			char *str, char *l_ender, int l_size)
{
	char	*ret;

	ret = get_end_line(str, l_ender, l_size);
	if (lib_strcmp(ret, expected))
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd(" l_ender = ", fd);lib_putstr_fd(str_clean(l_ender), fd);
		lib_putstr_fd(" l_size = ", fd);lib_putnbr(l_size);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putendl_fd(str_clean(ret), fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putendl_fd(str_clean(expected), fd);
		return (1);
	}
	return (0);
}

void	ut_get_end_line(int	fd)
{
	lib_putendl_fd("->Test: ut_get_end_line", fd);
// parameters non-existence
	ut_tmpl_get_end_line(fd, "not handling NULL para: str", NULL,
			NULL, "\n", 6);
//
	ut_tmpl_get_end_line(fd, "not handling NULL para: l_endler", "line1_",
			"line0_line1_", NULL, 6);
//
	ut_tmpl_get_end_line(fd, "not handling wrong para: l_size", "\0",
			"line0_line_1", "\n", -3);
//
	ut_tmpl_get_end_line(fd, "not handling wrong para: l_size", "line_1",
			"line0\nline_1", "\n", -3);
//
	ut_tmpl_get_end_line(fd, "not handling wrong para: l_size and l_ender", NULL,
			"line0\nline_1", "", -3);
// normal use
	ut_tmpl_get_end_line(fd, "not working:", "line1_line2_",
			"line0_line1_line2_", "", 6);
// \n
	ut_tmpl_get_end_line(fd, "not working:", "le1\nle2\n",
			"le0\nle1\nle2\n", "\n", 6);
//
	ut_tmpl_get_end_line(fd, "not working:", "line1_le2\n",
			"le0\nline1_le2\n", "\n", 6);
//
	ut_tmpl_get_end_line(fd, "not working:", "le1\nle2\n",
			"line0_le1\nle2\n", "\n", 6);
// test avec tab
	if (TAB_ON == 1)
	{
// \t
		ut_tmpl_get_end_line(fd, "not working:", "le1\nle2\n",
				"\tle1\nle2\n", "\n", 6);
//
		ut_tmpl_get_end_line(fd, "not working:", "\t\nle2\n",
				"\t\t\nle2\n", "\n", 6);
	}
	lib_putendl_fd("<-Done: ut_get_end_line", fd);	
}