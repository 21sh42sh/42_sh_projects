#include "fn_test_cmd_line.h"

int		get_str_screen_size(char *str);

static int		ut_tmpl_get_str_screen_size(int fd, int id, char *msg, int expected,
			char *str)
{
	int	ret;

	ret = get_str_screen_size(str);
	if (ret != expected)
	{
		lib_putstr("\n>case: ");lib_putnbr(id);lib_putendl("");
		error_margin(fd);lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("str = ", fd);lib_putendl_fd(str_clean(str), fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);
		lib_putendl_fd("", fd);lib_putstr(">case: ");
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);	
}

int		ut_get_str_screen_size(int fd)
{
	int	ret = 0;
	int	id = 1;

	lib_putendl_fd("->Test: ut_get_str_screen_size", fd);
	lib_putendl_fd("-->Note: set tab size to 6 to perform the test", fd);
	lib_putstr(">case: ");
// parameters non-existence
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not handling NULL para: str", 0,
		NULL);
// normal use test
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 4,
		"toto");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 5,
		"toto\n");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 5,
		"\ntoto");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 5,
		"to\nto");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 8,
		"toto\n\n\n\n");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 8,
		"\n\n\n\ntoto");
//
	ret += ut_tmpl_get_str_screen_size(fd, id++, "not working", 8,
		"to\n\n\n\nto");
// test avec tab
	if (TAB_ON == 1)
	{
	}
	lib_putendl_fd("", fd);
	lib_putendl_fd("<-Done: ut_get_str_screen_size", fd);
	return (ret);
}