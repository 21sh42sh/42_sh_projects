#include "fn_test_cmd_line.h"

char	get_str_char_at_z(char *str, int z);

static int	ut_tmpl_get_str_char_at_z(int fd, int id, char *msg, char expected,
			char *str, int z)
{
	char	ret;

	ret = get_str_char_at_z(str, z);
	if (ret != expected)
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd(" z = ", fd);lib_putnbr(z);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);
				lib_putendl_fd(lib_str_sanitize(lib_char_to_str(ret)), fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);
				lib_putendl_fd(lib_str_sanitize(lib_char_to_str(expected)), fd);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

void	ut_get_str_char_at_z(int	fd)
{
	int	id = 1;

	lib_putendl_fd("->Test: ut_get_str_char_at_z", fd);
	lib_putstr(">case: ");
// parameters non-existence
	ut_tmpl_get_str_char_at_z(fd, id++, "not handling NULL para: str", '\0',
			NULL, 6);
//
	ut_tmpl_get_str_char_at_z(fd, id++, "not handling wrong para: z", '\0',
			"line0\nline_1", -3);
// normal use
	ut_tmpl_get_str_char_at_z(fd, id++, "not working:", 'a',
			"abcdefghijklmnopqrstuvwxyz1234567890", 0);
//
	ut_tmpl_get_str_char_at_z(fd, id++, "not working:", 'b',
			"abcdefghijklmnopqrstuvwxyz1234567890", 1);
//
	ut_tmpl_get_str_char_at_z(fd, id++, "not working:", '0',
			"abcdefghijklmnopqrstuvwxyz1234567890", 35);
//
	ut_tmpl_get_str_char_at_z(fd, id++, "not working:", '\n',
			"abcde\nghijklmnopqrstuvwxyz1234567890", 5);
// test avec tab
	if (TAB_ON == 1)
	{
//
		ut_tmpl_get_str_char_at_z(fd, id++, "not working:", '\t',
				"abcde\tghijklmnopqrstuvwxyz1234567890", 5);
//
		ut_tmpl_get_str_char_at_z(fd, id++, "not working:", '\t',
				"abcde\tghijklmnopqrstuvwxyz1234567890", 4 + get_char_screen_size('\t'));
//
		ut_tmpl_get_str_char_at_z(fd, id++, "not working:", '\t',
				"abcde\tghijklmnopqrstuvwxyz1234567890", 2 + get_char_screen_size('\t'));
}
//
	lib_putendl_fd("", fd);
	lib_putendl_fd("<-Done: ut_get_str_char_at_z", fd);	
}