#include "fn_test_cmd_line.h"


void	executor(int fd)
{
	lib_putstr_fd("TEST: CMD_LINE TEST FUNCTION : TAB : ", fd);lib_putnbr(TAB_ON);lib_putendl("");
//
	lib_putendl_fd("Test: ut_output", fd);
	ut_is_line_ender(fd);
	ut_get_char_nb_until_line_n(fd);
	ut_get_str_line_n_size(fd);
	ut_get_str_lines_nb(fd);
	ut_get_current_output(fd);
	lib_putendl_fd("Done: ut_output", fd);
//
	lib_putendl_fd("Test: ut_cursor", fd);
	ut_get_xy_from_z(fd);
	lib_putendl_fd("Done: ut_cursor", fd);
//
	//ut_get_end_line(fd);
//
	ut_input_len(fd);
	ut_get_char_screen_size(fd);
//

	ut_get_str_char_at_z(fd);

	ut_get_word_at_n(fd);

	ut_get_suggestion(fd);

	lib_putstr_fd("DONE: CMD_LINE TEST FUNCTION : TAB : ", fd);lib_putnbr(TAB_ON);lib_putendl("");
}
