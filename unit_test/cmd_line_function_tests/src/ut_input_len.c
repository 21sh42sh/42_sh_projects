#include "fn_test_cmd_line.h"

int		input_len(char *str);

static int	ut_tmpl_input_len(int fd, char *msg, int expected,
		char *str)
{
	int	ret = 0;

	ret = input_len(str);
	if (ret != expected)
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);lib_putendl_fd("", fd);
		return (1);
	}
	return (0);
}

int	ut_input_len(int fd)
{
	int	ret = -0;

	lib_putendl_fd("->Test: ut_input_len", fd);
// parameters non-existance
	ret += ut_tmpl_input_len(fd, "not handling NULL para: str", 0,
			NULL);
// normal use tests
	ret += ut_tmpl_input_len(fd, "not handling empty para", 0,
			"");
//
	ret += ut_tmpl_input_len(fd, "not working:", 6,
			"string");
//
	ret += ut_tmpl_input_len(fd, "not working:", 7,
			"string\n");
//
	ret += ut_tmpl_input_len(fd, "not working:", 7,
			"str\ning");
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_input_len(fd, "not working:", 6,
				"\t");
//
		ret += ut_tmpl_input_len(fd, "not working:", 13,
				"str\tin\ng");
	}
	lib_putendl_fd("<-Done: ut_input_len", fd);
	return (ret);
}