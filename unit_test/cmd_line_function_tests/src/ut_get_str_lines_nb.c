#include "fn_test_cmd_line.h"

int		get_str_lines_nb(char *str, char *line_ender, int line_size);

static int		ut_tmpl_get_str_lines_nb(int fd, char *msg, int expected,
			char *str, char *line_ender, int l_size)
{
	int	ret;

	ret = get_str_lines_nb(str, line_ender, l_size);
	if (ret != expected)
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd("; line_ender = ", fd);lib_putstr_fd(str_clean(line_ender), fd);
		lib_putstr_fd("; l_size = ", fd);lib_putnbr_fd(l_size, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);
		lib_putendl_fd("", fd);
		return (1);
	}
	return (0);
}

int		ut_get_str_lines_nb(int fd)
{
	int	ret = 0;

	lib_putendl_fd("->Test: ut_get_str_lines_nb", fd);
// parameters non-existence
	ret += ut_tmpl_get_str_lines_nb(fd, "not handling NULL para: str", 0,
		NULL, "\n", 3);
//
	ret += ut_tmpl_get_str_lines_nb(fd, "not handling NULL para: l_endl", 1,
		"line1_", NULL, 6);
//
	ret += ut_tmpl_get_str_lines_nb(fd, "not handling wrong para: l_size", 1,
		"line1_", "\n", -3);

// normal use test
	ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
		"line0_line1_line2_", "\n", 6);
//
	ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
		"line0\nline1_line2_", "\n", 6);
//
	ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
		"le0\nline1\nline2_", "\n", 6);
//
	ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
		"line0_line1_le2", "\n", 6);
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
			"\tline1_le2", "\n", 6);
//
		ret += ut_tmpl_get_str_lines_nb(fd, "not working", 3,
			"line0_\tle2", "\n", 6);
//
		ret += ut_tmpl_get_str_lines_nb(fd, "not working", 4,
			"line0_\tle2\n", "\n", 6);
	}
	lib_putendl_fd("<-Done: ut_get_str_lines_nb", fd);
	return (ret);
}