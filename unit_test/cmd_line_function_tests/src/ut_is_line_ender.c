#include "fn_test_cmd_line.h"

int		is_line_ender(char *line_ender, char c);

static int	ut_tmpl_is_line_ender(int fd, char *msg, int expected,
		char *line_ender, char c)
{
	int	ret;

	ret = is_line_ender(line_ender, c);
	if (ret != expected)
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: line_ender = ", fd);lib_putstr_fd(str_clean(line_ender), fd);
		lib_putstr_fd("; c = ", fd);lib_putendl_fd(str_clean(char_to_str(c)), fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);
		lib_putendl_fd("", fd);
		return (1);
	}
	return (0);
}

int	ut_is_line_ender(int fd)
{
	int		ret = 0;
	int		match = 1;
	int		no_match = 0;

	lib_putendl_fd("->Test: ut_is_line_ender", fd);
// parameters non-existence
	ret += ut_tmpl_is_line_ender(fd, "not handling NULL para: line_ender", no_match,
		NULL, 'c');
//
	ret += ut_tmpl_is_line_ender(fd, "not handling wrong para: c", no_match,
		"s", (char)(-200));
// normal use test
	ret += ut_tmpl_is_line_ender(fd, "not working properly:", no_match,
		"\n", 'c');
//
	ret += ut_tmpl_is_line_ender(fd, "not working properly:", match,
		"c", 'c');
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_is_line_ender(fd, "not working properly:", no_match,
			"\n\t", 'c');
//
		ret += ut_tmpl_is_line_ender(fd, "not working properly:", match,
			"c\n\t", 'c');
}
//
	lib_putendl_fd("<-Done: ut_is_line_ender", fd);
	return (ret);
}