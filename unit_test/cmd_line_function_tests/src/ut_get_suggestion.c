#include "fn_test_cmd_line.h"
#include <sys/stat.h>

char		**get_suggestion(char *str);

static int	ut_tmpl_get_suggestion(int id, int fd, char *msg, char **expected,
			char *str)
{
	char	**ret;

	ret = get_suggestion(str);
	if (arrcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_str(fd, 1, "str", str);
		error_margin(fd);
		error_para_char_2(fd, 1, "Returned: ", ret);
		error_margin(fd);
		error_para_char_2(fd, 1, "Expected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

static void	set_up_test()
{
	mkdir("/tmp/ut_test/", 0700);
	creat("/tmp/ut_test/file1", 0700);
	creat("/tmp/ut_test/file2", 0700);
	creat("/tmp/ut_test/asdasfaf", 0700);
	mkdir("/tmp/ut_test/dir1", 0700);
	creat("/tmp/ut_test/dir1/file1", 0700);
	creat("/tmp/ut_test/dir1/file2", 0700);
	creat("/tmp/ut_test/dir1/iuoiuorte", 0700);
	mkdir("/tmp/ut_test/dir2", 0700);
	creat("/tmp/ut_test/dir2/file1", 0700);
	creat("/tmp/ut_test/dir2/file2", 0700);
	creat("/tmp/ut_test/dir2/weqrrewrwt", 0700);
	mkdir("/tmp/ut_test/wretwetwt", 0700);
	creat("/tmp/ut_test/wretwetwt/file1", 0700);
	creat("/tmp/ut_test/wretwetwt/file2", 0700);
	creat("/tmp/ut_test/wretwetwt/iuoiuorte", 0700);
	mkdir("/tmp/ut_test/abcdefg", 0700);
	creat("/tmp/ut_test/abcdefg/file1", 0700);
	creat("/tmp/ut_test/abcdefg/file2", 0700);
	creat("/tmp/ut_test/abcdefg/iuoiuorte", 0700);
	creat("/tmp/ut_test/abcdefg/plout", 0700);
	//rmrf("/tmp/ut_test");	
}

int		ut_get_suggestion(int fd)
{
	int		id = 0;
	int		ret = 0;
	char	**expected;

	set_up_test();
	lib_putendl_fd("->Test: ut_get_suggestion", fd);
// parameters non-existence
	expected = NULL;
	expected = lib_arr_push_back(expected, "");
	ret += ut_tmpl_get_suggestion(id++, fd, "not handling NULL para: str", expected,
			NULL);
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "file1");
	expected = lib_arr_push_back(expected, "file2");
	expected = lib_arr_push_back(expected, "asdasfaf");
	expected = lib_arr_push_back(expected, "dir1/");
	expected = lib_arr_push_back(expected, "dir2/");
	expected = lib_arr_push_back(expected, "wretwetwt/");
	expected = lib_arr_push_back(expected, "abcdefg/");
	ret += ut_tmpl_get_suggestion(id++, fd, "not handling NULL para: str", expected,
			"/tmp/ut_test/");
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "ir1/");
	expected = lib_arr_push_back(expected, "ir2/");
	ret += ut_tmpl_get_suggestion(id++, fd, "error", expected,
			"/tmp/ut_test/d");
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "ir1/");
	expected = lib_arr_push_back(expected, "ir2/");
	ret += ut_tmpl_get_suggestion(id++, fd, "error", expected,
			"/tmp/ut_test/d");
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "file1");
	expected = lib_arr_push_back(expected, "file2");
	expected = lib_arr_push_back(expected, "iuoiuorte");
	ret += ut_tmpl_get_suggestion(id++, fd, "error", expected,
			"/tmp/ut_test/dir1/");
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "/");
	ret += ut_tmpl_get_suggestion(id++, fd, "error", expected,
			"/tmp/ut_test/dir1");
	lib_arr_del(expected);
//
	expected = NULL;
	expected = lib_arr_push_back(expected, "");
	ret += ut_tmpl_get_suggestion(id++, fd, "error", expected,
			"/tmp/ut_test/doesnotexist");
	lib_arr_del(expected);
//
	lib_putendl_fd("<-Done: ut_get_suggestion", fd);
	return (ret);	
}