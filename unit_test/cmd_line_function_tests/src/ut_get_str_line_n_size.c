#include "fn_test_cmd_line.h"

int		get_str_line_n_size(char *str, char *line_ender, int l_size, int n);

static int		ut_tmpl_get_str_line_n_size(int fd, int id, char *msg, int expected,
			char *str, char *line_ender, int l_size, int n)
{
	int	ret;

	ret = get_str_line_n_size(str, line_ender, l_size, n);
	if (ret != expected)
	{
		lib_putstr("\n>case: ");lib_putnbr(id);lib_putendl("");
		error_margin(fd);lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd("; line_ender = ", fd);lib_putstr_fd(str_clean(line_ender), fd);
		lib_putstr_fd("; l_size = ", fd);lib_putnbr_fd(l_size, fd);
		lib_putstr_fd("; n = ", fd);lib_putnbr_fd(n, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);
		lib_putendl_fd("", fd);lib_putstr(">case: ");
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);	
}

int		ut_get_str_line_n_size(int fd)
{
	int	ret = 0;
	int	id = 1;

	lib_putendl_fd("->Test: ut_get_str_line_n_size", fd);
	lib_putendl_fd("-->Note: set tab size to 6 to perform the test", fd);
	lib_putstr(">case: ");
// parameters non-existence
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not handling NULL para: str", 0,
		NULL, "\n", 3, 2);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not handling wrong para: l_size", 5,
		"line0", "\n", -6, 0);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not handling wrong para: n neg", 0,
		"line0", "\n", 3, -3);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not handling wrong para: n too big", 0,
		"line0", "\n", 6, 30);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not handling NULL para: l_ending", 5,
		"line0", NULL, 6, 0);
// normal use test
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
		"line0_line1_lin2_", "\n", 6, 1);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 3,
		"line0_li\nline2_", "\n", 6, 1);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
		"le0\nline1_le2\n", "\n", 6, 1);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
		"line0\nline1_le2\nline3_line4\n", "\n", 6, 3);
//
	ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 4,
		"line0\nline1_le2\nline3_line4\n", "\n", 6, 2);
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
			"\t", "\n", 6, 0);
//
		ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 5,
			"\tline1", "\n", 6, 1);
//
		ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
			"\tline1_le2\nline3_line4\n", "\n", 6, 3);
//
		ret += ut_tmpl_get_str_line_n_size(fd, id++, "not working", 6,
			"\tline1_le2\n\tline4\n", "\n", 6, 3);
	}
	lib_putendl_fd("", fd);
	lib_putendl_fd("<-Done: ut_get_str_line_n_size", fd);
	return (ret);
}