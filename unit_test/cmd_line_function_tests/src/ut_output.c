#include "fn_test_cmd_line.h"

int	ut_is_line_ender(int fd);
int	ut_get_char_nb_until_line_n(int fd);
int	ut_get_str_line_n_size(int fd);
int	ut_get_str_lines_nb(int fd);
int	ut_get_current_output(int fd);

void	ut_output(int fd)
{
	lib_putendl_fd("Test: ut_output", fd);
	
	ut_is_line_ender(fd);
	ut_get_char_nb_until_line_n(fd);
	ut_get_str_line_n_size(fd);
	ut_get_str_lines_nb(fd);
	ut_get_current_output(fd);
	
	lib_putendl_fd("Done: ut_output", fd);
}