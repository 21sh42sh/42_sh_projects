#include "fn_test_cmd_line.h"

char		*get_word_at_n(char *str, int n);

static int	ut_tmpl_get_word_at_n(int id, int fd, char *msg, char *expected,
		char *str, int n)
{
	char	*ret;

	ret = get_word_at_n(str, n);
	if (lib_strcmp(ret, expected))
	{
		lib_putstr_fd("\n>case: ", fd);lib_putnbr_fd(id, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		error_para_str(fd, 1, "str", str);
		error_margin(fd);
		error_para_int(fd, 1, "n", n);
		error_margin(fd);
		error_para_str(fd, 1, "Returned: ", ret);
		error_margin(fd);
		error_para_str(fd, 1, "Expected: ", expected);
		return (1);
	}
	lib_putnbr(id);lib_putstr_fd(" ", fd);
	return (0);
}

int	ut_get_word_at_n(int fd)
{
	int	ret = 0;
	int id;

	id = 0;
	lib_putendl_fd("->Test: ut_get_word_at_n", fd);
// parameters non-existance
	ret += ut_tmpl_get_word_at_n(id++, fd, "not handling NULL para: str", NULL,
			NULL, 2);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "not handling negative para: n", NULL,
			NULL, -10);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "not handling too big para: n", NULL,
			NULL, 5);
// normal use
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "unMot",
			"unMot", 1);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "unMot",
			"unMot", 4);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "deux",
			"deux Mot", 1);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "deux",
			"deux Mot", 4);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "deux",
			"deux Mot", 0);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "Mot",
			"deux Mot", 5);
//
	ret += ut_tmpl_get_word_at_n(id++, fd, "error", "Mot",
			"deux Mot", 6);
//
	lib_putendl_fd("<-Done: ut_get_word_at_n", fd);
	return (ret);
}