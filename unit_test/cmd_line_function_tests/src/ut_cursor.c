#include "fn_test_cmd_line.h"


int	*get_xy_from_z_(char *str, int z, int l_size, char *l_ender);

static int	ut_tmpl_get_xy_from_z_(int fd, char *msg, int *expected,
		char *str, int z, int l_size, char *l_ender)
{
	int	*ret;

	ret = malloc(sizeof(int) * 2);
	ret[0] = ret[1] = -1;
	ret = get_xy_from_z_(str, z, l_size, l_ender);
	if (ret[0] != expected[0] || ret[1] != expected[1] )
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd(" z = ", fd);lib_putnbr(z);
		lib_putstr_fd(" l_size = ", fd);lib_putnbr(l_size);
		lib_putstr_fd(" l_ender = ", fd);lib_putendl_fd(str_clean(l_ender), fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);print_int_tab(fd, ret, 2);
		lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);print_int_tab(fd, expected, 2);
		lib_putendl_fd("", fd);
		return (1);
	}
	return (0);
}

void	ut_get_xy_from_z_depreciated(int fd)
{
	int		*expected;

	expected = malloc(sizeof(int) * 2);
	lib_putendl_fd("->Test: ut_get_xy_from_z", fd);
// parameters non-existence
	expected[0] = 0;expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling NULL para: str || l_ender", expected,
		NULL, 2, 2, NULL);
//
	expected[0] = 0;expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling wrong para: neg z", expected,
		"string", -1, 2, NULL);
//
	expected[0] = 2;expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling wrong para: neg l_size", expected,
		"string", 2, -1, NULL);
//
	expected[0] = 5; expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling wrong para: z bigger than str size", expected,
		"line1", 8, -2, "\n");
//
	expected[0] = 2;expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling NULL para: l_ender", expected,
		"string", 2, 2, NULL);
// normal use
	expected[0] = 3; expected[1] = 0;
	ut_tmpl_get_xy_from_z_(fd, "not handling a normal case with l_ender = \"\"", expected,
		"line1", 3, 5, "\n");
//
	expected[0] = 1; expected[1] = 1;
	ut_tmpl_get_xy_from_z_(fd, "not handling a normal case", expected,
		"line1_toobigend", 7, 6, "\n");
//
	expected[0] = 1; expected[1] = 1;
	ut_tmpl_get_xy_from_z_(fd, "not handling a normal case", expected,
		"line1\ntoobigend", 7, 6, "\n");
//
	expected[0] = 3; expected[1] = 4;
	ut_tmpl_get_xy_from_z_(fd, "not handling a normal case", expected,
		"line1_line2\nline3\nline1_toobigend", 27, 6, "\n");
//
	expected[0] = 5; expected[1] = 1;
	ut_tmpl_get_xy_from_z_(fd, "not handling a normal case", expected,
		"line1_lin2\nline3", 11, 6, "\n");
//
	free(expected);
	lib_putendl_fd("->Done: ut_get_xy_from_z", fd);
}

void	ut_cursor(int fd)
{
	lib_putendl_fd("Test: ut_cursor", fd);
	ut_get_xy_from_z(fd);
	lib_putendl_fd("Done: ut_cursor", fd);
}
