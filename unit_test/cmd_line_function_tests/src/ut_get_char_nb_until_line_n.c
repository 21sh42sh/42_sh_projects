#include "fn_test_cmd_line.h"

int		get_char_nb_until_line_n(char *str, char *line_ender, int l_size, int n);

static int	ut_tmpl_get_char_nb_until_line_n(int fd, char *msg, int expected,
			char *str, char *l_ender, int l_size, int n)
{
	int	ret;

	ret = get_char_nb_until_line_n(str, l_ender, l_size, n);
	if (ret != expected)
	{
		error_margin(fd);
		lib_putendl_fd(msg, fd);
		error_margin(fd);
		lib_putstr_fd("\tcase: str = ", fd);lib_putstr_fd(str_clean(str), fd);
		lib_putstr_fd("; l_ender = ", fd);lib_putstr_fd(str_clean(l_ender), fd);
		lib_putstr_fd("; l_size = ", fd);lib_putnbr_fd(l_size, fd);
		lib_putstr_fd("; n = ", fd);lib_putnbr_fd(n, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tReturned: ", fd);lib_putnbr_fd(ret, fd);lib_putendl_fd("", fd);
		error_margin(fd);
		lib_putstr_fd("\tExpected: ", fd);lib_putnbr_fd(expected, fd);lib_putendl_fd("", fd);
		return (1);
	}
	return (0);	
}

int	ut_get_char_nb_until_line_n(int fd)
{
	int	ret = 0;

	lib_putendl_fd("->Test: ut_get_char_nb_until_line_n", fd);
// parameters non-existence
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling NULL and wrong para: ALL", 0,
			NULL, NULL, -1, -1);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling NULL para: str", 0,
			NULL, "\n", 2, 4);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling NULL para: l_ender", 6,
			"string", NULL, 2, 4);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling empty para: l_ender", 6,
			"string", "", 2, 4);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling NULL para: l_size", 6,
			"string", "\n", -2, 4);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not handling wrong para: n < 0", 0,
			"string", "\n", 4, -2);
// normal use test
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 4,
			"string", "\n", 4, 1);
	ret = get_char_nb_until_line_n("string", "\n", 4, 1);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 6,
			"string", "\n", 4, 2);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 4,
			"str\ning", "\n", 4, 1);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 6,
			"string", "\n", 4, 2);
//
	ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 16,
			"line0_line1\nle2\nline3_", "\n", 6, 3);
// test avec tab
	if (TAB_ON == 1)
	{
//
		ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 16,
				"\tline1\nle2\nline3_", "\n", 6, 3);
//
		ret += ut_tmpl_get_char_nb_until_line_n(fd, "not working", 13,
				"\t\t\nle2\nline3_", "\n", 6, 3);
	}
	lib_putendl_fd("<-Done: ut_get_char_nb_until_line_n", fd);
	return (ret);
}
