/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_line.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 18:15:40 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 19:31:55 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CMD_LINE_H
# define CMD_LINE_H
# include "cmd_line_struct.h"
# include "libft.h"
# include "shell_wrap.h"

/*
** define
*/
# define H_SEARCH ui->h_search
# define CURSOR_POS ui->cursor_pos
# define WIN_SIZE_NEW ui->win_size_new
# define WIN_SIZE ui->win_size
# define WIN_SIZE_PAD (WIN_SIZE.x-2)
# define PROMPT ui->prompt
# define PROMPT_LEN input_len(PROMPT)
# define CMDS ui->cmds
# define CMD_ID ui->cmd_id
# define CMD CMDS[ui->cmd_id]
# define D_FD ui->debug->fd
# define KEY ui->key
# define KEY0 ui->key.x
# define KEY1 ui->key.y
# define KEY2 ui->key.z
# define LAST_KEY ui->last_key
# define LAST_KEY0 ui->last_key.x
# define LAST_KEY1 ui->last_key.y
# define LAST_KEY2 ui->last_key.z
# define UI_AC ui->auto_comp
# define LST_DIRENT ((struct dirent *)lst->content)

/*
** Errors
*/
# define ERROR -1
# define ERROR_MALLOC -2
# define ERROR_NOT_A_TERM -3
# define ERROR_TERM -4
# define ERROR_GET_TERMIOS -5
# define ERROR_CHANGE_TERMIOS -5

/*
** keys inuputs (not used anymore)
** - get them by declaring buffer as follow: char *buffer[4]
**  and read as follow: read(fd, buffer, 3);
**  and then work with (int)buffer[0].
** unknown exceptions: 25715 29555 32639 8355711
*/
# define UNKNOWN_EXCEPTION 25715
# define UNKNOWN_EXCEPTION_1 29555
# define UNKNOWN_EXCEPTION_2 32639
# define UNKNOWN_EXCEPTION_3 8355711
# define HOME 4741915
# define END 4610843
# define ESC 27
# define SPACE 32
# define ENTER 10
# define DEL 127
# define DEL_FWD 3365659
# define LEFT (KEY0 == 27 && KEY1 == 91 && KEY2 == 68)
# define RIGHT 4414235
# define UP 4283163
# define DOWN 4348699
# define CTRL_D 4
# define CTRL_C 3
# define CTRL_L 12
# define CTRL_R 18
# define CTRL_UI 3234587
# define SHIFT_RIGHT 4403771
# define SHIFT_LEFT 4469307
# define SHIFT_HOME 4731451
# define SHIFT_END 4600379
# define OPT_X 8948194
# define OPT_C 42947
# define OPT_V 10127586
# define OPT_TOP 65
# define OPT_BOTTOM 66
# define OPT_LEFT 68
# define OPT_RIGHT 67

/*
** Page up/down also print a ~ right after (need to catch it)
*/

# define PAGE_UP 3496731
# define PAGE_DO 3562267

/*
** Input mode
*/
# define IM_DEFAULT 0
# define IM_HISTORY_SEARCH 1
# define IM_COPY_CUT_PASTE 2
# define IM_AUTO_COMPLETION 2

/*
** ----------
** ----------
** PROTOTYPES
** ----------
** ----------
*/

void		signal_catch(void);
/*
** ----------
** main
** ----------
*/
char		**cmd_line(char **cmds, char *prompt, int mode);

/*
** low level
*/

t_int_3		t_int_3_set(int x, int y, int z);
int			ram_save_int(int i, int flag);
int			is_line_ender(char *line_ender, char c);
char		*get_current_output(t_ui *ui);
int			get_char_screen_size(char c);
char		*get_word_at_n(char *str, int n);

/*
** medium level
*/
void		disp_n_fd(char *str, int n, int fd);
void		disp_fd(char *str, int fd);
void		disp_n(char *str, int n);
void		disp(char *str);
char		get_str_char_at_z(char *str, int z);
int			input_len(char *str);
int			input_n_len(char *str, int n);
int			get_str_lines_nb(char *str, char *line_ender, int line_size);
int			get_str_line_n_size(char *str, char *line_ender, int l_size, int n);
int			get_char_nb_until_line_n
				(char *str, char *line_ender, int l_size, int n);
t_int_3		get_xy_from_z(char *str, int z, int l_size, char *l_ender);
int			get_str_key_from_z(char *str, int z);
char		**get_suggestion(char *str);

/*
** higher level
*/
int			move_cursor_left_advanced(t_ui *ui, char *output, int flag);
int			move_cursor_left_advanced_n(t_ui *ui, int n);
int			move_cursor_right_advanced(t_ui *ui, char *output, int flag);
int			move_cursor_right_advanced_n(t_ui *ui, int n);
void		cursor_update_xy(t_ui *ui);
void		screen_put_str(t_ui *ui, char	*str);
void		screen_erase(t_ui *ui);
void		screen_reset(t_ui *ui);
void		insert_and_reset(t_ui *ui, int i, char c);
void		delete_and_reset(t_ui *ui, int i);
int			k_end(t_ui *ui);
int			keys_copy_cut_paste(t_ui *ui);
int			keys_copy_cut_paste_option_regular(t_ui *ui);
int			keys_copy_cut_paste_option_arrow(t_ui *ui);

/*
** output
*/
int			get_output_key_from_z(t_ui *ui, int z);
int			get_output_char_nb_until_line_n(t_ui *ui, int n);
char		get_output_char_at_z(t_ui *ui, int z);
int			get_output_line_n_size(t_ui *ui, int n);
int			get_output_lines_nb(t_ui *ui);

/*
** cmd_line
*/
t_ui		*handler(int flag, struct s_ui *ui_pt);
void		cmd_line_clear(t_ui *ui, int flag);
char		**cmd_line_xterm(char **cmds, char *prompt, int mode);

/*
** return status
*/
int			cmd_line_return_status(int status, char *common);
int			is_cmd_line_exit_status(char *status);

/*
** keys_dispatcher
*/
int			keys(t_ui *ui);
int			is_default_key(int key);

/*
** keys_basics
*/
int			keys_default_ccp(t_ui *ui);
int			keys_default(t_ui *ui);
int			keys_default_ctrl(t_ui *ui);
int			keys_default_del(t_ui *ui);
int			keys_default_home_end(t_ui *ui);
int			keys_default_cursor_move_left_right(t_ui *ui);
int			keys_default_cmds(t_ui *ui);
int			keys_auto_completion(t_ui *ui);

/*
** keys_history_search
*/
int			history_search_keys(t_ui *ui);

/*
** history_search_tools
*/
int			get_correspondance(t_ui *ui, int start);
void		clear_screen_h(t_ui *ui);
void		update_screen_h(t_ui *ui);

/*
** word_to_word.c
*/
int			get_next_word(t_ui *ui);
int			get_prev_word(t_ui *ui);

/*
** ----------
** STRUCTURES
** ----------
*/

/*
** t_ui_crud
*/
int			t_ui_ini(t_ui *ui, char *prompt, char **cmds, int mode);
void		t_ui_del(t_ui *ui);
void		t_ui_del_all(t_ui *ui);

/*
** t_h_search_crud
*/
void		t_h_search_ini(t_h_search *search, int flag);
void		t_h_search_del(t_h_search *search);

/*
** t_auto_complete
*/

int			t_auto_completion_create(t_auto_comp *ac);
void		t_auto_completion_destroy(t_auto_comp *ac);
void		t_auto_completion_del(t_auto_comp *ac);
void		t_auto_completion_ini(t_auto_comp *ac);

/*
** ----------
**   TOOLS
** ----------
*/

/*
** term handler
*/
int			term_change(t_ui *ui);
int			term_back(t_ui *ui);
void		termcaps_usual(char *cmd);
void		termcaps_usual_pos(char *cmd, int x, int y);

/*
** term_tools
*/
void		del_str_c(char **line, int *pos, int n);
void		del_line(t_ui *ui, int n);
void		move_cursor(int n, int flag);
int			cursor_check(t_ui *ui, int pos);

/*
** screen_handler_default.c
*/
void		cursor_to_prompt(t_ui *ui);
void		put_str_to_screen(t_ui *ui, char *str);
void		update_screen(t_ui *ui);

/*
** tools
*/
int			cursor_on_cmds(t_ui *ui);
int			cursor_on_str(t_ui *ui, int pos_x);
int			get_len_line_nb(t_ui *ui, int len);
int			get_str_line_nb(t_ui *ui, char *str);
void		clear_screen_cmd(t_ui *ui);

/*
** tools_cursor_move_vert
*/
int			move_cursor_up(t_ui *ui, int safety);
int			move_cursor_do(t_ui *ui);
int			move_cursor_up_n(t_ui *ui, int n, int safety);
int			move_cursor_do_n(t_ui *ui, int n);

/*
** tools_cursor_move_type_1
*/
int			move_cursor_left(t_ui *ui, int safety);
int			move_cursor_left_n(t_ui *ui, int n, int safety);
int			move_cursor_right(t_ui *ui, int safety);
int			move_cursor_right_n(t_ui *ui, int n, int safety);

/*
** tools_get_win_size.c
*/
int			get_win_size(int flag);

/*
** ac_get_suggestions etc
*/
t_list		*lst_remove_non_match(t_list *lst, char *to_complete);
int			dyn_get_dirfile_separator(char *s);
#endif
