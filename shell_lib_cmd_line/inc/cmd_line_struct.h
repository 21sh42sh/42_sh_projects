/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_line_struct.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 18:15:40 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 17:58:53 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CMD_LINE_STRUCT_H
# define CMD_LINE_STRUCT_H
# include "cmd_line.h"
# include <curses.h>
# include <fcntl.h>
# include <signal.h>
# include <stdlib.h>
# include <sys/ioctl.h>
# include <term.h>
# include <termios.h>
# include <unistd.h>

typedef struct		s_int_3
{
	int				x;
	int				y;
	int				z;
}					t_int_3;

typedef struct		s_int_6
{
	int				x;
	int				y;
	int				z;
	int				a;
	int				b;
	int				c;
}					t_int_6;

typedef struct		s_auto_comp
{
	char			**suggestion;
	char			*cmd;
	int				cursor_pos_o;
	int				cursor_pos_m;
	int				s_index;
	char			*word;
}					t_auto_comp;

typedef struct		s_h_search
{
	int				cmd_id_start;
	int				cmd_cursor_start;
	int				cmd_id_last;
	int				cmd_cursor_pos;
	char			*prompt;
	char			*error_msg;
	int				error;
	char			*searching;
	char			*last_search;
	int				searching_pos;
	int				cmd_cursor_pos_l;

}					t_h_search;

typedef struct		s_ui
{
	struct termios	term;
	struct termios	term_o;
	int				mode;
	t_auto_comp		auto_comp;
	t_h_search		h_search;
	t_int_3			cursor_pos;
	t_int_3			win_size;
	t_int_3			win_size_new;
	t_int_3			key;
	t_int_3			last_key;
	char			**cmds_o;
	char			**cmds;
	int				cmd_id;
	char			*line_ender;
	char			*prompt;
	int				input_mode;
	int				tab_size;
	int				select_from;
	int				select_long;
	int				ignore_next_read;
	char			*clipboard;
}					t_ui;
#endif
