/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_cursor_move_vert.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/03 18:08:21 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** safety : 0 => off
*/

/*
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_up(t_ui *ui, int safety)
{
	if (CURSOR_POS.y > 0 || safety == 0)
	{
		termcaps_usual("up");
		CURSOR_POS.y--;
		return (0);
	}
	return (1);
}

/*
** return:
** _______ 0
*/

int		move_cursor_do(t_ui *ui)
{
	termcaps_usual("do");
	CURSOR_POS.y++;
	return (0);
}

/*
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_up_n(t_ui *ui, int n, int safety)
{
	termcaps_usual("vi");
	while (n > 0)
	{
		if (move_cursor_up(ui, safety))
			return (1);
		n--;
	}
	termcaps_usual("ve");
	return (0);
}

/*
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_do_n(t_ui *ui, int n)
{
	termcaps_usual("vi");
	while (n > 0)
	{
		move_cursor_do(ui);
		n--;
	}
	termcaps_usual("ve");
	return (0);
}
