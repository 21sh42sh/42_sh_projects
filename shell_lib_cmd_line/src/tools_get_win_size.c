/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_get_win_size.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/01/28 18:04:20 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** Flag = 1 => get col
** Flag = 2 => get row
*/

int		get_win_size(int flag)
{
	struct winsize	w;
	int				ret;

	ret = -1;
	ret = ioctl(0, TIOCGWINSZ, &w);
	if (ret == 0 && flag == 1)
		return (w.ws_col);
	if (ret == 0 && flag == 2)
		return (w.ws_row);
	return (ret);
}
