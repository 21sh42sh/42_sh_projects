/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_line.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 00:47:49 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/27 14:41:56 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static char	**cmd_line_default(char **cmds, char *prompt)
{
	char	*pt;
	char	*line;

	pt = NULL;
	pt = get_shell_env_by_name(use_shell_struct(NULL), "PWD");
	if (!lib_strcmp(prompt, "$> ") && pt && pt[0])
		lib_putstr(get_shell_env_by_name(use_shell_struct(NULL), "PWD"));
	lib_putstr(prompt);
	get_next_line(0, &line);
	cmds = lib_arr_push_front(cmds, line);
	free(line);
	return (cmds);
}

char		**read_from_av_ac(char **cmds, t_shell *sh)
{
	if (sh->ac_i >= sh->ac - 1)
	{
		lib_arr_del(cmds);
		return (NULL);
	}
	sh->ac_i += 1;
	cmds = lib_arr_push_front(cmds, sh->av[sh->ac_i]);
	return (cmds);
}

char		**read_closed_std0(char **cmds)
{
	char	*line;

	if (lib_arr_len(cmds) > 50)
	{
		lib_arr_del(cmds);
		return (NULL);
	}
	if (get_next_line(0, &line) <= 0)
	{
		free(line);
		lib_arr_del(cmds);
		return (NULL);
	}
	cmds = lib_arr_push_front(cmds, line);
	free(line);
	return (cmds);
}

char		**check_cmds(char **cmds, t_shell *sh)
{
	char	*hist_size_env;
	int		hist_size;

	hist_size_env = get_shell_env_by_name(sh, "HIST_SIZE");
	if (!hist_size_env)
		hist_size = 100;
	else
	{
		hist_size = lib_atoi(hist_size_env);
		hist_size == 0 ? hist_size = 1 : hist_size;
	}
	if (lib_arr_len(cmds) > (size_t)hist_size)
		cmds = lib_arr_ndup(cmds, hist_size, 1);
	return (cmds);
}

char		**cmd_line(char **cmds, char *prompt, int mode)
{
	t_shell	*sh;

	cmd_line_return_status(0, "ini");
	sh = use_shell_struct(NULL);
	if (sh->ac > 1)
		cmds = read_from_av_ac(cmds, sh);
	else if (isatty(0))
	{
		if (!lib_strcmp(get_shell_env_by_name(sh, "TERM"),
				"xterm-256color"))
			cmds = cmd_line_xterm(cmds, prompt, mode);
		else
			cmds = cmd_line_default(cmds, prompt);
	}
	else
		cmds = read_closed_std0(cmds);
	cmds = check_cmds(cmds, sh);
	return (cmds);
}
