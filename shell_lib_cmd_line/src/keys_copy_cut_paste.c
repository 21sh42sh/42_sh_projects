/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_copy_cut_paste.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 20:53:20 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static void	keys_copy_cut_paste_clean(t_ui *ui)
{
	int		i;
	int		pos;
	char	*output;

	pos = CURSOR_POS.z;
	ui->select_from = -1;
	ui->input_mode = IM_DEFAULT;
	move_cursor_left_advanced_n(ui, ui->select_long);
	output = get_current_output(ui);
	i = get_str_key_from_z(output, CURSOR_POS.z);
	while (ui->select_long > 0 && output && output + i)
	{
		ui->select_long--;
		write(1, output + i, 1);
		CURSOR_POS.z += get_char_screen_size(output[i]);
		i++;
	}
	move_cursor_right_advanced_n(ui, pos - CURSOR_POS.z);
	move_cursor_left_advanced_n(ui, CURSOR_POS.z - pos);
	lib_strdel(&output);
}

/*
** return:
** _______ boolean
*/

static int	is_ccp_key(t_int_3 key)
{
	if (key.x == 27 && key.y == 27 && key.z == 91)
		return (1);
	return (0);
}

/*
** return:
** _______ boolean
*/

static int	is_ccp_key_ommit(t_int_3 key)
{
	if (key.x == 27 && key.y == 27 && key.z == 91)
		return (1);
	return (0);
}

int			keys_copy_cut_paste(t_ui *ui)
{
	int	ret;

	if ((ui->ignore_next_read && is_ccp_key(LAST_KEY))
			|| is_ccp_key_ommit(KEY))
	{
		ui->ignore_next_read = 0;
		return (0);
	}
	ret = -1;
	if (ret == -1)
		ret = keys_copy_cut_paste_option_arrow(ui);
	if (ret == -1)
		ret = keys_copy_cut_paste_option_regular(ui);
	if (ret == -1)
		ret = keys_default(ui);
	else if (ret == -100)
		keys_copy_cut_paste_clean(ui);
	return (ret);
}
