/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_auto_completion_crud.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 19:29:27 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

void	t_auto_completion_ini(t_auto_comp *ac)
{
	if (ac)
	{
		ac->suggestion = NULL;
		ac->cmd = NULL;
		ac->word = NULL;
		ac->cursor_pos_o = 0;
		ac->s_index = 0;
	}
}

void	t_auto_completion_del(t_auto_comp *ac)
{
	lib_arr_del(ac->suggestion);
	lib_strdel(&ac->cmd);
	lib_strdel(&ac->word);
	ac->cursor_pos_o = 0;
	ac->s_index = 0;
}

int		t_auto_completion_create(t_auto_comp *ac)
{
	t_auto_completion_ini(ac);
	return (0);
}

void	t_auto_completion_destroy(t_auto_comp *ac)
{
	if (ac)
	{
		t_auto_completion_del(ac);
	}
}
