/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen_handler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 14:15:37 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** Get the screen size and check if it has changed.
** return:
** _______ 0 if no change
** _______ 1 if changed
*/

int		update_screen_size(t_ui *ui)
{
	WIN_SIZE_NEW.x = get_win_size(1);
	WIN_SIZE_NEW.y = get_win_size(2);
	if (WIN_SIZE_NEW.x != WIN_SIZE.x)
		return (1);
	return (0);
}

void	insert_and_reset(t_ui *ui, int i, char c)
{
	screen_erase(ui);
	CMD = lib_strins_c(CMD, c, i - PROMPT_LEN, 1);
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, i + 1 - PROMPT_LEN);
	move_cursor_right_advanced_n(ui, PROMPT_LEN - CURSOR_POS.z);
}

void	delete_and_reset(t_ui *ui, int i)
{
	screen_erase(ui);
	CMD = lib_strrm_c(CMD, i - PROMPT_LEN, 1);
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, i - PROMPT_LEN);
	move_cursor_right_advanced_n(ui, PROMPT_LEN - CURSOR_POS.z);
}

void	screen_erase(t_ui *ui)
{
	t_int_3	cur_tmp;
	char	*tmp;

	tmp = get_current_output(ui);
	cur_tmp.y = get_str_lines_nb(tmp, ui->line_ender, WIN_SIZE_NEW.x);
	while (CURSOR_POS.y < cur_tmp.y && !move_cursor_do(ui))
		;
	while (CURSOR_POS.y > cur_tmp.y && cur_tmp.y >= 0 && !move_cursor_up(ui, 1))
		;
	termcaps_usual("cr");
	CURSOR_POS.x = 0;
	del_line(ui, cur_tmp.y + 1);
	CURSOR_POS.z = 0;
	free(tmp);
}

void	screen_reset(t_ui *ui)
{
	t_int_3	cursor_old;
	char	*output;

	WIN_SIZE = update_screen_size(ui) ? WIN_SIZE_NEW : WIN_SIZE;
	cursor_old.z = CURSOR_POS.z;
	output = get_current_output(ui);
	cursor_old =
		get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
	CURSOR_POS =
		get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
	screen_erase(ui);
	disp(output);
	CURSOR_POS.z += input_len(output);
	CURSOR_POS =
		get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
	if (ui->input_mode == IM_HISTORY_SEARCH)
		while (CURSOR_POS.z != H_SEARCH.cmd_cursor_pos)
			move_cursor_left_advanced(ui, output, 0);
	else
		move_cursor_left_advanced_n(ui, CURSOR_POS.z - cursor_old.z);
	if (CURSOR_POS.z < PROMPT_LEN)
		move_cursor_right_advanced_n(ui, PROMPT_LEN - CURSOR_POS.z);
	CURSOR_POS =
		get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
	free(output);
}
