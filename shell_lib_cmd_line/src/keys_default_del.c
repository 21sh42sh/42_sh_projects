/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_del.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/06 16:41:13 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		key_del(t_ui *ui)
{
	if (CURSOR_POS.z - PROMPT_LEN > 0
		&& CURSOR_POS.z - PROMPT_LEN < input_len(CMD) + 1)
	{
		if (CURSOR_POS.x == 0
			|| get_output_char_at_z(ui, CURSOR_POS.z - 1) == '\n'
			|| get_output_line_n_size(ui, CURSOR_POS.y) == WIN_SIZE.x)
		{
			delete_and_reset(ui, CURSOR_POS.z - 1);
			return (0);
		}
		CMD = lib_strrm_c(CMD, CURSOR_POS.z - PROMPT_LEN - 1, 1);
		move_cursor_left_advanced_n(ui, 1);
		termcaps_usual("dc");
	}
	return (0);
}

int		key_del_fwd(t_ui *ui)
{
	if (CURSOR_POS.z - PROMPT_LEN >= 0
		&& CURSOR_POS.z - PROMPT_LEN < input_len(CMD))
	{
		if (get_output_char_at_z(ui, CURSOR_POS.z) == '\n'
			|| get_output_line_n_size(ui, CURSOR_POS.y) == WIN_SIZE.x)
		{
			delete_and_reset(ui, CURSOR_POS.z);
			return (0);
		}
		CMD = lib_strrm_c(CMD, CURSOR_POS.z - PROMPT_LEN, 1);
		termcaps_usual("dc");
	}
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int		keys_default_del(t_ui *ui)
{
	if (KEY0 == 127 && KEY1 == 0 && KEY2 == 0)
		return (key_del(ui));
	if ((LAST_KEY0 == 27 && LAST_KEY1 == 91 && LAST_KEY2 == 51)
			&& (KEY0 == 126 && KEY1 == 0 && KEY2 == 0))
		return (key_del_fwd(ui));
	return (-1);
}
