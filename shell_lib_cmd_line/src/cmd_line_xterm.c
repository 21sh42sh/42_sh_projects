/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_line_xterm.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/27 14:44:56 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** flag: 1 free all but ui->line
** flag: 2 free everything
*/

void	cmd_line_clear(t_ui *ui, int flag)
{
	if (flag == 1)
	{
		term_back(ui);
		t_ui_del(ui);
	}
	else if (flag == 2)
	{
		term_back(ui);
		t_ui_del_all(ui);
	}
}

char	**exit_succes(t_ui *ui, char **cmds)
{
	cmds = lib_arr_push_front(cmds, CMD);
	cmd_line_clear(ui, 2);
	return (cmds);
}

void	debug(t_ui *ui)
{
	int		pos;

	if (1)
		return ;
	pos = CURSOR_POS.z;
	screen_erase(ui);
	lib_putendl(lib_itoa(KEY0));
	lib_putendl(lib_itoa(KEY1));
	lib_putendl(lib_itoa(KEY2));
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, pos - CURSOR_POS.z);
}

/*
** cmd_line execution function.
** return:
** _______ cmds
** _______ NULL
*/

char	**cmd_line_do(char **cmds, t_ui *ui)
{
	int			ret;
	char		buf[3];

	while (1)
	{
		lib_bzero(buf, 3);
		read(0, buf, 3);
		ui->key = t_int_3_set((int)buf[0], (int)buf[1], (int)buf[2]);
		debug(ui);
		ret = keys(ui);
		if (ret == 1)
			return (exit_succes(ui, cmds));
		else if (ret == 2 || ret == 3)
		{
			cmd_line_clear(ui, 2);
			lib_arr_del(cmds);
			return (NULL);
		}
		LAST_KEY = t_int_3_set(KEY0, KEY1, KEY2);
	}
	return (NULL);
}

/*
** cmd_line main function.
** mode:
** _____ 1: print C^ on ctrl-C
** _____ 2: exit on ctrl-C
*/

char	**cmd_line_xterm(char **cmds, char *prompt, int mode)
{
	t_ui		*ui;

	if (!(ui = malloc(sizeof(t_ui))))
		return (cmds);
	if ((t_ui_ini(ui, prompt, cmds, mode)))
		return (NULL);
	ram_save_int(0, -1);
	disp(PROMPT);
	CURSOR_POS.z = input_len(PROMPT);
	return (cmd_line_do(cmds, ui));
}
