/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/08 18:39:48 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static t_int_3	t_int_3_fill(int x, int y, int z)
{
	t_int_3	new;

	new.x = x;
	new.y = y;
	new.z = z;
	return (new);
}

/*
** Calcul the x and y index based on the z index.
** parameters:
** ___________ l_size positive or zero.
** ___________ z positive or zero.
** variable:
** _________ pos.x E {0:l_size - 1}
** _________ pos.y positive or zero
** _________ k E {0:z}
** return:
** _______ (0, 0, z) if z < 0
** _______ pos otherwise.
*/

t_int_3			get_xy_from_z(char *str, int z, int l_size, char *l_ender)
{
	t_int_3		pos;
	int			i;
	int			k;

	pos = t_int_3_fill(0, 0, z);
	if (z < 0)
		return (pos);
	pos.x = -1;
	i = 0;
	k = 0;
	while (k <= z)
	{
		if ((str && i > 0 && i <= (int)lib_strlen(str) && str[i - 1]
				&& is_line_ender(l_ender, str[i - 1]))
				|| pos.x == l_size - 1)
			pos = t_int_3_fill(0, pos.y + 1, z);
		else
			pos.x++;
		if (str && i >= 0 && i < (int)lib_strlen(str))
			k += get_char_screen_size(str[i]);
		else
			k++;
		i++;
	}
	return (pos);
}

void			cursor_update_xy(t_ui *ui)
{
	char	*output;

	WIN_SIZE_NEW.x = get_win_size(1);
	WIN_SIZE_NEW.y = get_win_size(2);
	output = get_current_output(ui);
	CURSOR_POS = get_xy_from_z(output, CURSOR_POS.z,
		WIN_SIZE.x, ui->line_ender);
	free(output);
	WIN_SIZE.x = WIN_SIZE_NEW.x;
	WIN_SIZE.y = WIN_SIZE_NEW.y;
}
