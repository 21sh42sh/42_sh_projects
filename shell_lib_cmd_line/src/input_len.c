/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_len.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/08/25 16:51:02 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		input_len(char *str)
{
	int	len;

	len = 0;
	while (str && *str)
		len += get_char_screen_size(*str++);
	return (len);
}

int		input_n_len(char *str, int n)
{
	int	len;

	len = 0;
	while (str && *str && len < n)
		len += get_char_screen_size(*str++);
	return (len);
}
