/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_copy_cut_paste_regular.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 16:26:24 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** CUT
*/

static int	ctrl_k(t_ui *ui)
{
	int		pos;
	int		from;
	char	*tmp;

	if (ui->select_from < 0 || ui->select_long == 0)
		return (0);
	pos = CURSOR_POS.z;
	if ((from = get_output_key_from_z(ui, ui->select_from) - PROMPT_LEN) < 0)
		return (0);
	screen_erase(ui);
	ui->clipboard = lib_strnew(ui->select_long);
	ui->clipboard = lib_strncpy(ui->clipboard, CMD + from, ui->select_long);
	tmp = lib_strnew(from);
	tmp = lib_strncpy(tmp, CMD, from);
	tmp = lib_strjoinfree(tmp, CMD + from + ui->select_long, 1);
	free(CMD);
	CMD = tmp;
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, pos - ui->select_long - CURSOR_POS.z);
	move_cursor_left_advanced_n(ui, CURSOR_POS.z - pos + ui->select_long);
	ui->input_mode = IM_DEFAULT;
	ui->select_from = -1;
	ui->select_long = 0;
	return (0);
}

/*
** COPY
*/

static int	ctrl_g(t_ui *ui)
{
	int	from;

	if (ui->clipboard)
		free(ui->clipboard);
	if (ui->select_from < PROMPT_LEN)
		return (0);
	from = get_output_key_from_z(ui, ui->select_from) - PROMPT_LEN;
	ui->clipboard = lib_strnew(ui->select_long);
	ui->clipboard = lib_strncpy(ui->clipboard, CMD + from, ui->select_long);
	ui->input_mode = IM_DEFAULT;
	ui->select_from = -1;
	ui->select_long = 0;
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int			keys_copy_cut_paste_option_regular(t_ui *ui)
{
	if ((KEY0 == 11 && KEY1 == 0 && KEY2 == 0))
		return (ctrl_k(ui));
	if ((KEY0 == 7 && KEY1 == 0 && KEY2 == 0))
		return (ctrl_g(ui));
	return (-1);
}
