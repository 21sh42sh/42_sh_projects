/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_dispatcher.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 16:32:16 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		keys(t_ui *ui)
{
	int		ret;

	cursor_update_xy(ui);
	ret = 0;
	if (ui->input_mode == IM_DEFAULT)
		ret = keys_default(ui);
	else if (ui->input_mode == IM_HISTORY_SEARCH)
		ret = (history_search_keys(ui));
	else if (ui->input_mode == IM_COPY_CUT_PASTE)
		ret = (keys_copy_cut_paste(ui));
	else if (ui->input_mode == IM_AUTO_COMPLETION)
		ret = (keys_auto_completion(ui));
	cursor_update_xy(ui);
	return (ret);
}
