/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/28 15:41:06 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** return the key that match the position z.
*/

int		get_str_key_from_z(char *str, int z)
{
	int	i;

	i = 0;
	while (z && str && str[i])
	{
		z -= get_char_screen_size(str[i]);
		i++;
	}
	return (i);
}

/*
** return the key that match the position str key.
*/

int		get_z_from_str_key(char *str, int i)
{
	int	z;

	z = 0;
	while (z && str && str[i])
	{
		z += get_char_screen_size(str[i]);
		i--;
	}
	return (z - 1);
}

/*
** Check if the char c is equal to a line ender (defined in line_ender).
** return:
** _______ 0 no match
** _______ 1 match
*/

int		is_line_ender(char *line_ender, char c)
{
	while (line_ender && *line_ender)
	{
		if (c == *line_ender)
			return (1);
		line_ender += 1;
	}
	return (0);
}

/*
** return the size until the line n in the str when it will be put on a screen
**  of width line_size and with the element stored in line_ender as line
**  breaking point.
** /!\ include the line_breaking point in the returned value. /!\
*/

int		get_char_nb_until_line_n(char *str, char *line_ender, int l_size, int n)
{
	int	size;
	int	nb_line;
	int	char_nb;

	size = 0;
	char_nb = 0;
	nb_line = 0;
	while (str && *str && nb_line < n)
	{
		size += get_char_screen_size(*str);
		char_nb += get_char_screen_size(*str);
		if ((l_size > 0 && size >= l_size) || is_line_ender(line_ender, *str))
		{
			nb_line++;
			size = 0;
		}
		str += 1;
	}
	return (char_nb);
}

/*
** return the character at the position z
** parameters:
** ___________ z positive or zero.
*/

char	get_str_char_at_z(char *str, int z)
{
	int	k;

	if (!str || !*str || z < 0)
		return ('\0');
	k = -1;
	while (str && *str && k < z)
	{
		k += get_char_screen_size(*str);
		str += 1;
	}
	return (*(str - 1));
}
