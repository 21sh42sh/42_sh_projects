/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_h_search_crud.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 15:20:31 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** flag: 1 => free sub-element before initializing
*/

void	t_h_search_ini(t_h_search *search, int flag)
{
	if (flag == 1)
	{
		if (search->last_search)
			free(search->last_search);
		search->last_search = lib_strdup(search->searching);
		free(search->error_msg);
		free(search->prompt);
		free(search->searching);
	}
	search->cmd_id_start = 0;
	search->cmd_id_last = 0;
	search->cmd_cursor_pos = 0;
	search->cmd_cursor_pos_l = 0;
	search->cmd_cursor_start = 0;
	search->error_msg = lib_strdup("failing ");
	search->prompt = lib_strdup("bck-i-search: ");
	search->searching = lib_strdup("");
	search->error = 0;
	if (flag != 1)
		search->last_search = lib_strdup(search->searching);
}

void	t_h_search_del(t_h_search *search)
{
	if (search->error_msg)
		free(search->error_msg);
	if (search->prompt)
		free(search->prompt);
	if (search->searching)
		free(search->searching);
	if (search->last_search)
		free(search->last_search);
}
