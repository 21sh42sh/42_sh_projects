/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_copy_cut_paste_arrow.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 16:53:19 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static int	k_opt_left(t_ui *ui)
{
	char	*output;
	int		i;

	if (CURSOR_POS.z <= PROMPT_LEN)
		return (0);
	if (ui->select_long <= 0)
		return (0);
	ui->select_long--;
	move_cursor_left_advanced_n(ui, 1);
	output = get_current_output(ui);
	i = get_str_key_from_z(output, CURSOR_POS.z);
	lib_putendl_fd(lib_itoa(i), 2);
	if (output[i] != '\n')
	{
		lib_putendl_fd("\\n", 2);
		if (output[i] == '\t')
			write(1, " ", 1);
		else
			write(1, output + i, 1);
		CURSOR_POS.z += get_char_screen_size(output[i]);
		move_cursor_left_advanced_n(ui, 1);
	}
	free(output);
	return (0);
}

static int	k_opt_right(t_ui *ui)
{
	char	*output;
	int		i;

	if (CURSOR_POS.z - PROMPT_LEN > input_len(CMD))
		return (0);
	if (ui->select_from == -1)
		ui->select_from = CURSOR_POS.z;
	output = get_current_output(ui);
	ui->select_long++;
	termcaps_usual("us");
	i = get_str_key_from_z(output, CURSOR_POS.z);
	write(1, output + i, 1);
	CURSOR_POS.z += get_char_screen_size(output[i]);
	termcaps_usual("ue");
	free(output);
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int			keys_copy_cut_paste_option_arrow(t_ui *ui)
{
	if (LAST_KEY0 == 27 && LAST_KEY1 == 27 && LAST_KEY2 == 91
			&& KEY0 == 68 && KEY1 == 0 && KEY2 == 0 && 1 == 0)
		return (k_opt_left(ui));
	if (LAST_KEY0 == 27 && LAST_KEY1 == 27 && LAST_KEY2 == 91
			&& KEY0 == 67 && KEY1 == 0 && KEY2 == 0)
		return (k_opt_right(ui));
	return (-1);
}
