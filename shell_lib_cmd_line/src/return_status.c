/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   return_status.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/27 15:47:15 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** contain the exit status of the cmd_line
** status != 0 will save status as the return value.
** common:
** _______ NULL (and status == 0) retireve ret status
** _______ "ini" set ret to 0
** _______ "common" 1
** _______ "Ctrl-C" 2
** _______ "Ctrl-D" 3
*/

static int	get_status(char *msg, int ret)
{
	if (!lib_strcmp(msg, "error"))
		return (-1);
	if (!lib_strcmp(msg, "ini"))
		return (0);
	if (!lib_strcmp(msg, "ENTER"))
		return (1);
	if (!lib_strcmp(msg, "Ctrl-C"))
		return (2);
	if (!lib_strcmp(msg, "Ctrl-D"))
		return (3);
	return (ret);
}

int			cmd_line_return_status(int status, char *common)
{
	static int	ret;

	if (!common && status == 0)
		return (ret);
	if (status != 0)
		ret = status;
	ret = get_status(common, ret);
	return (ret);
}

/*
** Boolean
** return:
** _______ 1 if the return status is set to your expectation
** _______ 0 otherwise
*/

int			is_cmd_line_exit_status(char *status)
{
	int test;
	int ret;

	test = get_status(status, -2);
	ret = cmd_line_return_status(0, NULL);
	return (ret == test);
}
