/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_cursor_move.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 14:13:58 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** Move the cursor on the right on the global output so that it go on the
**  beggining of the bottom line when reaching the end of the current
**  screen line.
*/

/*
** flag:
** _____ 0 -> do not free output
** _____ 1 -> do free output
*/

int		move_cursor_right_advanced(t_ui *ui, char *output, int flag)
{
	if (!output)
		output = get_current_output(ui);
	if (CURSOR_POS.x == WIN_SIZE.x - 1)
	{
		move_cursor_do(ui);
		termcaps_usual("cr");
		CURSOR_POS.x = 0;
		CURSOR_POS.z++;
	}
	else if (get_str_char_at_z(output, CURSOR_POS.z) == '\n')
	{
		move_cursor_do(ui);
		termcaps_usual("cr");
		CURSOR_POS.x = 0;
		CURSOR_POS.z++;
	}
	else
		move_cursor_right(ui, 0);
	CURSOR_POS =
		get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
	if (flag == 1)
		free(output);
	return (0);
}

int		move_cursor_right_advanced_n(t_ui *ui, int n)
{
	char	*output;

	output = get_current_output(ui);
	termcaps_usual("vi");
	while (n-- > 0)
	{
		if (1 == move_cursor_right_advanced(ui, output, 0))
			return (1);
	}
	termcaps_usual("ve");
	free(output);
	return (0);
}

/*
** Move the cursor on the left on the global output so that it go at the
**  end of the upper line when reaching the beggining of the current
**  screen line.
** flag:
** _____ 0 -> do not free output
** _____ 1 -> do free output
** return:
** _______ 1 => cannot move further.
*/

int		move_cursor_left_advanced(t_ui *ui, char *output, int flag)
{
	int		len;

	if (CURSOR_POS.x <= 0)
	{
		if (CURSOR_POS.y == 0)
			return (1);
		if (!output)
			output = get_current_output(ui);
		len = get_str_line_n_size(output,
				ui->line_ender, WIN_SIZE.x, CURSOR_POS.y - 1);
		move_cursor_up(ui, 0);
		termcaps_usual("cr");
		CURSOR_POS.x = 0;
		CURSOR_POS.z -= len + 1;
		move_cursor_right_n(ui, len, 1);
		if (get_str_char_at_z(output, CURSOR_POS.z) == '\n')
			termcaps_usual("le");
		CURSOR_POS =
			get_xy_from_z(output, CURSOR_POS.z, WIN_SIZE.x, ui->line_ender);
		if (flag == 1)
			free(output);
	}
	else
		move_cursor_left(ui, 0);
	return (0);
}

int		move_cursor_left_advanced_n(t_ui *ui, int n)
{
	char	*output;

	output = get_current_output(ui);
	termcaps_usual("vi");
	while (n-- > 0)
	{
		if (move_cursor_left_advanced(ui, output, 0) == 1)
		{
			termcaps_usual("ve");
			return (1);
		}
	}
	termcaps_usual("ve");
	free(output);
	return (0);
}
