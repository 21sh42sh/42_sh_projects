/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_cursor_move_hor.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/03 18:58:02 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** move the cursor 1 times on the left. A safety is activable to allow
**  the movement only if the CURSOR_POS.x is superior to 0.
** safety : 0 => off
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_left(t_ui *ui, int safety)
{
	if (safety == 0 || (safety == 1 && CURSOR_POS.x > 0))
	{
		termcaps_usual("le");
		CURSOR_POS.x--;
		CURSOR_POS.z--;
	}
	else
		return (1);
	return (0);
}

/*
** move the cursor n times on the left. A safety is activable to allow
**  the movement only if the CURSOR_POS.x is superior to 0.
** safety : 0 => off
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_left_n(t_ui *ui, int n, int safety)
{
	termcaps_usual("vi");
	while (n > 0)
	{
		if (move_cursor_left(ui, safety))
		{
			termcaps_usual("ve");
			return (1);
		}
		n--;
	}
	termcaps_usual("ve");
	return (0);
}

/*
** move the cursor 1 times on the right. A safety is activable to allow
**  the movement only if the CURSOR_POS.x is inferior to WIN_SIZE.x.
** safety : 0 => off
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_right(t_ui *ui, int safety)
{
	int	len;

	if (safety)
		len = get_output_line_n_size(ui, CURSOR_POS.y);
	if (safety == 0 || (safety == 1 && CURSOR_POS.x < len))
	{
		termcaps_usual("nd");
		CURSOR_POS.x++;
		CURSOR_POS.z++;
	}
	else
		return (1);
	return (0);
}

/*
** safety : 0 => off
** return:
** _______ 0 didn't reached safety
** _______ 1 safety reached
*/

int		move_cursor_right_n(t_ui *ui, int n, int safety)
{
	termcaps_usual("vi");
	while (n > 0)
	{
		if (move_cursor_right(ui, safety))
		{
			termcaps_usual("ve");
			return (1);
		}
		n--;
	}
	termcaps_usual("ve");
	return (0);
}
