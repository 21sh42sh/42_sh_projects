/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_cmds.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 15:36:53 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		k_down(t_ui *ui)
{
	if (CMD_ID > 0)
	{
		screen_erase(ui);
		CMD_ID--;
		disp(PROMPT);
		disp(CMD);
		CURSOR_POS.z = PROMPT_LEN + input_len(CMD);
	}
	return (0);
}

int		k_up(t_ui *ui)
{
	if (CMD_ID < (int)lib_arr_len(CMDS) - 1)
	{
		screen_erase(ui);
		CMD_ID++;
		disp(PROMPT);
		disp(CMD);
		CURSOR_POS.z = PROMPT_LEN + input_len(CMD);
	}
	return (0);
}

int		k_enter(t_ui *ui)
{
	if (LAST_KEY0 == 27 && LAST_KEY1 == 0 && LAST_KEY2 == 0)
	{
		insert_and_reset(ui, CURSOR_POS.z, '\n');
		return (0);
	}
	k_end(ui);
	if (!CMD)
		CMD = lib_strdup("");
	write(1, "\n", 1);
	term_back(ui);
	cmd_line_return_status(0, "Enter");
	return (1);
}

int		k_tab(t_ui *ui)
{
	if (LAST_KEY0 != 27 || LAST_KEY1 != 0 || LAST_KEY2 != 0)
	{
		ui->input_mode = IM_AUTO_COMPLETION;
		return (keys_auto_completion(ui));
	}
	ram_save_int(CURSOR_POS.z + 1, 1);
	screen_erase(ui);
	CMD = lib_strins_c(CMD, '\t', ram_save_int(0, 3) - 1 - PROMPT_LEN, 1);
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, ram_save_int(0, 2) - CURSOR_POS.z);
	return (0);
}

/*
** return:
** _______ 0 if a regular button has been detected
** _______ 1 if enter has been pressed
** _______ -1 otherwise
*/

int		keys_default_cmds(t_ui *ui)
{
	if (KEY0 == 27 && KEY1 == 0 && KEY2 == 0)
	{
		if (LAST_KEY0 == 27 && LAST_KEY1 == 0 && LAST_KEY2 == 0)
			LAST_KEY = t_int_3_set(0, 0, 0);
	}
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 66)
		return (k_down(ui));
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 65)
		return (k_up(ui));
	if (KEY0 == 10 && KEY1 == 0 && KEY2 == 0)
		return (k_enter(ui));
	if (KEY0 == 9 && KEY1 == 0 && KEY2 == 0)
		return (k_tab(ui));
	return (-1);
}
