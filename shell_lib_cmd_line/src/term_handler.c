/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 21:29:28 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/24 16:40:12 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"
#include "builtin.h"
#include "shell_wrap.h"

static int	tputs_func(int disp)
{
	char	c;

	c = (char)disp;
	write(1, &c, 1);
	return (1);
}

void		termcaps_usual(char *cmd)
{
	char	*str;

	tgetent(NULL, "xterm-256color");
	str = tgetstr(cmd, NULL);
	tputs(str, 0, tputs_func);
}

void		termcaps_usual_pos(char *cmd, int x, int y)
{
	char	*str;

	tgetent(NULL, "xterm-256color");
	str = tgetstr(cmd, NULL);
	tputs(tgoto(str, x, y), 0, tputs_func);
}

int			term_change(t_ui *ui)
{
	ui->term.c_lflag &= ~(ECHO);
	ui->term.c_lflag &= ~(ICANON);
	ui->term.c_lflag &= ~(ISIG);
	ui->term.c_cc[VMIN] = 1;
	ui->term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, &ui->term) == -1)
		return (-1);
	return (1);
}

int			term_back(t_ui *ui)
{
	termcaps_usual("ve");
	termcaps_usual("ei");
	if (tcsetattr(0, TCSADRAIN, &ui->term_o) == -1)
		return (-1);
	return (1);
}
