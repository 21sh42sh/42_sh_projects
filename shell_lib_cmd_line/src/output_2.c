/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/27 17:38:56 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** Return the line number the str will take on a screen of width line_size
**  and with the element stored in line_ender as line breaking point.
*/

int		get_output_lines_nb(t_ui *ui)
{
	char	*output;
	int		nb;

	output = get_current_output(ui);
	nb = get_str_lines_nb(output, ui->line_ender, WIN_SIZE.x);
	free(output);
	return (nb);
}

/*
** Return the current output based on the activated flags.
*/

char	*get_current_output(t_ui *ui)
{
	char	*output;

	output = lib_strdup("");
	output = lib_strjoinfree(output, PROMPT, 1);
	if (CMDS && CMD_ID >= 0 && CMD)
	{
		output = lib_strjoinfree(output, CMD, 1);
	}
	if (ui->input_mode == IM_DEFAULT)
		return (output);
	if (ui->input_mode == IM_HISTORY_SEARCH)
	{
		output = lib_strjoinfree(output, "\n", 1);
		if (H_SEARCH.error)
			output = lib_strjoinfree(output, H_SEARCH.error_msg, 1);
		output = lib_strjoinfree(output, H_SEARCH.prompt, 1);
		output = lib_strjoinfree(output, H_SEARCH.searching, 1);
	}
	return (output);
}

int		get_prompt_z(t_ui *ui)
{
	return (get_str_lines_nb(PROMPT, ui->line_ender, WIN_SIZE.x));
}
