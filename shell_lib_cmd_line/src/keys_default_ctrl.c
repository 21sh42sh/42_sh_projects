/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_ctrl.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/27 14:42:32 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static int	ctrl_c(t_ui *ui)
{
	if (ui->mode == 2 || ui->mode == 3)
	{
		term_back(ui);
		cmd_line_return_status(0, "Ctrl-C");
		return (ui->mode);
	}
	termcaps_usual("im");
	disp("^C");
	CURSOR_POS.z += 2;
	termcaps_usual("em");
	move_cursor_right_advanced_n(ui,
			input_len(CMD) + PROMPT_LEN - CURSOR_POS.z);
	write(1, "\n", 1);
	lib_arr_del(ui->cmds);
	ui->cmds = lib_arr_dup(ui->cmds_o);
	ui->cmds = lib_arr_push_front(CMDS, "");
	CMD_ID = 0;
	CURSOR_POS.z = 0;
	disp(PROMPT);
	CURSOR_POS.z += PROMPT_LEN;
	disp(CMD);
	CURSOR_POS.z += input_len(CMD);
	H_SEARCH.cmd_id_start = 0;
	return (0);
}

static int	ctrl_d(t_ui *ui)
{
	if (lib_strlen(CMD) != 0)
		return (0);
	if (ui->mode == 2 || ui->mode == 3)
	{
		cmd_line_return_status(0, "Ctrl-D");
		term_back(ui);
	}
	if (ui->mode == 2)
		return (2);
	if (ui->mode == 3)
	{
		return (3);
	}
	termcaps_usual("im");
	disp("^D");
	CURSOR_POS.z += 2;
	termcaps_usual("em");
	move_cursor_right_advanced_n(ui,
			input_len(CMD) + PROMPT_LEN - CURSOR_POS.z);
	term_back(ui);
	exit(0);
	return (1);
}

static int	ctrl_l(t_ui *ui)
{
	termcaps_usual("cl");
	CURSOR_POS.z = 0;
	disp(PROMPT);
	CURSOR_POS.z += PROMPT_LEN;
	disp(CMD);
	CURSOR_POS.z += input_len(CMD);
	return (0);
}

static int	ctrl_r(t_ui *ui)
{
	H_SEARCH.cmd_id_start = CMD_ID;
	ui->input_mode = IM_HISTORY_SEARCH;
	screen_reset(ui);
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int			keys_default_ctrl(t_ui *ui)
{
	if (KEY0 == 3 && KEY1 == 0 && KEY2 == 0)
		return (ctrl_c(ui));
	if (KEY0 == 4 && KEY1 == 0 && KEY2 == 0)
		return (ctrl_d(ui));
	if (KEY0 == 12 && KEY1 == 0 && KEY2 == 0)
		return (ctrl_l(ui));
	if (KEY0 == 18 && KEY1 == 0 && KEY2 == 0)
		return (ctrl_r(ui));
	return (-1);
}
