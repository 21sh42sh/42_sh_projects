/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/21 17:18:47 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		keys_default_std(t_ui *ui)
{
	if (!(KEY0 == 27 && KEY1 == 27 && KEY2 == 91)
			&& KEY1 == 0 && KEY2 == 0
			&& ((32 <= KEY0 && KEY0 <= 126) || KEY0 == '~'))
	{
		if (get_output_line_n_size(ui, CURSOR_POS.y) == WIN_SIZE.x)
		{
			insert_and_reset(ui, CURSOR_POS.z, KEY0);
			return (0);
		}
		CMD = lib_strins_c(CMD, KEY0, CURSOR_POS.z - PROMPT_LEN, 1);
		termcaps_usual("im");
		if (KEY0 == 126)
			write(1, "~", 1);
		else
			write(1, &KEY0, 1);
		CURSOR_POS.z++;
		termcaps_usual("ei");
		cursor_update_xy(ui);
		if (CURSOR_POS.x == 0)
		{
			termcaps_usual("do");
			termcaps_usual("cr");
		}
	}
	return (0);
}

int		keys_default(t_ui *ui)
{
	int	ret;

	if (ui->ignore_next_read)
	{
		ui->ignore_next_read = 0;
		return (0);
	}
	ret = -1;
	if (ret == -1)
		ret = keys_default_cmds(ui);
	if (ret == -1)
		ret = keys_default_cursor_move_left_right(ui);
	if (ret == -1)
		ret = keys_default_del(ui);
	if (ret == -1)
		ret = keys_default_ctrl(ui);
	if (ret == -1)
		ret = keys_default_home_end(ui);
	if (ret == -1 && 1 == 0)
		ret = keys_default_ccp(ui);
	if (ret == -1)
		ret = keys_default_std(ui);
	if (ret == -1)
		ret = 0;
	return (ret);
}
