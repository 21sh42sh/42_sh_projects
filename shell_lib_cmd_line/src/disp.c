/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 14:18:54 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

void		disp_n_fd(char *str, int n, int fd)
{
	char	*print;

	print = lib_strdup(str);
	if (n < (int)lib_strlen(str))
		print[n] = 0;
	while (lib_strstr(print, "\t"))
		print = lib_str_replace(print, "\t", " ");
	lib_putstr_fd(print, fd);
	free(print);
}

void		disp_n(char *str, int n)
{
	disp_n_fd(str, n, 1);
}

void		disp_fd(char *str, int fd)
{
	disp_n_fd(str, lib_strlen(str), fd);
}

void		disp(char *str)
{
	disp_fd(str, 1);
}
