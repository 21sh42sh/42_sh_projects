/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/01 14:04:56 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** return the key that match the position z.
*/

int		get_output_key_from_z(t_ui *ui, int z)
{
	int		i;
	char	*output;

	output = get_current_output(ui);
	i = 0;
	while (z && output && output[i])
	{
		z -= get_char_screen_size(output[i]);
		i++;
	}
	free(output);
	return (i);
}

/*
** return the key that match the position z.
*/

int		get_z_from_output_key(t_ui *ui, int i)
{
	int		z;
	char	*output;

	output = get_current_output(ui);
	z = 0;
	while (z && output && output[i])
	{
		z += get_char_screen_size(output[i]);
		i--;
	}
	return (z);
}

/*
** return the size until the line n in the output on a screen
**  of width line_size and with the element stored in line_ender as line
**  breaking point.
** /!\ include the line_breaking point in the returned value. /!\
*/

int		get_output_char_nb_until_line_n(t_ui *ui, int n)
{
	char	*output;
	int		nb;

	output = get_current_output(ui);
	nb = get_char_nb_until_line_n(output, ui->line_ender, WIN_SIZE.x, n);
	free(output);
	return (nb);
}

/*
** return the character at the position z
*/

char	get_output_char_at_z(t_ui *ui, int z)
{
	char	*output;
	char	c;

	output = get_current_output(ui);
	c = get_str_char_at_z(output, z);
	free(output);
	return (c);
}

/*
** return the size of the line n in the output with a screen
**  of width line_size and with the element stored in line_ender as line
**  breaking point
** lines are counted from 0 to n.
*/

int		get_output_line_n_size(t_ui *ui, int n)
{
	char	*output;
	int		nb;

	output = get_current_output(ui);
	nb = get_str_line_n_size(output, ui->line_ender, WIN_SIZE.x, n);
	free(output);
	return (nb);
}
