/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_ac.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 15:30:27 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static void	move_cursor_end_word(t_ui *ui, char *output)
{
	int		ret;

	ret = 0;
	while (output[CURSOR_POS.z] && !ret && !lib_isspace(output[CURSOR_POS.z]))
		ret = move_cursor_right_advanced(ui, output, 0);
}

static int	keys_auto_completion_ini(t_ui *ui)
{
	char	*output;

	if (!(output = get_current_output(ui)))
		return (1);
	UI_AC.cursor_pos_o = CURSOR_POS.z - PROMPT_LEN;
	move_cursor_end_word(ui, output);
	t_auto_completion_ini(&(UI_AC));
	UI_AC.cmd = lib_strdup(CMD);
	UI_AC.word = get_word_at_n(output, CURSOR_POS.z);
	UI_AC.cursor_pos_m = CURSOR_POS.z - PROMPT_LEN;
	if (!(UI_AC.suggestion = get_suggestion(UI_AC.word)))
	{
		t_auto_completion_del(&(UI_AC));
		ui->input_mode = IM_DEFAULT;
		write(1, "\a", 1);
		return (1);
	}
	UI_AC.suggestion = lib_arr_push_back(UI_AC.suggestion, "");
	UI_AC.s_index = -1;
	ui->input_mode = IM_AUTO_COMPLETION;
	lib_strdel(&output);
	return (0);
}

static void	update_cmd(t_ui *ui)
{
	char	*new_cmd;

	UI_AC.s_index++;
	if (UI_AC.s_index >= (int)lib_arr_len(UI_AC.suggestion))
		UI_AC.s_index = 0;
	new_cmd = lib_strndup(UI_AC.cmd, UI_AC.cursor_pos_m);
	new_cmd = lib_strjoinfree(new_cmd, UI_AC.suggestion[UI_AC.s_index], 1);
	new_cmd = lib_strjoinfree(new_cmd, UI_AC.cmd + UI_AC.cursor_pos_m, 1);
	lib_strdel(&CMD);
	CMD = new_cmd;
}

int			keys_auto_completion(t_ui *ui)
{
	if (LAST_KEY0 != '\t' || LAST_KEY1 != 0 || LAST_KEY2 != 0
			|| !UI_AC.suggestion)
		if (keys_auto_completion_ini(ui))
			return (0);
	if (!(KEY0 == '\t' && KEY1 == 0 && KEY2 == 0))
	{
		t_auto_completion_del(&(UI_AC));
		ui->input_mode = IM_DEFAULT;
		return (keys_default_ccp(ui));
	}
	screen_erase(ui);
	update_cmd(ui);
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, UI_AC.cursor_pos_m
			+ lib_strlen(UI_AC.suggestion[UI_AC.s_index]));
	return (0);
}
