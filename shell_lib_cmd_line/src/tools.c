/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 16:19:05 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** flag == -1: initialize lock.
** flag == 0: save the parameter and ignore the lock parameter.
** flag == 1: change the saved value if not locked and lock.
** flag == 2: unlock. allow to change the saved value at next call.
** 				return the saved value.
** flag == 3: just return the value.
*/

int		ram_save_int(int i, int flag)
{
	static int	tmp;
	static int	lock;

	if (flag == -1)
	{
		lock = 0;
		tmp = 0;
	}
	if (flag == 0 || (flag == 1 && !lock))
	{
		tmp = i;
		lock = 1;
	}
	if (flag == 2)
		lock = 0;
	return (tmp);
}

t_int_3	t_int_3_set(int x, int y, int z)
{
	t_int_3	new;

	new.x = x;
	new.y = y;
	new.z = z;
	return (new);
}
