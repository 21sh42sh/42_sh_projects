/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ac_get_suggestion.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/10 11:39:53 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

#include <libft.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>

int			dyn_get_dirfile_separator(char *s)
{
	int			i;
	struct stat	st;
	char		tmp;

	if ((i = lib_strlen(s)))
	{
		while (i--)
		{
			if (s[i] == '/')
			{
				tmp = s[i + 1];
				s[i + 1] = '\0';
				if (stat(s, &st) == -1)
					return (-1);
				if ((st.st_mode & S_IFDIR) != S_IFDIR || access(s, R_OK) != 0)
					return (-1);
				s[i + 1] = tmp;
				return (i);
			}
		}
		return (0);
	}
	return (-1);
}

static void	lst_actually_remove_non_match(t_list **lst, t_list **prev,
	t_list **beg)
{
	if (*prev)
	{
		(*prev)->next = (*lst)->next;
		free(*lst);
		*lst = (*prev)->next;
	}
	else
	{
		*beg = (*lst)->next;
		free(*lst);
		*lst = *beg;
	}
}

t_list		*lst_remove_non_match(t_list *lst, char *to_complete)
{
	t_list	*beg;
	t_list	*prev;

	beg = lst;
	if (lst && to_complete)
	{
		prev = NULL;
		while (lst)
		{
			if (lib_strncmp(LST_DIRENT->d_name, to_complete,
	lib_strlen(to_complete)) || lib_strncmp(LST_DIRENT->d_name, ".",
	LST_DIRENT->d_namlen) == 0 || lib_strncmp(LST_DIRENT->d_name, "..",
	LST_DIRENT->d_namlen) == 0)
				lst_actually_remove_non_match(&lst, &prev, &beg);
			else
			{
				prev = lst;
				lst = lst->next;
			}
		}
	}
	return (beg);
}
