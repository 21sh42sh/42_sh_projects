/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_cursor_left_right.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/01 14:36:07 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		k_page_up(t_ui *ui)
{
	int	old_pos;

	ui->ignore_next_read = 1;
	if (CURSOR_POS.y < get_str_lines_nb(PROMPT, ui->line_ender, WIN_SIZE.x))
		return (0);
	old_pos = CURSOR_POS.x;
	if ('\n' == get_output_char_at_z(ui,
			get_output_char_nb_until_line_n(ui, CURSOR_POS.y) - 1))
	{
		move_cursor_left_advanced_n(ui, old_pos
				+ get_output_line_n_size(ui, CURSOR_POS.y - 1));
		if (get_output_line_n_size(ui, CURSOR_POS.y) < old_pos)
			move_cursor_right_n(ui,
					get_output_line_n_size(ui, CURSOR_POS.y) - 1, 1);
		else
			move_cursor_right_n(ui, old_pos, 1);
	}
	else
	{
		move_cursor_left_advanced_n(ui, old_pos
				+ get_output_line_n_size(ui, CURSOR_POS.y - 1));
		move_cursor_right_n(ui, old_pos, 1);
	}
	move_cursor_right_n(ui, PROMPT_LEN - CURSOR_POS.z, 1);
	return (0);
}

int		k_page_down(t_ui *ui)
{
	ui->ignore_next_read = 1;
	if (CURSOR_POS.y + 1 >= get_output_lines_nb(ui))
		return (0);
	ram_save_int(CURSOR_POS.x, 1);
	move_cursor_right_advanced_n(ui,
			get_output_line_n_size(ui, CURSOR_POS.y) - CURSOR_POS.x);
	if (get_output_line_n_size(ui, CURSOR_POS.y) < ram_save_int(0, 2))
		move_cursor_right_advanced_n(ui,
			get_output_line_n_size(ui, CURSOR_POS.y));
	else
		move_cursor_right_advanced_n(ui, ram_save_int(0, 2));
	return (0);
}

int		k_left(t_ui *ui)
{
	if (CURSOR_POS.z - PROMPT_LEN > 0)
		move_cursor_left_advanced_n(ui, 1);
	return (0);
}

int		k_right(t_ui *ui)
{
	if (CURSOR_POS.z - PROMPT_LEN < input_len(CMD))
		move_cursor_right_advanced_n(ui, 1);
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int		keys_default_cursor_move_left_right(t_ui *ui)
{
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 68)
		return (k_left(ui));
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 67)
		return (k_right(ui));
	if ((LAST_KEY0 == 27 && LAST_KEY1 == 91 && LAST_KEY2 == 49)
			&& (KEY0 == 59 && KEY1 == 50 && KEY2 == 67))
	{
		move_cursor_right_advanced_n(ui, get_next_word(ui));
		return (0);
	}
	if ((LAST_KEY0 == 27 && LAST_KEY1 == 91 && LAST_KEY2 == 49)
			&& (KEY0 == 59 && KEY1 == 50 && KEY2 == 68))
	{
		move_cursor_left_advanced_n(ui, get_prev_word(ui));
		return (0);
	}
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 53)
		return (k_page_up(ui));
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 54)
		return (k_page_down(ui));
	return (-1);
}
