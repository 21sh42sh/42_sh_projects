/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_history_search_tools.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/28 15:14:52 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		get_correspondance(t_ui *ui, int start)
{
	int	i;

	i = start;
	if (start == -1 || !CMDS[start])
		i = H_SEARCH.cmd_id_start - 1;
	while (CMDS[++i])
	{
		if (lib_strrstr(CMDS[i], H_SEARCH.searching) != NULL)
		{
			CMD_ID = i;
			H_SEARCH.error = 0;
			H_SEARCH.cmd_cursor_pos_l =
					lib_strrstr(CMD, H_SEARCH.searching) - CMD;
			H_SEARCH.cmd_cursor_pos = PROMPT_LEN
					+ input_n_len(CMD, H_SEARCH.cmd_cursor_pos_l);
			return (0);
		}
	}
	H_SEARCH.error = 1;
	return (1);
}
