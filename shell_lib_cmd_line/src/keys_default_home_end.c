/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_home_end.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/01 12:44:58 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int	k_home(t_ui *ui)
{
	if (LAST_KEY0 == 27 && LAST_KEY1 == 0 && LAST_KEY2 == 0)
	{
		screen_reset(ui);
		move_cursor_right_advanced_n(ui,
				input_len(CMD) + PROMPT_LEN - CURSOR_POS.z);
		term_back(ui);
		exit(0);
	}
	move_cursor_left_advanced_n(ui, CURSOR_POS.z - PROMPT_LEN);
	return (0);
}

int	k_end(t_ui *ui)
{
	if (LAST_KEY0 == 27 && LAST_KEY1 == 0 && LAST_KEY2 == 0)
	{
		screen_reset(ui);
		return (0);
	}
	move_cursor_right_advanced_n(ui,
			input_len(CMD) + PROMPT_LEN - CURSOR_POS.z);
	return (0);
}

int	k_shift_home(t_ui *ui)
{
	if (CURSOR_POS.y == 0)
		move_cursor_left_n(ui, CURSOR_POS.x - PROMPT_LEN, 1);
	else
		move_cursor_left_n(ui, WIN_SIZE.x + 1, 1);
	return (0);
}

int	k_shift_end(t_ui *ui)
{
	move_cursor_right_n(ui,
			get_output_line_n_size(ui, CURSOR_POS.y) - CURSOR_POS.x - 1, 1);
	return (0);
}

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

int	keys_default_home_end(t_ui *ui)
{
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 72)
		return (k_home(ui));
	if (KEY0 == 27 && KEY1 == 91 && KEY2 == 70)
		return (k_end(ui));
	if (KEY0 == 59 && KEY1 == 50 && KEY2 == 72)
		return (k_shift_home(ui));
	if (KEY0 == 59 && KEY1 == 50 && KEY2 == 70)
		return (k_shift_end(ui));
	return (-1);
}
