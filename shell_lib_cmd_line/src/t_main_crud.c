/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_main_crud.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 15:31:30 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** Initialize the termios and the prompt.
** return:
** _______ 1 Error while getting termios the first time.
** _______ 2 Error while getting termios the second time.
** _______ 3 Error when trying to modify the termios.
** _______ 4 Error when creating t_auto_comp.
** _______ 0 everything is fine.
*/

int		t_ui_ini_1(t_ui *ui, char *prompt)
{
	if (tcgetattr(0, &ui->term) == -1)
		return (1);
	lib_memcpy(&ui->term_o, &ui->term, sizeof(struct termios));
	if (term_change(ui) == -1)
		return (3);
	if (!lib_strcmp(prompt, "$> "))
	{
		ui->prompt = lib_strjoin(
			get_shell_env_by_name(use_shell_struct(NULL), "PWD"), prompt);
	}
	else
		ui->prompt = lib_strdup(prompt);
	if (t_auto_completion_create(&(ui->auto_comp)))
		return (4);
	ui->select_from = -1;
	ui->select_long = 0;
	ui->ignore_next_read = 0;
	ui->clipboard = lib_strdup("");
	return (0);
}

/*
** Initialize the t_ui structure.
** return:
** _______ 1 Error while getting termios the first time.
** _______ 2 Error while getting termios the second time.
** _______ 3 Error when trying to modify the termios.
** _______ 0 everything is fine.
*/

int		t_ui_ini(t_ui *ui, char *prompt, char **cmds, int mode)
{
	int	ret;

	if ((ret = t_ui_ini_1(ui, prompt)))
		return (ret);
	ui->cmds_o = cmds;
	ui->cmds = lib_arr_dup(cmds);
	ui->cmds = lib_arr_push_front(CMDS, "");
	ui->cmd_id = 0;
	if (!CMD)
		return (4);
	ui->input_mode = IM_DEFAULT;
	if (!(ui->line_ender = lib_strdup("\n")))
		return (5);
	CURSOR_POS.x = 0;
	CURSOR_POS.y = 0;
	CURSOR_POS.z = 0;
	WIN_SIZE_NEW.x = get_win_size(1);
	WIN_SIZE_NEW.y = get_win_size(2);
	WIN_SIZE.x = WIN_SIZE_NEW.x;
	WIN_SIZE.y = WIN_SIZE_NEW.y;
	WIN_SIZE.z = 0;
	LAST_KEY = t_int_3_set(0, 0, 0);
	t_h_search_ini(&ui->h_search, 0);
	ui->mode = mode;
	return (0);
}

void	t_ui_del(t_ui *ui)
{
	t_h_search_del(&H_SEARCH);
	t_auto_completion_destroy(&(ui->auto_comp));
	if (PROMPT)
		free(PROMPT);
	if (ui->cmds)
		lib_arr_del(ui->cmds);
	if (ui->line_ender)
		free(ui->line_ender);
	if (ui->clipboard)
		free(ui->clipboard);
}

void	t_ui_del_all(t_ui *ui)
{
	t_ui_del(ui);
	free(ui);
}
