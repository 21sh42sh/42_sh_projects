/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 21:29:28 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/03 18:13:00 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

void			del_line(t_ui *ui, int n)
{
	int	k;

	k = 0;
	while (k < n)
	{
		termcaps_usual("dl");
		termcaps_usual("up");
		k++;
		CURSOR_POS.y--;
	}
	if (n > 0)
	{
		termcaps_usual("do");
		CURSOR_POS.y++;
	}
}

/*
** flag: 1 => left
** *~note:
** cursor pos is not handle here because you cannot take into concideration
** if the cursor went one line down
*/

void			move_cursor(int n, int flag)
{
	if (flag == 1)
	{
		while (n > 0)
		{
			termcaps_usual("le");
			n--;
		}
	}
	else
	{
		while (n > 0)
		{
			termcaps_usual("nd");
			n--;
		}
	}
}
