/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ac_get_suggestion.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 11:39:53 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

#include <libft.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>

static char	**lst_dirent_to_arr(t_list *lst)
{
	char	**ret;
	int		count;

	ret = NULL;
	if (lst)
	{
		count = lib_lst_len(lst);
		ret = lib_arr_new(count);
		count = 0;
		while (lst)
		{
			ret[count] = lib_strnew(LST_DIRENT->d_namlen +
	(LST_DIRENT->d_type == DT_DIR));
			ret[count] = lib_strncpy(ret[count], LST_DIRENT->d_name,
	LST_DIRENT->d_namlen);
			if (LST_DIRENT->d_type == DT_DIR)
				ret[count][lib_strlen(ret[count])] = '/';
			lst = lst->next;
			count++;
		}
	}
	return (ret);
}

static void	get_matching_entries_loop(t_list **lst, DIR *my_dir)
{
	t_list			*lst_last;
	struct dirent	*curdirent;

	*lst = NULL;
	lst_last = NULL;
	while ((curdirent = readdir(my_dir)))
	{
		if (lst_last)
		{
			lst_last->next = lib_lst_create_elem(curdirent,
	sizeof(struct dirent));
			lst_last = lst_last->next;
		}
		else
		{
			*lst = lib_lst_create_elem(curdirent, sizeof(struct dirent));
			lst_last = *lst;
		}
	}
}

static char	**get_matching_entries(char *s, int separator)
{
	char			**ret;
	t_list			*lst;
	DIR				*my_dir;
	char			tmp;

	ret = NULL;
	if (s && separator >= 0)
	{
		tmp = s[separator + 1];
		s[separator + 1] = '\0';
		if ((my_dir = opendir(s)))
		{
			get_matching_entries_loop(&lst, my_dir);
			s[separator + 1] = tmp;
			lst = lst_remove_non_match(lst, s + separator + 1);
			ret = lst_dirent_to_arr(lst);
			closedir(my_dir);
		}
		s[separator + 1] = tmp;
	}
	return (ret);
}

static void	remove_supplied_part(char **arr, char *str, int separator)
{
	int		len;
	int		i;
	char	*tmp;

	(void)separator;
	if (arr && str && *arr)
	{
		i = 0;
		len = lib_strlen(str + separator + 1);
		while (arr[i] != NULL)
		{
			tmp = arr[i];
			arr[i] = lib_strdup(tmp + len);
			lib_strdel(&tmp);
			i++;
		}
	}
}

char		**get_suggestion(char *str)
{
	char	*tmp;
	char	**ret;
	int		i;

	ret = NULL;
	i = -2;
	if (str && (tmp = lib_strdup(str)) &&
	(i = dyn_get_dirfile_separator(tmp)) != -1)
	{
		if (tmp[0] != '.' && tmp[0] != '/')
		{
			i = 1;
			tmp = lib_strjoinfree("./", tmp, 2);
		}
		ret = get_matching_entries(tmp, i);
		remove_supplied_part(ret, tmp, i);
		lib_strdel(&tmp);
	}
	if (!ret)
		ret = lib_arr_new(0);
	return (ret);
}
