/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_history_search.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/03 16:20:46 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

static void	get_prev(t_ui *ui)
{
	char	*tmp;

	tmp = lib_strnew(H_SEARCH.cmd_cursor_pos + 1);
	tmp = lib_strncpy(tmp, CMD, H_SEARCH.cmd_cursor_pos_l);
	if (lib_strrstr(tmp, H_SEARCH.searching) == NULL)
	{
		H_SEARCH.error = get_correspondance(ui, CMD_ID);
		free(tmp);
		return ;
	}
	H_SEARCH.error = 0;
	H_SEARCH.cmd_cursor_pos_l =
		lib_strrstr(tmp, H_SEARCH.searching) - tmp;
	H_SEARCH.cmd_cursor_pos = PROMPT_LEN
		+ input_n_len(tmp, H_SEARCH.cmd_cursor_pos_l);
	free(tmp);
}

static void	del_spe(t_ui *ui)
{
	if (H_SEARCH.cmd_cursor_pos_l
			< lib_strrstr(CMD, H_SEARCH.searching) - CMD)
	{
		H_SEARCH.error = 0;
		H_SEARCH.cmd_cursor_pos_l = lib_strstr(CMD + H_SEARCH.cmd_cursor_pos_l
				+ lib_strlen(H_SEARCH.searching),
				H_SEARCH.searching) - (CMD);
		H_SEARCH.cmd_cursor_pos = PROMPT_LEN
				+ input_n_len(CMD, H_SEARCH.cmd_cursor_pos_l);
		return ;
	}
	H_SEARCH.searching = lib_strrm_c(H_SEARCH.searching,
			lib_strlen(H_SEARCH.searching) - 1, 1);
	screen_erase(ui);
	H_SEARCH.error = get_correspondance(ui, -1);
}

/*
** return :
** ________ 0 if a match an history mode key
** ________ 1 otherwise
*/

static	int	is_not_history_key(t_ui *ui)
{
	if (KEY1 == 0 && KEY2 == 0
			&& (KEY0 == 27 || KEY0 == 18 || KEY0 == 127))
		return (0);
	if (KEY1 == 0 && KEY2 == 0
			&& 26 <= KEY0 && KEY0 <= 126)
		return (0);
	return (1);
}

static	int	history_search_keys_sub(t_ui *ui)
{
	if (KEY0 == 27 && KEY1 == 0 && KEY2 == 0)
	{
		ui->input_mode = IM_DEFAULT;
		screen_erase(ui);
		t_h_search_ini(&H_SEARCH, 1);
		return (1);
	}
	if (KEY0 == 18 && KEY1 == 0 && KEY2 == 0)
	{
		screen_erase(ui);
		get_prev(ui);
		return (1);
	}
	if (KEY1 == 0 && KEY2 == 0 && 26 <= KEY0 && KEY0 <= 126)
	{
		H_SEARCH.searching = lib_strins_c(H_SEARCH.searching, (char)KEY0,
			lib_strlen(H_SEARCH.searching), 1);
		return (1);
	}
	return (0);
}

int			history_search_keys(t_ui *ui)
{
	if (ui->ignore_next_read)
	{
		ui->ignore_next_read = 0;
		return (0);
	}
	if (history_search_keys_sub(ui))
		;
	if (KEY0 == 127 && KEY1 == 0 && KEY2 == 0)
		del_spe(ui);
	else if (is_not_history_key(ui))
	{
		CURSOR_POS.x = H_SEARCH.cmd_cursor_pos;
		ui->input_mode = IM_DEFAULT;
		screen_reset(ui);
		t_h_search_ini(&H_SEARCH, 1);
		return (keys_default(ui));
	}
	if (KEY1 == 0 && KEY2 == 0 && KEY0 != 18 && KEY0 != 127)
	{
		screen_erase(ui);
		H_SEARCH.error = get_correspondance(ui, -1);
	}
	screen_reset(ui);
	return (0);
}
