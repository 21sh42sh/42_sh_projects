/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys_default_option.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/06 16:16:25 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** return:
** _______ 0 if a button has been detected
** _______ -1 otherwise
*/

static int	ctrl_k(t_ui *ui)
{
	char	*tmp;
	int		pos;
	int		len;

	pos = CURSOR_POS.z;
	screen_erase(ui);
	if (ui->clipboard)
		free(ui->clipboard);
	len = get_output_key_from_z(ui, pos) - PROMPT_LEN;
	ui->clipboard = lib_strdup(CMD + len);
	tmp = lib_strnew(len);
	tmp = lib_strncpy(tmp, CMD, len);
	free(CMD);
	CMD = tmp;
	screen_reset(ui);
	move_cursor_right_advanced_n(ui, lib_strlen(CMD));
	return (0);
}

static int	ctrl_y(t_ui *ui)
{
	int		pos;
	int		i;
	char	*tmp;

	if (!ui->clipboard)
		return (0);
	pos = CURSOR_POS.z;
	i = get_output_key_from_z(ui, pos);
	screen_erase(ui);
	tmp = lib_strnew(i - PROMPT_LEN + 1);
	tmp = lib_strncpy(tmp, CMD, i - PROMPT_LEN);
	tmp = lib_strjoinfree(tmp, ui->clipboard, 1);
	tmp = lib_strjoinfree(tmp, CMD + i - PROMPT_LEN, 1);
	free(CMD);
	CMD = tmp;
	screen_reset(ui);
	move_cursor_right_advanced_n(ui,
			pos + lib_strlen(ui->clipboard) - PROMPT_LEN);
	return (0);
}

static int	ctrl_g(t_ui *ui)
{
	ui->ignore_next_read = 1;
	if (ui->clipboard)
		free(ui->clipboard);
	ui->clipboard = lib_strdup(CMD
			+ get_output_key_from_z(ui, CURSOR_POS.z) - PROMPT_LEN);
	return (0);
}

int			keys_default_ccp(t_ui *ui)
{
	if ((KEY0 == 7 && KEY1 == 0 && KEY2 == 0))
		return (ctrl_g(ui));
	if ((KEY0 == 11 && KEY1 == 0 && KEY2 == 0))
		return (ctrl_k(ui));
	if ((KEY0 == 25 && KEY1 == 0 && KEY2 == 0))
		return (ctrl_y(ui));
	if (1 == 0 && ((KEY0 == 27 && KEY1 == 27 && KEY2 == 91)
			|| (KEY0 == 7 && KEY1 == 0 && KEY2 == 0)
			|| (KEY0 == 11 && KEY1 == 0 && KEY2 == 0)))
	{
		ui->input_mode = IM_COPY_CUT_PASTE;
		return (keys_copy_cut_paste(ui));
	}
	return (-1);
}
