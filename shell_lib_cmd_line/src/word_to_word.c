/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   word_to_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 22:12:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/03 18:05:19 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

int		get_next_word(t_ui *ui)
{
	int	i;
	int	z;

	i = get_output_key_from_z(ui, CURSOR_POS.z) - PROMPT_LEN;
	z = 0;
	while (i >= 0 && CMD && CMD[i] && !lib_isspace(CMD[i]))
	{
		z += get_char_screen_size(CMD[i]);
		i++;
	}
	while (i >= 0 && CMD && CMD[i] && lib_isspace(CMD[i]))
	{
		z += get_char_screen_size(CMD[i]);
		i++;
	}
	return (z);
}

int		get_prev_word(t_ui *ui)
{
	int	i;
	int	z;

	i = get_output_key_from_z(ui, CURSOR_POS.z) - PROMPT_LEN - 1;
	z = 0;
	while (i >= 0 && CMD && CMD[i] && !lib_isspace(CMD[i]))
	{
		z += get_char_screen_size(CMD[i]);
		i--;
	}
	while (i >= 0 && CMD && CMD[i] && lib_isspace(CMD[i]))
	{
		z += get_char_screen_size(CMD[i]);
		i--;
	}
	while (i >= 0 && CMD && CMD[i] && !lib_isspace(CMD[i]))
	{
		z += get_char_screen_size(CMD[i]);
		i--;
	}
	if (i > 0)
		i--;
	return (z);
}
