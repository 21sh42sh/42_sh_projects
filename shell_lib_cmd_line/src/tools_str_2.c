/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_str_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 19:09:49 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/10 13:17:55 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cmd_line.h"

/*
** return the size of the line n in the str when it will be put on a screen
**  of width line_size and with the element stored in line_ender as line
**  breaking point
** lines are counted from 0 to n.
** parameters:
** ___________ l_size positive or zero.
** ___________ n positive or zero.
*/

int		get_str_line_n_size(char *str, char *line_ender, int l_size, int n)
{
	int	size;
	int	i;
	int	nb_line;

	size = 0;
	nb_line = -1;
	i = 0;
	while (str && str[i] && n >= 0)
	{
		if (((l_size > 0 && size >= l_size)
			|| (i > 0 && is_line_ender(line_ender, str[i - 1]))))
		{
			nb_line++;
			if (nb_line >= n)
				return (size);
			size = 1;
		}
		else
			size += get_char_screen_size(*str);
		i++;
	}
	if (nb_line >= n - 1)
		return (size);
	return (0);
}

/*
** return the line number the str will take on a screen of width line_size
**  and with the element stored in line_ender as line breaking point
*/

int		get_str_lines_nb(char *str, char *line_ender, int line_size)
{
	int	k;
	int	nb_line;

	if (!str || !*str)
		return (0);
	nb_line = 1;
	k = 0;
	while (str && *str)
	{
		k += get_char_screen_size(*str);
		if ((*(str + 1) && line_size > 0 && k >= line_size)
			|| is_line_ender(line_ender, *str))
		{
			nb_line++;
			k = 0;
		}
		str += 1;
	}
	return (nb_line);
}

/*
** return the word pointed by the n position or one char before.
*/

char	*get_word_at_n(char *str, int n)
{
	char	*word;
	int		i;
	size_t	beginning;
	size_t	len;

	len = lib_strlen(str);
	if (!str || n < 0 || n > (int)len)
		return (NULL);
	i = n;
	while (i >= 0 && (str[i] == ' ' || str[i] == '\n'
			|| str[i] == '\t' || str[i] == '\r'))
		i--;
	while (i >= 0 && str[i] != ' ' && str[i] != '\n'
			&& str[i] != '\t' && str[i] != '\r')
		i--;
	i++;
	beginning = (size_t)i;
	while (i < (int)len && str[i] != ' ' && str[i] != '\n'
			&& str[i] != '\t' && str[i] != '\r')
		i++;
	word = lib_strndup(str + beginning, (size_t)i - beginning);
	return (word);
}
