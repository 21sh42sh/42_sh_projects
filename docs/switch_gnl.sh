#!/bin/bash

if [ $# -eq 2 ]; then
	PATH="$2"
else
	PATH="."
fi
GNL_PATH=$PATH/"../libft/src/lib_gnl"
SRC=$PATH/"gnl_src"
GNL_NAME="get_next_line.c"

if [ $# -eq 0 ]; then
	echo "USAGE : $0 [01]"
	echo "arg 1:"
	echo "_____ 0 : Turn -g off"
	echo "_____ 1 : Turn -g on"
	echo "arg 1:"
	echo "_____ nothing or the path to the switch_gnl.sh file."
	exit
elif [ $1 -eq 1 ]; then
	echo "Switching gnl in $GNL_PATH to gnl_multi_fd"
	/bin/cp $SRC/gnl_multi_fd $GNL_PATH/$GNL_NAME
elif [ $1 -eq 0 ]; then
	echo "Switching gnl in $GNL_PATH to gnl_genuine"
	/bin/cp $SRC/gnl_genuine $GNL_PATH/$GNL_NAME
fi
