#!/bin/sh
files=""
len=$(($#-1))
array=${@:1:$len}
last=${!#}
for arg in $array;do
	files="$files $arg"
done
head_id=$(echo $last|grep -oE "[a-z_]+\.h")
if [ -z $head_id ]; then
	echo "Last argument is not .h\nQuitting"
	exit
fi
head_id=$(echo $head_id|sed "s/\./_/"|tr '[:lower:]' '[:upper:]'|tr '.' '_')
if [ -f $last ]; then
	sed -i ".old" -E "/^[a-z_]+[[:space:]]+[*]*[a-z_]+\(.*\)\;$/d; /^#endif$/d" $last
else
	echo "#ifndef $head_id\n# define $head_id\n" > $last
fi
grep -hE "^[a-z_]+[[:space:]]+[*]*[a-z_]+\(.*\)$"$files|sed -E "/^int[[:space:]]+main[[:space:]]*\(.*\)$/d"|sed "s/$/\;/"|sed -E "s/[[:space:]]+/	/"|sed -E "s/^[a-z_]{2,3}	/&	/">> $last
echo "\n#endif" >> $last