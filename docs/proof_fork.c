#include <stdlib.h>
#include <unistd.h>

/*
** Test to show that fork(2) is leaking if no execve is executed
** Compile using : gcc proof_fork.c -o proof_fork
** Run using : valgrind --track-origins=yes --tool=memcheck --leak-check=full proof_fork
*/

int main(int argc, char const *argv[])
{
	char *s;
	pid_t pid;

	s = (char *)malloc(100);
	pid = fork();
	free (s);
	if (pid)
		sleep(5);
	return 0;
}