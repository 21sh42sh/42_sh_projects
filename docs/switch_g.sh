#!/bin/bash

BASENAME=$(dirname "$0")
MAKEFILES=$(find "$BASENAME/.." -type f -name "Makefile")

if [ $# -eq 0 ]
then
	echo "USAGE : $0 [01]"
	echo "0 : Turn -g off"
	echo "1 : Turn -g on"
	exit
elif [ $1 -eq 1 ]
then
	echo "Turn it on"
	sed -i.bak "s/^\(.*-Wextra.*\)$/\1 -g/g" $MAKEFILES
elif [ $1 -eq 0 ]
then
	echo "Turn it off"
	sed -i.bak "s/^\(.*\)\(-g\)\(.*\)$/\1\3/g" $MAKEFILES
fi
