#!/bin/bash

BASENAME=$(dirname "$0")
MAKEFILES=$(find "$BASENAME/.." -type f -name "Makefile")

if [ $# -eq 0 ]
then
	echo "USAGE : $0 [01]"
	echo "0 : Turn fsanitize off"
	echo "1 : Turn fsanitize on"
	exit
elif [ $1 -eq 1 ]
then
	echo "Turn it on"
	sed -i.bak "s/^\(.*-Wextra.*\)$/\1 -fsanitize=address/g" $MAKEFILES
elif [ $1 -eq 0 ]
then
	echo "Turn it off"
	sed -i.bak "s/^\(.*\)\(-fsanitize=address\)\(.*\)$/\1\3/g" $MAKEFILES
fi
