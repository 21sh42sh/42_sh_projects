/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operator.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 16:19:20 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/02 16:22:44 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATOR_H
# define OPERATOR_H

# include "libft.h"
# include "shell_wrap.h"

int		exec_pipe(t_command *cmd);
void	do_heredoc(t_command *cmd, char *heredoc);
int		exe_shell_bltn(t_command *cmd);

#endif
