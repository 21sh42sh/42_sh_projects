/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ope_pipe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/01 23:56:32 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/02 17:35:00 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operator.h"
#include <shell_wrap.h>

static void	kill_zombies(t_command *init_cmd)
{
	while (init_cmd)
	{
		if (init_cmd->pid > 0)
			kill(init_cmd->pid, SIGKILL);
		wait(NULL);
		init_cmd = init_cmd->right;
	}
}

static void	pipe_sub(t_command *cmd, int *p, int fd_in, int *my_fds)
{
	if (is_cmd_valid(cmd) == 1)
	{
		if (cmd->heredoc && (my_fds = lib_memalloc(sizeof(int) * 2)))
			pipe(my_fds);
		if ((cmd->pid = fork()) == -1)
			exit(EXIT_FAILURE);
		else if (cmd->pid == 0)
		{
			dup2(fd_in, 0);
			if (cmd->right && cmd->right->type == OP_PIPE)
				dup2(p[1], 1);
			close(p[0]);
			if (!exe_shell_bltn(cmd))
				exec_cmds(cmd, my_fds);
			del_shell(use_shell_struct(NULL));
			exit(EXIT_FAILURE);
		}
		else if (my_fds && cmd->pid != -1)
		{
			close(my_fds[0]);
			lib_putstr_fd(cmd->heredoc, my_fds[1]);
			close(my_fds[1]);
			free(my_fds);
		}
	}
}

int			exec_pipe(t_command *cmd)
{
	int			p[2];
	t_command	*init_cmd;
	int			fd_in;

	fd_in = cmd->fd[0];
	init_cmd = cmd;
	while (cmd)
	{
		pipe(p);
		pipe_sub(cmd, p, fd_in, NULL);
		close(p[1]);
		fd_in = p[0];
		if (cmd->right && cmd->right->type == OP_PIPE)
			cmd = cmd->right;
		else
		{
			waitpid(cmd->pid, NULL, 0);
			kill_zombies(init_cmd);
			cmd->pid = 0;
			break ;
		}
	}
	return (0);
}
