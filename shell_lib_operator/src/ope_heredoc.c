/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ope_heredoc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 06:23:25 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/29 18:23:33 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <unistd.h>
#include <shell_wrap.h>

void	do_heredoc(t_command *cmd, char *heredoc)
{
	pid_t	pid;
	int		fds[2];

	lib_putendl_fd(cmd->options[0], 2);
	pipe(fds);
	if ((pid = fork()) > 0)
	{
		close(fds[0]);
		lib_putstr_fd(heredoc, fds[1]);
		close(fds[1]);
		waitpid(pid, NULL, 0);
	}
	else if (pid == 0)
	{
		close(fds[1]);
		dup2(fds[0], 0);
		execve(cmd->options[0], cmd->options, cmd->sh->env);
		lib_putendl_fd("heredoc error", 2);
		exit(-1);
	}
}
