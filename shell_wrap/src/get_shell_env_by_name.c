/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_shell_env_by_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 15:07:22 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:53:15 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

char	*get_shell_env_by_name(t_shell *sh, char *name)
{
	char	*ret;
	size_t	i;

	ret = NULL;
	if (sh && name ? sh->env : NULL)
	{
		i = 0;
		if ((name = lib_strjoin(name, "=")))
		{
			while (sh->env[i] ? lib_strncmp(sh->env[i], name,
	lib_strlen(name)) : 0)
				i++;
			if (sh->env[i])
				ret = sh->env[i] + lib_strlen(name);
			free(name);
		}
	}
	return (ret);
}
