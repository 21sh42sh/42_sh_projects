/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal_handling.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 00:47:49 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/18 15:42:24 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <signal.h>
#include <shell_wrap.h>

void	sigint_handle(int sig)
{
	(void)sig;
	if (isatty(0))
	{
		lib_putstr("\n");
	}
	else
	{
		del_shell(use_shell_struct(NULL));
		exit(1);
	}
}

/*
** Signal caught to be ignored by valgrind
** System's default action is to discard the signal
** see manpage : signal(2)
*/

void	sigio_handle(int sig)
{
	(void)sig;
}

void	sig_handler_init(void)
{
	signal(SIGINT, sigint_handle);
	signal(SIGIO, sigio_handle);
}
