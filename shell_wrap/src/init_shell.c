/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_shell.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 14:40:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/22 21:30:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell_wrap.h"
#include "builtin.h"

static void	init_env(t_shell *sh)
{
	char buf[1024];
	char *tmp;
	char *nb;

	if (sh)
	{
		if (!sh->env)
			lib_memalloc(sizeof(char *) * 2);
		if ((tmp = get_shell_env_by_name(sh, "SHLVL")))
		{
			set_shell_env_by_name(sh, "SHLVL", (nb = lib_itoa(lib_atoi(tmp)
	+ 1)));
			if (nb)
				free(nb);
		}
		else
			set_shell_env_by_name(sh, "SHLVL", "1");
		lib_bzero(buf, 1024);
		sh->native_folder = lib_strdup(getcwd(buf, 1024));
		if (!get_shell_env_by_name(sh, "PWD"))
			set_shell_env_by_name(sh, "PWD", sh->native_folder);
		set_shell_env_by_name(sh, "HIST_SIZE", "100");
		set_shell_env_by_name(sh, "HIST_FILE", "/tmp/.42_history");
	}
}

static char	**envcpy(void)
{
	extern char	**environ;
	char		**new_env;

	new_env = lib_arr_dup(environ);
	return (new_env);
}

static void	init_builtin(t_shell *sh)
{
	if (sh ? sh->fn_bltns : NULL)
	{
		sh->fn_bltns[0] = posix_bltn_cd;
		sh->fn_bltns[1] = bltn_env;
		sh->fn_bltns[2] = bltn_setenv;
		sh->fn_bltns[3] = bltn_unsetenv;
		sh->fn_bltns[4] = bltn_echo;
		sh->fn_bltns[5] = bltn_exit;
		sh->fn_bltns[6] = bltn_history;
		sh->fn_bltns[7] = bltn_where;
		sh->fn_bltns[8] = bltn_which;
	}
}

void		load_alias(t_shell *sh)
{
	int		fd;
	int		ret;
	char	*line;
	char	*tmp;
	char	**alias;

	if (1)
		return ;
	alias = NULL;
	tmp = lib_strjoin(get_shell_env_by_name(sh, "HOME"), "/.myalias");
	if (tmp && !access(tmp, R_OK) && (fd = open(tmp, O_RDONLY)) > 0)
	{
		line = NULL;
		while ((ret = get_next_line(fd, &line)) > 0)
		{
			if (lib_strstr(line, "alias ") == line && is_good_alias(line + 6))
				alias = bltn_alias_set(alias, line + 6);
		}
		if (ret == -1 || close(fd) == -1)
			return ;
	}
	if (tmp)
		free(tmp);
	if (tmp && fd == -1)
		return ;
}

int			init_shell(t_shell *sh)
{
	if (sh)
	{
		sh->env = envcpy();
		init_env(sh);
		sh->fn_bltns = lib_memalloc(sizeof(t_builtin) * (NB_BUILTINS + 1));
		init_builtin(sh);
		sh->bltns = lib_strsplit(
	"cd;env;setenv;unsetenv;echo;exit;history;where;which", ';');
		sh->cmds = NULL;
		sh->history = history_get(sh, NULL);
		sh->history_cmd_nb = 0;
		sh->history_file_nb_line = lib_arr_len(sh->history);
		load_alias(sh);
		sig_handler_init();
		return (sh->env == NULL);
	}
	return (-1);
}
