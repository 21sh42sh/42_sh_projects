/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_shell_history.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:16:37 by hlequien          #+#    #+#             */
/*   Updated: 2016/06/02 16:20:21 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

void	add_shell_history(t_shell *sh, char *cmd_line)
{
	char **old;

	if (sh && cmd_line ? sh->history : NULL)
	{
		old = sh->history;
		sh->history = lib_arr_push_back(sh->history, cmd_line);
		free(old);
	}
}
