/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_singleton.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 01:20:44 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:55:35 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>

t_shell	*use_shell_struct(t_shell *sh)
{
	static t_shell *my_sh;

	if (sh)
		my_sh = sh;
	return (my_sh);
}
