/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   del_shell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 11:51:20 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/25 20:26:49 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

void	del_fd(t_fd *fd)
{
	if (fd->name)
	{
		free(fd->name);
		fd->name = NULL;
	}
}

void	del_all_cmds(t_command *cmd)
{
	if (cmd)
	{
		if (cmd->left)
			del_all_cmds(cmd->left);
		if (cmd->right)
			del_all_cmds(cmd->right);
		if (cmd->options)
			lib_arr_del(cmd->options);
		if (cmd->heredoc)
			free(cmd->heredoc);
		if (cmd->sh->cmds == cmd)
			cmd->sh->cmds = NULL;
		if (cmd->pid)
			kill(cmd->pid, SIGSTOP);
		del_fd(&(cmd->fds[0]));
		del_fd(&(cmd->fds[1]));
		del_fd(&(cmd->fds[2]));
		free(cmd);
	}
}

void	del_shell(t_shell *sh)
{
	if (sh)
	{
		history_save(sh->history, NULL);
		if (sh->env)
			lib_arr_del(sh->env);
		if (sh->fn_bltns)
			free(sh->fn_bltns);
		if (sh->bltns)
			lib_arr_del(sh->bltns);
		if (sh->history)
			lib_arr_del(sh->history);
		if (sh->alias)
			lib_arr_del(sh->alias);
		if (sh->native_folder)
			free(sh->native_folder);
		if (sh->cmds)
			del_all_cmds(sh->cmds);
		free(sh);
	}
}
