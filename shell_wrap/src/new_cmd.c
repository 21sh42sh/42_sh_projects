/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_cmd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 16:12:36 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/17 15:48:21 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

t_command	*new_cmd(t_shell *sh)
{
	t_command *cmd;

	if ((cmd = lib_memalloc(sizeof(t_command))))
	{
		cmd->fd[0] = 0;
		cmd->fd[1] = 1;
		cmd->fd[2] = 2;
		cmd->sh = sh;
		cmd->heredoc = NULL;
	}
	return (cmd);
}
