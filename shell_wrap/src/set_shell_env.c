/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_shell_env.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 15:12:31 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/29 17:04:14 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell_wrap.h>

static char	**st_arr_add(char **ft_array, char *value)
{
	char	**ret;
	size_t	i;

	ret = (char **)lib_memalloc(sizeof(char *) * (lib_arr_len(ft_array) + 2));
	ret = lib_arr_cpy(ret, ft_array);
	i = 0;
	while (ret[i])
		i++;
	ret[i] = value;
	lib_arr_del(ft_array);
	return (ret);
}

void		set_shell_env_by_name(t_shell *sh, char *name, char *value)
{
	size_t i;

	if (sh && name)
	{
		if (!sh->env)
			sh->env = (char **)lib_memalloc(sizeof(char *) * 2);
		name = lib_strjoin(name, "=");
		i = 0;
		while (sh->env[i] && lib_strncmp(name, sh->env[i], lib_strlen(name)))
			i++;
		if (sh->env[i])
		{
			free(sh->env[i]);
			sh->env[i] = lib_strjoin(name, value);
		}
		else
		{
			sh->env = st_arr_add(sh->env, lib_strjoin(name, value));
		}
		if (name)
			free(name);
	}
}
