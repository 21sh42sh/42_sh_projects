/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_path_handling.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/04 18:36:39 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/05 16:35:52 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell_wrap.h>
#include <sys/stat.h>

char	*find_cmd_in_path(t_command *cmd)
{
	char		*ret;
	size_t		i;
	char		**path;
	struct stat	st;

	ret = NULL;
	if (cmd ? cmd->sh : NULL)
		if (cmd->sh->env)
		{
			if ((path = lib_strsplit(get_shell_env_by_name(cmd->sh,
	"PATH"), ':')))
			{
				i = 0;
				while (path[i] && !ret)
				{
					ret = lib_strjoinfree(lib_strjoinfree(path[i++], "/", 0),
	cmd->options[0], 1);
					if (stat(ret, &st) == -1)
						lib_strdel(&ret);
				}
			}
			if (path)
				lib_arr_del(path);
		}
	return (ret);
}

int		is_cmd_option_path(t_command *cmd)
{
	if (cmd ? cmd->options : NULL)
	{
		if (cmd->options[0][0] == '/' || !lib_strncmp(cmd->options[0], "./", 2)
	|| !lib_strncmp(cmd->options[0], "../", 2))
			return (1);
		return (0);
	}
	return (-1);
}
