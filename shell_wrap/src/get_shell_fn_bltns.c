/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_shell_fn_bltns.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 15:31:02 by hlequien          #+#    #+#             */
/*   Updated: 2016/06/02 15:31:05 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

t_builtin	get_shell_fn_bltns(t_shell *sh, char *name)
{
	size_t i;

	if (sh && name ? sh->fn_bltns && sh->bltns : 0)
	{
		i = 0;
		while (sh->bltns[i] && sh->fn_bltns[i])
		{
			if (lib_strcmp(sh->bltns[i], name) == 0)
				return (sh->fn_bltns[i]);
			i++;
		}
	}
	return (NULL);
}
