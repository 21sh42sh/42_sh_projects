/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork_execve_wrap.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/04 18:20:05 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/02 17:29:04 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell_wrap.h>

static void	on_fork_child(t_command *cmd, int fds[2])
{
	char	*path;

	do_exec_redir(cmd);
	if (is_cmd_option_path(cmd) > 0)
		path = cmd->options[0];
	else
		path = find_cmd_in_path(cmd);
	if (path)
	{
		if (cmd->heredoc)
		{
			close(fds[1]);
			dup2(fds[0], 0);
		}
		execve(path, cmd->options, cmd->sh->env);
		lib_putendl_fd("42sh: Execve failed.", 2);
	}
	else
	{
		lib_putstr_fd("42sh: command not found: ", 2);
		lib_putendl_fd(cmd->options[0], 2);
	}
	del_shell(cmd->sh);
	exit(-1);
}

static int	on_fork_parent(t_command *cmd, int fds[2], pid_t pid, int *stat)
{
	if (cmd->heredoc)
	{
		close(fds[0]);
		lib_putstr_fd(cmd->heredoc, fds[1]);
		close(fds[1]);
	}
	if (cmd->right == NULL)
	{
		waitpid(pid, stat, 0);
		cmd->pid = 0;
	}
	else
		return (1);
	return (0);
}

static int	on_fork_fail(t_command *cmd, int fds[2])
{
	lib_putendl_fd("42sh: fork failed", 2);
	if (cmd->heredoc)
	{
		close(fds[0]);
		close(fds[1]);
	}
	return (1);
}

int			is_cmd_valid(t_command *cmd)
{
	char	*path;

	if (cmd ? cmd->options && cmd->sh : 0)
	{
		if (get_shell_fn_bltns(cmd->sh, cmd->options[0]))
			return (1);
		if (is_cmd_option_path(cmd))
		{
			if (access(cmd->options[0], X_OK) == 0)
				return (1);
		}
		else if ((path = find_cmd_in_path(cmd)))
		{
			free(path);
			return (1);
		}
		lib_putstr_fd("42sh: not a command :", 2);
		lib_putendl_fd(cmd->options[0], 2);
	}
	return (-1);
}

int			fork_execve_wrap(t_command *cmd, int *my_fds)
{
	int		ret;
	int		st;
	int		fds[2];

	ret = -1;
	if (cmd ? (ret = is_cmd_valid(cmd)) > 0 : 0)
	{
		lib_bzero(fds, sizeof(int) * 2);
		if (cmd->heredoc && !my_fds)
			pipe(fds);
		else if (cmd->heredoc && my_fds)
			lib_memcpy(fds, my_fds, sizeof(int) * 2);
		cmd->pid = 0;
		if (cmd->type != OP_PIPE && cmd->right == NULL)
			cmd->pid = fork();
		if (cmd->pid == -1)
			return (on_fork_fail(cmd, fds));
		else if (cmd->pid)
			on_fork_parent(cmd, fds, cmd->pid, &st);
		else
			on_fork_child(cmd, fds);
		ret = WEXITSTATUS(st);
	}
	return (ret == -1 ? 1 : ret);
}
