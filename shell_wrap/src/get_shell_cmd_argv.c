/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_shell_cmd_argv.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 17:12:54 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:45:14 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

char	**get_shell_cmd_argv(t_shell *sh)
{
	if (sh ? sh->cmds : NULL)
		return (sh->cmds->options);
	return (NULL);
}
