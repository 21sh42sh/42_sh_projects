/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history_handler.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 15:31:02 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 16:22:19 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>

static char	**read_file(int fd)
{
	char	*line;
	char	**new_history;

	new_history = NULL;
	while (get_next_line(fd, &line) > 0)
	{
		new_history = lib_arr_push_front(new_history, line);
		lib_strdel(&line);
	}
	return (new_history);
}

char		**history_get(t_shell *sh, char *file)
{
	char	*history_file;
	char	**new_history;
	int		fd;

	if (!sh)
		sh = use_shell_struct(NULL);
	if (file)
		history_file = file;
	else if (!(history_file = get_shell_env_by_name(sh, "HIST_FILE")))
		return (NULL);
	if ((fd = open(history_file, O_RDONLY, S_IWUSR | S_IRUSR)) < 0)
	{
		lib_putstr_fd("42sh: Unable to load file as history: ", 2);
		lib_putendl_fd(history_file, 2);
		return (NULL);
	}
	new_history = read_file(fd);
	close(fd);
	sh->history_file_nb_line = lib_arr_len(new_history);
	return (new_history);
}

void		history_save(char **history, char *file)
{
	t_shell	*sh;
	char	*hist_file;
	int		fd;
	int		i;

	if (!history)
		return ;
	sh = use_shell_struct(NULL);
	if (file)
		fd = open(file, O_WRONLY | O_TRUNC | O_CREAT, S_IWUSR | S_IRUSR);
	else if ((hist_file = get_shell_env_by_name(sh, "HIST_FILE")))
		fd = open(hist_file, O_WRONLY | O_TRUNC | O_CREAT, S_IWUSR | S_IRUSR);
	else
		return ;
	if (fd < 0)
		return ;
	i = (int)lib_arr_len(history) - 1;
	while (i > 0 && history[i])
	{
		lib_putendl_fd(history[i], fd);
		i--;
	}
	close(fd);
}
