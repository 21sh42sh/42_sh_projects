/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_shell_cmds.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 17:37:20 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/08 14:58:00 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>
#include <sys/wait.h>

int					do_exec_redir(t_command *cmd)
{
	int	ret;
	int	i;

	ret = 0;
	if (cmd)
	{
		cmd_do_redir(cmd);
		i = 0;
		while (i < 3)
		{
			if (cmd->fd[i] != i)
			{
				if (cmd->fd[i] == 1000000)
					close(i);
				else
					ret = dup2(cmd->fd[i], i);
			}
			i++;
		}
	}
	return (ret);
}

int					exec_cmds(t_command *cmd, int *fds)
{
	int ret;

	ret = -1;
	if (cmd)
		ret = fork_execve_wrap(cmd, fds);
	return (ret);
}

int					exec_shell_cmds(t_shell *sh)
{
	int ret;

	ret = -1;
	if (sh ? sh->cmds : NULL)
		ret = exec_cmds(sh->cmds, NULL);
	return (ret);
}

static t_command	*st_new_cmd(t_shell *sh, char *s)
{
	t_command *cmd;

	cmd = NULL;
	if (s)
	{
		if ((cmd = lib_memalloc(sizeof(t_command))))
		{
			cmd->options = lib_strsplit(s, ' ');
			cmd->sh = sh;
			cmd->fd[1] = 1;
		}
	}
	return (cmd);
}

int					exec_cmd_from_str(t_shell *sh, char *s)
{
	t_command	*cmd;
	int			ret;

	if (s && sh)
	{
		cmd = sh->cmds;
		sh->cmds = st_new_cmd(sh, s);
		ret = exec_shell_cmds(sh);
		del_all_cmds(sh->cmds);
		sh->cmds = cmd;
		return (ret);
	}
	return (-1);
}
