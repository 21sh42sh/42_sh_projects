/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dup_shell_env_by_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 16:22:50 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:43:27 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>

char	*dup_shell_env_by_name(t_shell *sh, char *name)
{
	char *ret;

	ret = get_shell_env_by_name(sh, name);
	if (ret)
		return (lib_strdup(ret));
	return (NULL);
}
