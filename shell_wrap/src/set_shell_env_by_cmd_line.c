/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_shell_env_by_cmd_line.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 17:17:24 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:55:13 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

void	set_shell_env_by_cmd_line(t_shell *sh, char *cmd)
{
	char *ft_array[2];
	char *separator;

	if (sh && cmd)
	{
		if ((separator = lib_strchr(cmd, '=')))
		{
			ft_array[0] = lib_strnew(separator - cmd);
			lib_strncpy(ft_array[0], cmd, separator - cmd);
			ft_array[1] = lib_strdup(separator + 1);
			set_shell_env_by_name(sh, ft_array[0], ft_array[1]);
			free(ft_array[0]);
			free(ft_array[1]);
		}
	}
}
