/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_shell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 17:54:46 by hlequien          #+#    #+#             */
/*   Updated: 2016/06/02 15:32:33 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>

t_shell	*new_shell(void)
{
	t_shell *ret;

	if ((ret = (t_shell *)lib_memalloc(sizeof(t_shell))))
		if (init_shell(ret) != 0)
		{
			free(ret);
			return (NULL);
		}
	return (ret);
}
