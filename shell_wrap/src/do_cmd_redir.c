/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_cmd_redir.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/17 16:51:53 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/17 16:56:24 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>

void	cmd_do_redir(t_command *cmd)
{
	int i;

	if (cmd)
	{
		i = 0;
		while (i < 3)
		{
			if (cmd->fds[i].name && cmd->fd[i] == i)
				cmd->fd[i] = open(cmd->fds[i].name, cmd->fds[i].flags, 0666);
			i++;
		}
	}
}
