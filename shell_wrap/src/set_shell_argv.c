/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_shell_argv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/03 16:23:03 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:54:33 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

void	test_set_shell_argv_from_cmd_line(t_shell *sh, char *s)
{
	if (sh && s)
	{
		if (!sh->cmds)
		{
			sh->cmds = (t_command *)lib_memalloc(sizeof(t_command));
			sh->cmds->sh = sh;
		}
		sh->cmds->options = lib_strsplit(s, ' ');
	}
}
