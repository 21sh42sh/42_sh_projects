/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_shell_argv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/03 16:18:58 by hlequien          #+#    #+#             */
/*   Updated: 2016/06/03 16:21:48 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

char	**get_shell_argv(t_shell *sh)
{
	if (sh ? sh->cmds : NULL)
		return (sh->cmds->options);
	return (NULL);
}
