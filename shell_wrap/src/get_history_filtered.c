/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_history_filtered.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:21:35 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/04 15:44:52 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <libft.h>

static int	sh_filter_hist(char *s1, char *s2)
{
	while (*s1 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 == *s2 ? 1 : *s1 && !*s2);
}

char		**get_shell_history_filtered(t_shell *sh, char *starting)
{
	char **ret;

	ret = NULL;
	if (sh && starting ? sh->history : NULL)
		ret = lib_arr_filter(sh->history, starting, sh_filter_hist);
	return (ret);
}
