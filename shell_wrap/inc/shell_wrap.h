/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_wrap.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 16:14:12 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/02 17:25:43 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_WRAP_H
# define SHELL_WRAP_H

# include "libft.h"
# include "builtin.h"
# include "shell_wrap_struct.h"

void		history_save(char **history, char *file);
char		**history_get(t_shell *sh, char *file);
char		*get_shell_env_by_name(t_shell *sh, char *name);
t_builtin	get_shell_fn_bltns(t_shell *sh, char *name);
int			init_shell(t_shell *sh);
t_shell		*new_shell(void);
void		set_shell_env_by_name(t_shell *sh, char *name, char *value);
void		del_shell(t_shell *sh);
char		**get_shell_cmd_argv(t_shell *sh);
void		set_shell_env_by_cmd_line(t_shell *sh, char *cmd);
char		**get_shell_argv(t_shell *sh);
char		**get_shell_argv(t_shell *sh);
void		test_set_shell_argv_from_cmd_line(t_shell *sh, char *s);
char		*dup_shell_env_by_name(t_shell *sh, char *name);
char		**get_shell_env(t_shell *sh);
int			exec_shell_cmds(t_shell *sh);
char		*find_cmd_in_path(t_command *cmd);
int			exec_cmd_from_str(t_shell *sh, char *s);
void		del_all_cmds(t_command *cmd);
t_command	*new_cmd(t_shell *sh);
t_shell		*use_shell_struct(t_shell *sh);
int			fork_execve_wrap(t_command *cmd, int *fds);
int			do_exec_redir(t_command *cmd);
char		*find_cmd_in_path(t_command *cmd);
int			is_cmd_option_path(t_command *cmd);
t_command	*get_shell_next_cmd(t_shell *sh);
char		**get_shell_cmd_argv(t_shell *sh);
char		*get_shell_cmd_path(t_shell *sh);
void		pop_shell_cmd(t_shell *sh);
char		*get_shell_user_cmd(t_shell *sh);
int			parse_shell_cmd_line(t_shell *sh);
int			exec_cmds(t_command *cmd, int *fds);
void		sig_handler_init(void);
void		cmd_do_redir(t_command *cmd);
int			is_cmd_valid(t_command *cmd);
#endif
