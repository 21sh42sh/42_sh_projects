/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_wrap_struct.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 16:19:20 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/22 21:30:45 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_WRAP_STRUCT_H
# define SHELL_WRAP_STRUCT_H

# include "libft.h"

enum	e_cmd_type{
	OP_REG,
	OP_PIPE,
	OP_IF,
	OP_ELSE
};

enum	e_cmd_rdr{
	RED_NONE,
	RED_OUT,
	RED_IN,
	RED_IN_ADD,
	RED_OUT_ADD,
	RED_STR_RDR
};

/*
** The functions will use "get", "set", "destroy", "new" etc. wrapper functions
** The wrapper functions will be generic functions which return value will
** be structure independant so it can be changed when integrated in project
** return value to be stored in singleton
*/

typedef struct s_shell	t_shell;
typedef struct s_command	t_command;
typedef int	(*t_builtin)(t_command *);
typedef	struct s_fd	t_fd;
# define NB_BUILTINS 9

struct	s_fd{
	int		flags;
	char	*name;
};

struct	s_shell {
	char			**env;
	t_builtin		*fn_bltns;
	char			**bltns;
	t_command		*cmds;
	char			**history;
	int				history_cmd_nb;
	int				history_file_nb_line;
	char			**alias;
	int				exit_status;
	char			*native_folder;
	char			**av;
	int				ac;
	int				ac_i;
};

struct	s_command{
	char			**options;
	t_shell			*sh;
	char			*heredoc;
	int				fd[3];
	int				ret;
	t_command		*left;
	t_command		*right;
	t_command		*parent;
	enum e_cmd_type	type;
	enum e_cmd_rdr	redir[3];
	pid_t			pid;
	t_fd			fds[3];
};

#endif
