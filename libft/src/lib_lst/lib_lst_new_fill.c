/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_new_fill.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 17:07:55 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/11 16:04:58 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*lib_lst_new_fill(size_t n, void *(*constructor)(void),
	size_t default_sz)
{
	t_list *lst;

	lst = lib_lst_new();
	lst->content = constructor();
	lst->content_size = default_sz;
	while (n--)
		lib_lst_push_back(lst, constructor(), default_sz);
	return (lst);
}
