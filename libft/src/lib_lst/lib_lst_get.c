/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_get.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 15:19:34 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/11 16:24:39 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*lib_lst_get(t_list *lst, size_t index)
{
	void *ret;

	ret = NULL;
	if (lst)
	{
		lst = lib_lst_get_elem__(lst, index);
		ret = lst->content;
	}
	return (ret);
}
