/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_del.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 17:18:27 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/11 16:14:05 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	lib_lst_del(t_list *lst, void (*delfn)(void *))
{
	t_list *next;

	while (lst)
	{
		next = lst->next;
		if (delfn && lst->content)
			delfn(lst->content);
		free(lst);
		lst = next;
	}
}
