/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_rev.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 15:01:43 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/11 15:04:13 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	lib_lst_rev(t_list **lst)
{
	t_list	*tmp;
	t_list	*tmp_prev;
	t_list	*tmp_next;

	if (lst)
	{
		tmp_prev = NULL;
		tmp = *lst;
		while (tmp)
		{
			tmp_next = tmp->next;
			tmp->next = tmp_prev;
			if (!tmp_next)
				*lst = tmp;
			tmp_prev = tmp;
			tmp = tmp_next;
		}
	}
}
