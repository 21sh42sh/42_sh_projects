/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_get_elem__.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/10 19:28:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/11 16:09:14 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*lib_lst_get_elem__(t_list *lst, size_t index)
{
	while (lst && index)
	{
		lst = lst->next;
		index--;
	}
	return (lst);
}
