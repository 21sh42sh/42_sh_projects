/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_del_if.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 16:12:04 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/23 16:18:52 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	lib_lst_del_if(t_list *lst, int (*cmp)(void *), void (del)(void *))
{
	t_list *prev;
	t_list *next;

	prev = NULL;
	while (lst)
	{
		if (prev)
			prev->next = lst->next;
		next = lst->next;
		if (lst->content ? cmp(lst->content) : 0)
		{
			del(lst->content);
			free(lst);
		}
		else
			prev = lst;
		lst = next;
	}
}
