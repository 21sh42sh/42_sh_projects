/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 15:36:09 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/13 16:28:29 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	lib_lst_push_back(t_list *lst, void *data, size_t sz)
{
	t_list *new;

	new = lib_lst_create_elem(data, sz);
	lst = lib_lst_last(lst);
	lst->next = new;
}
