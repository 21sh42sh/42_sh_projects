/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_create_elem.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 16:52:35 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/17 19:01:46 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*lib_lst_create_elem(void *data, size_t sz)
{
	t_list *ret;

	if ((ret = (t_list *)lib_memalloc(sizeof(t_list))))
	{
		ret->content = data;
		ret->content_size = sz;
	}
	return (ret);
}
