/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst_to_arr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/16 18:04:47 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/16 19:14:34 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	**lib_lst_to_arr(t_list *lst)
{
	void	**ret;
	size_t	sz;
	size_t	i;

	ret = NULL;
	if (lst)
	{
		sz = lib_lst_len(lst);
		ret = (void **)lib_memalloc((sz + 1) * sizeof(void *));
		i = 0;
		while (i < sz)
		{
			ret[i] = lib_memalloc(lst->content_size);
			lib_memcpy(ret[i], lst->content, lst->content_size);
			i++;
			lst = lst->next;
		}
	}
	return (ret);
}
