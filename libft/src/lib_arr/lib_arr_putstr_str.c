/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_putstr_str.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 22:16:58 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 22:18:13 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		lib_arr_putstr_str(const char **array, const char *str)
{
	return (lib_arr_putstr_str_fd(array, str, 1));
}
