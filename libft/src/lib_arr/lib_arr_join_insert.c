/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_join_insert.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 15:04:33 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/28 16:46:44 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Join an array into a string and insert insert between each arr element.
** flag:
** _____ 1 delete arr
** _____ 2 delete insert
** _____ 3 delete arr and insert
** return:
** _______ the joined array.
*/

char		*lib_arr_joini(char **arr, char *insert, int flag)
{
	char	*str;
	int		i;

	str = NULL;
	if (!arr)
		str = lib_strdup(insert);
	i = 0;
	while (arr && arr[i])
	{
		str = lib_strjoinfree(str, arr[i], 1);
		if (insert && arr[i + 1])
			str = lib_strjoinfree(str, insert, 1);
		i++;
	}
	if (flag == 1 || flag == 3)
		lib_arr_del(arr);
	if (flag == 2 || flag == 3)
		lib_strdel(&insert);
	return (str);
}
