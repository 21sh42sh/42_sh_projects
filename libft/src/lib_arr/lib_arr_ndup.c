/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_ndup.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/25 15:52:32 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** lib_arr_ndup duplicate the first n enties of the (char **) arr.
** param:
** ______ flag != 0 : delete arr
** return:
** _______ a (char **) of size 0 to n
** _______ NULL if arr is NULL or n < 0
*/

char			**lib_arr_ndup(char **arr, size_t n, int flag)
{
	char		**new_arr;
	size_t		len_arr;
	size_t		i;

	if (!arr || (int)n < 0)
		return (NULL);
	len_arr = lib_arr_len(arr);
	new_arr = lib_arr_new(0);
	i = 0;
	while (arr && arr[i] && i < n && i < len_arr)
	{
		new_arr = lib_arr_push_back(new_arr, arr[i]);
		i++;
	}
	if (flag)
		lib_arr_del(arr);
	return (new_arr);
}
