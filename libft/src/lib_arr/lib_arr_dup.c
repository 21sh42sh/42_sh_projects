/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_dup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/21 19:27:32 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**lib_arr_dup(char **src)
{
	size_t	len;
	size_t	i;
	char	**dup;

	if (!src)
		return (NULL);
	len = lib_arr_len(src);
	if (!(dup = lib_arr_new(len)))
		return (NULL);
	i = 0;
	while (i < len)
	{
		if (!(dup[i] = lib_strdup(src[i])))
			return (NULL);
		i++;
	}
	dup[i] = NULL;
	return (dup);
}
