/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_to_str.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/22 15:32:16 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/06 14:44:17 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*lib_arr_to_str(char **arr)
{
	int		i;
	char	*str;

	str = lib_strnew(0);
	i = 0;
	while (arr && arr[i])
	{
		str = lib_strjoinfree(str, " ", 1);
		str = lib_strjoinfree(str, arr[i++], 1);
	}
	return (str);
}
