/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/22 15:33:22 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		lib_arr_sort(char **arr, int (*cmp_fn)(char *s1, char *s2))
{
	size_t	i;
	size_t	j;
	size_t	size;

	size = lib_arr_len(arr);
	if (size < 2)
		return ;
	i = 0;
	while (i < size - 1)
	{
		j = 0;
		while (j < size - 1 - i)
		{
			if (cmp_fn(arr[j + 1], arr[j]))
				lib_arr_swap(arr, j + 1, j);
			j++;
		}
		i++;
	}
}
