/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_putstr_fd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 22:11:11 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			lib_arr_putstr_fd(const char **array, int fd)
{
	if (!array || fd < 0)
		return ;
	if (!*array)
	{
		write(fd, "\n", 1);
		return ;
	}
	while (*array)
	{
		write(fd, *array, lib_strlen(*array));
		write(fd, "\n", 1);
		array++;
	}
}
