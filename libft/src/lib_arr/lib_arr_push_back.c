/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 20:12:42 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			**lib_arr_push_back(char **old_arr, char *new_field)
{
	char		**new_arr;
	size_t		len_old_arr;
	size_t		i;

	new_arr = NULL;
	len_old_arr = lib_arr_len(old_arr);
	if (!(new_arr = (char **)malloc(sizeof(char *) * (len_old_arr + 2))))
		return (NULL);
	i = 0;
	while (i < len_old_arr)
	{
		new_arr[i] = NULL;
		if (!(new_arr[i] = lib_strdup(old_arr[i])))
			return (NULL);
		i++;
	}
	new_arr[i] = NULL;
	if (!(new_arr[i] = lib_strdup(new_field)))
		return (NULL);
	new_arr[i + 1] = NULL;
	lib_arr_del(old_arr);
	return (new_arr);
}
