/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_filter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/11 15:46:04 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**lib_arr_filter(char **arr, char *filter, int (*fn_filter)(char *s1,
	char *s2))
{
	char **new;

	if (!arr)
		return (NULL);
	new = lib_arr_new(0);
	while (*arr)
	{
		if (fn_filter(*arr, filter))
			new = lib_arr_push_back(new, *arr);
	}
	return (new);
}
