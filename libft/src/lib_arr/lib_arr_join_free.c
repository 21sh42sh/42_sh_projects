/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_join_free.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 15:03:41 by mafagot           #+#    #+#             */
/*   Updated: 2016/07/12 15:04:22 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**lib_arr_join_free(char **a1, char **a2, int flag_arr_to_free)
{
	size_t	len1;
	size_t	len2;
	char	**ret;

	if ((!a1 || !a2) && !flag_arr_to_free)
		return (lib_arr_join(a1, a2));
	len1 = lib_arr_len(a1);
	len2 = lib_arr_len(a2);
	if (!(ret = lib_arr_new(len1 + len2)))
		return (NULL);
	if (a1 && !lib_arr_cpy(ret, a1))
		return (NULL);
	if (a2 && !lib_arr_cpy(ret + len1, a2))
		return (NULL);
	if (a1 && (flag_arr_to_free == 1 || flag_arr_to_free == 3))
		lib_arr_del(a1);
	if (a2 && (flag_arr_to_free == 2 || flag_arr_to_free == 3))
		lib_arr_del(a2);
	return (ret);
}
