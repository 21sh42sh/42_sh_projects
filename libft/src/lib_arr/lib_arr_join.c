/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_join.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 15:04:33 by mafagot           #+#    #+#             */
/*   Updated: 2016/07/12 15:05:11 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**lib_arr_join(char **a1, char **a2)
{
	size_t	len1;
	size_t	len2;
	char	**ret;

	if (a1 == NULL || a2 == NULL)
		return ((a1) ? lib_arr_dup(a1) : lib_arr_dup(a2));
	len1 = lib_arr_len(a1);
	len2 = lib_arr_len(a2);
	if (!(ret = lib_arr_new(len1 + len2)))
		return (NULL);
	if (!lib_arr_cpy(ret, a1) || !lib_arr_cpy(ret + len1, a2))
		return (NULL);
	return (ret);
}
