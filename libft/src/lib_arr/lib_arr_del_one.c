/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_del_one.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 20:54:31 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/12 20:57:01 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	lib_arr_del_one(char **arr, size_t index)
{
	char	*to_free;

	if (index >= lib_arr_len(arr))
		return ;
	to_free = *(arr + index);
	arr = arr + index;
	while (*arr)
	{
		*arr = *(arr + 1);
		arr++;
	}
	*(arr - 1) = NULL;
	free(to_free);
}
