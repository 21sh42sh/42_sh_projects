/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_arr_putstr_str_fd.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 22:12:29 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/06 14:48:20 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		lib_arr_putstr_str_fd(const char **array, const char *str, int fd)
{
	if (!array || !str || fd < 0)
		return ;
	if (!*array || !*str)
	{
		write(fd, "\n", 1);
		return ;
	}
	while (*array)
	{
		write(fd, *array, lib_strlen(*array));
		write(fd, str, lib_strlen(str));
		array++;
	}
}
