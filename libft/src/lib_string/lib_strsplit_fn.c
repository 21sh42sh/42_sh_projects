/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strsplit_fn.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 14:58:38 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/25 15:05:21 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	lib_tally_parts(char const *str, int (*is_sth)(int c))
{
	size_t		count;
	size_t		i;
	size_t		len;

	count = 0;
	i = 0;
	len = lib_strlen(str);
	while (str[i] && is_sth(str[i]))
		i++;
	if (i == len)
		return (0);
	if ((is_sth(str[0]) == 0) && (len != 0))
		count += 1;
	i = 0;
	while (i < len - 1)
	{
		if (is_sth(str[i]) && (is_sth(str[i + 1]) == 0))
			count++;
		i++;
	}
	return (count);
}

char			**lib_strsplit_fn(const char *str, int (*is_sth)(int c))
{
	char		**ret;
	size_t		count;
	size_t		i;
	size_t		len;

	if (str == NULL)
		return (NULL);
	count = lib_tally_parts(str, is_sth);
	if (!(ret = (char **)malloc(sizeof(char *) * (count + 1))))
		return (NULL);
	i = 0;
	while (*str && i < count)
	{
		while (*str && is_sth(*str))
			str++;
		len = 0;
		while (is_sth(*str++) == 0)
			len++;
		ret[i] = lib_strncpy(lib_strnew(len), str - (len + 1), len);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
