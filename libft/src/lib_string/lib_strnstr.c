/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strnstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 22:09:45 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 18:03:20 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*lib_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	len2;

	i = 0;
	len2 = lib_strlen(s2);
	if (len2 == 0)
		return ((char *)s1);
	while (s1[i] && n-- >= len2)
	{
		if (s1[i] == s2[0] && lib_strncmp((const char*)(s1 + i), s2, len2) == 0)
			return ((char *)(s1 + i));
		i++;
	}
	return (NULL);
}
