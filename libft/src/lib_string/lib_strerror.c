/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strerror.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 16:03:39 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/17 14:08:04 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#ifndef LIB_MAX_ERROR_LEN
# define LIB_MAX_ERROR_LEN 256
#endif

static char	*lib_err_buf(void)
{
	static char buf[LIB_MAX_ERROR_LEN];

	return (buf);
}

char		*lib_strerror(int errnum)
{
	extern int	sys_nerr;
	extern char	*sys_errlist[];
	char		*ret;
	char		*tmp;

	ret = lib_err_buf();
	if (errnum < 0 || errnum >= sys_nerr || sys_errlist[errnum] == NULL)
	{
		if (!(tmp = lib_itoa(errnum)))
			return (NULL);
		lib_strncpy(ret, "Unknown error ", 14);
		lib_strncpy(ret + 14, tmp, lib_strlen(tmp));
		ret[14 + lib_strlen(tmp)] = '\0';
		free(tmp);
	}
	else
	{
		lib_strncpy(ret, sys_errlist[errnum], LIB_MAX_ERROR_LEN - 1);
		ret[LIB_MAX_ERROR_LEN - 1] = '\0';
	}
	return (ret);
}
