/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strndup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 22:17:29 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/10 23:17:10 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*lib_strndup(const char *s, size_t n)
{
	char	*ret;
	size_t	len;

	len = lib_strlen(s);
	if (n < len)
		len = n;
	if (!(ret = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	ret[len] = '\0';
	return (lib_strncpy(ret, s, len));
}
