/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strncat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 22:09:45 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 17:03:42 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*lib_strncat(char *s1, const char *s2, size_t n)
{
	char	*s1_orig;
	size_t	i;

	if (!s1 || !s2)
		return (void *)0;
	s1_orig = s1;
	i = 0;
	while (*s1)
		s1++;
	while (*s2 && i++ < n)
		*s1++ = *s2++;
	*s1 = '\0';
	return (s1_orig);
}
