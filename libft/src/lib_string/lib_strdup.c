/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 22:09:45 by mafagot           #+#    #+#             */
/*   Updated: 2016/08/17 03:13:50 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*lib_strdup(const char *s1)
{
	char	*dup;
	char	*tmp;

	dup = NULL;
	if (s1)
	{
		if (!(dup = (char *)malloc(sizeof(char) * (lib_strlen(s1) + 1))))
			return (NULL);
		tmp = dup;
		while (*s1)
			*tmp++ = *s1++;
		*tmp = '\0';
	}
	return (dup);
}
