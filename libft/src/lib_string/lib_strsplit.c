/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strsplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 14:42:11 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 20:04:25 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	lib_count_parts(char const *s, char c)
{
	size_t		count;
	size_t		i;
	size_t		len;

	count = 0;
	i = 0;
	len = lib_strlen(s);
	while (s[i] && s[i] == c)
		i++;
	if (i == len)
		return (0);
	if (s[0] != c && len != 0)
		count += 1;
	i = 0;
	while (i < len - 1)
	{
		if (s[i] == c && s[i + 1] != c)
			count++;
		i++;
	}
	return (count);
}

char			**lib_strsplit(char const *s, char c)
{
	char		**ret;
	size_t		count;
	size_t		i;
	size_t		len;

	if (s == NULL)
		return (NULL);
	count = lib_count_parts(s, c);
	if (!(ret = (char **)lib_memalloc(sizeof(char *) * (count + 1))))
		return (NULL);
	i = 0;
	while (*s && i < count)
	{
		while (*s == c)
			s++;
		len = 0;
		while (*s != c && *s)
		{
			s++;
			len++;
		}
		ret[i++] = lib_strncpy(lib_strnew(len), s - len, len);
	}
	ret[i] = NULL;
	return (ret);
}
