/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strrchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 22:09:45 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 18:08:32 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*lib_strrchr(const char *s, int c)
{
	char		*str;
	size_t		ln;
	size_t		i;

	if (!s)
		return (NULL);
	str = (char *)s;
	ln = lib_strlen(s);
	i = 1;
	if (c == 0)
		return (str + ln);
	while (i <= ln)
	{
		if (str[ln - i] == c)
			return (str + ln - i);
		i++;
	}
	return (NULL);
}
