/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 20:04:03 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*lib_strcat(char *s1, const char *s2)
{
	unsigned int	i;
	unsigned int	ln;

	i = 0;
	ln = lib_strlen(s1);
	while (s2[i])
	{
		s1[ln + i] = s2[i];
		i++;
	}
	s1[ln + i] = '\0';
	return (s1);
}
