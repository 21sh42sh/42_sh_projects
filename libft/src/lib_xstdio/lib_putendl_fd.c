/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_putendl_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/09 16:14:09 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/09 16:31:10 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>

void	lib_putendl_fd(const char *s, int fd)
{
	write(fd, s, lib_strlen(s));
	write(fd, "\n", 1);
}
