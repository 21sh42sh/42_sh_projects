/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_putnbr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 15:53:39 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/11 16:02:15 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	lib_putnbr_fd(int n, int fd)
{
	char *s;

	s = lib_itoa(n);
	if (s)
	{
		lib_putstr_fd(s, fd);
		free(s);
	}
}
