/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/14 19:17:06 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/21 15:31:54 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			get_last_line(char **line, char **stck, t_var *v)
{
	int				len;

	len = lib_strlen(v->tmp);
	if (v->i == 0 && len > 0)
	{
		*line = lib_strncpy(lib_strnew(len), v->tmp, len);
		lib_bzero(*stck, BUFF_SIZE);
		v->i = 1;
	}
	else
	{
		*line = NULL;
		lib_strdel(stck);
	}
	free(v->tmp);
	free(v->buf);
	return (v->i);
}

int					get_next_line(int const fd, char **line)
{
	static char		*stck;
	t_var			v;

	if (BUFF_SIZE < 1 || !line)
		return (-1);
	stck = (stck == NULL) ? lib_strnew(BUFF_SIZE) : stck;
	if (!(v.buf = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1))) \
		|| !(v.tmp = lib_strncpy(lib_strnew(BUFF_SIZE), stck, BUFF_SIZE)))
		return (-1);
	while (lib_strchr(v.tmp, '\n') == 0)
	{
		if ((v.i = read(fd, v.buf, BUFF_SIZE)) < 1)
			return (get_last_line(line, &stck, &v));
		v.buf[v.i] = '\0';
		v.tmp = lib_strjoinfree(v.tmp, v.buf, 1);
	}
	free(v.buf);
	v.i = (size_t)(lib_strchr(v.tmp, '\n') - v.tmp);
	*line = lib_strncpy(lib_strnew(v.i), v.tmp, v.i);
	lib_bzero(stck, BUFF_SIZE);
	stck = lib_strncpy(stck, lib_strchr(v.tmp, '\n') + 1, BUFF_SIZE);
	free(v.tmp);
	return (1);
}
