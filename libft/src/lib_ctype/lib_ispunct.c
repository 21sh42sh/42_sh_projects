/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_ispunct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 21:51:42 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/06 14:43:40 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The default C locale classifies the
** characters !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ as punctuation.
*/

int		lib_ispunct(int c)
{
	if ((c >= 0x21 && c <= 0x2F) || (c >= 0x3a && c <= 0x40) \
		|| (c >= 0x5B && c <= 0x60) || (c >= 0x7B && c <= 0x7E))
		return (1);
	return (0);
}
