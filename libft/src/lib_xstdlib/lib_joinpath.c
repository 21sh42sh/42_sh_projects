/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_joinpath.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/07 17:31:46 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/09 16:19:08 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	is_path_slash_ended(char *path)
{
	if (path)
	{
		if (path[lib_strlen(path) - 1] == '/')
			return (1);
	}
	return (0);
}

char		*lib_joinpath(char *path1, char *path2)
{
	char	*ret;
	int		slash;

	ret = NULL;
	if (path1 && path2 ? *path1 && *path2 : 0)
	{
		slash = is_path_slash_ended(path1);
		ret = lib_strnew(lib_strlen(path1) + lib_strlen(path2) + !slash);
		lib_strcpy(ret, path1);
		if (!slash)
			ret[lib_strlen(ret)] = '/';
		lib_strcpy(ret + lib_strlen(path1) + !slash, path2);
	}
	return (ret);
}
