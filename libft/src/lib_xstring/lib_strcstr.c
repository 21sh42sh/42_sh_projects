/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strcstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 18:49:49 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/06 07:11:39 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** lib_strcstr() finds the first occurrence of one char of all the chars
** from substring s2 (needle) in the string s1 (haystack).
** It does NOT create a new string.
**
** example: lib_strcstr("abcdef12345", "2fc") -> "cdef12345" (== s1 + 2)
** because among ALL the chars composing s2, 'c' appears the 1st in s1.
*/

char		*lib_strcstr(const char *s1, const char *s2)
{
	char	*ret;
	char	*tmp;

	ret = NULL;
	if (!s1 || !s2 || !*s1 || !*s2)
		return (NULL);
	while (*s2)
	{
		tmp = (char *)s1;
		while (*tmp)
		{
			if (*tmp == *s2)
			{
				if (!ret)
					ret = tmp;
				else if (tmp < ret)
					ret = tmp;
			}
			tmp++;
		}
		s2++;
	}
	return (ret);
}
