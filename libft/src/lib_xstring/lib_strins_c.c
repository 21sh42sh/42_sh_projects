/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strins_c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 18:43:12 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/14 00:16:09 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** "strins_c" stands for STRing INSert a Char
** Example of usage:
** lib_strins_c("abcdef", 'X', 3, 1) -> "abcXdef", and "abcdef" is freed
*/

char		*lib_strins_c(char *str, const char c, int pos, int flag_free)
{
	size_t	len;
	char	*ret;
	char	s[2];

	if (!str && !c)
		return (NULL);
	s[0] = c;
	s[1] = '\0';
	len = lib_strlen(str);
	if (pos < 0 || (size_t)pos > (len + 1))
		ret = (flag_free) ? lib_strjoinfree(str, s, 1) : lib_strjoin(str, s);
	else
	{
		if (!(ret = lib_strnew(len + 1)))
			return (NULL);
		lib_strncpy(ret, str, pos);
		ret[pos] = c;
		lib_strcpy(ret + pos + 1, str + pos);
		if (flag_free)
			free(str);
	}
	return (ret);
}
