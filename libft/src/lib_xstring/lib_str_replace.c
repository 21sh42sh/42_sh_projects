/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_str_replace.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 21:59:41 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/14 22:28:29 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** lib_str_replace() finds the first occurrence of the substring s2 (needle)
** in the string s1 (haystack) and replace it with the substring s3.
** A new string is not necessarely created in the process.
** If s2 is null or empty, s1 is returned. Even if s3 has a value.
** If s3 is null. s2 is "removed" (same behavior as if s3 == "").
*/

char		*lib_str_replace(char *s1, const char *s2, const char *s3)
{
	char	*ret;
	char	*tmp;
	size_t	i;

	if (!s2 || !*s2 || !(tmp = lib_strstr(s1, s2)))
		return (s1);
	i = lib_strlen(s1) - lib_strlen(s2) + lib_strlen(s3) + 1;
	if (!(ret = (char *)malloc(sizeof(char) * i)))
		return (NULL);
	i = 0;
	while (s1 + i != tmp)
	{
		ret[i] = s1[i];
		i++;
	}
	while (s3 && *s3)
		ret[i++] = *s3++;
	tmp = tmp + lib_strlen(s2);
	while (*tmp)
		ret[i++] = *tmp++;
	ret[i] = '\0';
	free(s1);
	return (ret);
}
