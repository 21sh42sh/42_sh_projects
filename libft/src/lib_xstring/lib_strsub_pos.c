/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strsub_pos.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:44:39 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/23 15:14:12 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*lib_strsub_pos(char *s1, const char *s2, const char *s3, \
						const char *ptr)
{
	char	*ret;
	char	*tmp;
	size_t	i;

	if (!s2 || !*s2 || !ptr || ptr > s1 + lib_strlen(s1) \
		|| ptr != lib_strstr(ptr, s2))
		return (s1);
	i = lib_strlen(s1) - lib_strlen(s2) + lib_strlen(s3) + 1;
	if (!(ret = (char *)malloc(sizeof(char) * i)))
		return (NULL);
	i = 0;
	while (s1 + i != ptr)
	{
		ret[i] = s1[i];
		i++;
	}
	while (s3 && *s3)
		ret[i++] = *s3++;
	tmp = (char *)ptr + lib_strlen(s2);
	while (*tmp)
		ret[i++] = *tmp++;
	ret[i] = '\0';
	free(s1);
	return (ret);
}
