/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_str_sanitize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 21:59:41 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/11 16:00:52 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Replace the caractere `\n`, \t`, \r` by the string "\n", "\t", "\r" in str.
** return:
** _______ a new malloced string.
*/

char	*lib_str_sanitize(char *str)
{
	char	*str_clean;

	if (str == NULL)
		return (NULL);
	str_clean = lib_strnew(0);
	while (str && *str)
	{
		if ((*str == '\n') || (*str == '\r') || (*str == '\t'))
			str_clean = lib_strins_c(str_clean, (char)(92), -1, 1);
		if (*str == '\n')
			str_clean = lib_strins_c(str_clean, 'n', -1, 1);
		else if (*str == '\r')
			str_clean = lib_strins_c(str_clean, 'r', -1, 1);
		else if (*str == '\t')
			str_clean = lib_strins_c(str_clean, 't', -1, 1);
		else
			str_clean = lib_strins_c(str_clean, *str, -1, 1);
		str += 1;
	}
	return (str_clean);
}
