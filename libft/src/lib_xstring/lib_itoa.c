/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_itoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 20:00:14 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/29 18:18:09 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#define LIB_INT_MIN_STR "-2147483648"

static void		lib_stat_length(int n, size_t *len)
{
	*len = 1;
	if (n > 0)
	{
		*len = 0;
		n *= -1;
	}
	while (n != 0)
	{
		n /= 10;
		*len += 1;
	}
}

char			*lib_itoa(int n)
{
	size_t		len;
	size_t		i;
	char		*str;

	if (n == INT_MIN)
		return (lib_strdup(LIB_INT_MIN_STR));
	lib_stat_length(n, &len);
	if (!(str = (char *)malloc(sizeof(*str) * (len + 1))))
		return (NULL);
	if (n < 0)
	{
		str[0] = '-';
		n *= -1;
	}
	if (n == 0)
		str[0] = '0';
	i = 0;
	while (n != 0)
	{
		str[len - i - 1] = n % 10 + '0';
		i++;
		n /= 10;
	}
	str[len] = '\0';
	return (str);
}
