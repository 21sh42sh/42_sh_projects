/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strins_s.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 18:43:12 by mafagot           #+#    #+#             */
/*   Updated: 2016/05/02 16:56:50 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** "strins_s" stands for STRing INSert a String
** Example of usage:
** lib_strins_s("abcdef", "XXX", 3, 1) -> "abcXXXdef",
**                                        "abcef" is freed, XXX" is not freed
*/

char		*lib_strins_s(char *str, char *ins, int pos, int flag_str_to_free)
{
	size_t	len_s;
	size_t	len_i;
	char	*ret;

	if (!str || !ins)
		return (str ? str : ins);
	len_s = lib_strlen(str);
	if (pos > (int)len_s + 1 || pos < 0)
		ret = lib_strjoinfree(str, ins, flag_str_to_free);
	else
	{
		len_i = lib_strlen(ins);
		if (!(ret = lib_strnew(len_s + len_i)))
			return (NULL);
		lib_strncpy(ret, str, pos);
		lib_strncpy(ret + pos, ins, len_i);
		lib_strcpy(ret + pos + len_i, str + pos);
		if (str && (flag_str_to_free == 1 || flag_str_to_free == 3))
			free(str);
		if (ins && (flag_str_to_free == 2 || flag_str_to_free == 3))
			free(ins);
	}
	return (ret);
}
