/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strrm_c.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/02 17:04:24 by mafagot           #+#    #+#             */
/*   Updated: 2016/05/02 17:49:19 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** "strrmv_c" stands for STRing ReMove a Char
** Example of usage:
** lib_strrm_c("abcdef", 3, 1) -> "abcef" and "abcdef" is freed
*/

char		*lib_strrm_c(char *s, size_t pos, int flag_str_to_free)
{
	char	*ret;
	size_t	len;

	if (!s || !*s)
		return (s);
	len = lib_strlen(s);
	if (pos > len)
		return (s);
	if (!(ret = (char *)malloc(sizeof(char) * len)))
		return (NULL);
	lib_strncpy(ret, s, pos);
	lib_strcpy(ret + pos, s + pos + 1);
	if (flag_str_to_free)
		free(s);
	return (ret);
}
