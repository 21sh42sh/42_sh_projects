/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strrstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/12 16:23:34 by mafagot           #+#    #+#             */
/*   Updated: 2016/08/04 01:42:10 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** lib_strrstr() finds the last occurrence of the substring s2 (needle)
** in the string s1 (haystack).
** It does NOT create a new string.
*/

char		*lib_strrstr(const char *s1, const char *s2)
{
	size_t	len_1;
	size_t	len_2;
	char	*tmp;

	if (!s1 || !s2)
		return (NULL);
	if (*s2 == '\0')
		return ((char *)s1);
	len_1 = lib_strlen(s1);
	len_2 = lib_strlen(s2);
	if (len_1 == 0)
		return (NULL);
	tmp = (char *)(s1 + len_1 - 1);
	while (tmp >= s1)
	{
		if (lib_memcmp(tmp, s2, len_2) == 0)
			return ((char *)tmp);
		tmp--;
	}
	return (NULL);
}
