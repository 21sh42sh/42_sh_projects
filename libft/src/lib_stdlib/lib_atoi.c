/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_atoi.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/04/20 20:05:10 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				lib_atoi(const char *str)
{
	int					res;
	int					sign;
	unsigned int		i;

	i = 0;
	res = 0;
	sign = 1;
	if (str == NULL)
		return (0);
	while (lib_isspace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sign = -1;
		i++;
	}
	if ((lib_strcmp(str + i, "2147483648") == 0) && (sign == -1))
		return (INT_MIN);
	while (str[i] && lib_isdigit(str[i]))
	{
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (res * sign);
}
