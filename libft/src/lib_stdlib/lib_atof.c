/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_atof.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/29 18:27:59 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

float	lib_atof(char *str)
{
	float	nbr;
	float	tmp;
	char	**array;
	size_t	len;

	array = lib_strsplit(str, '.');
	nbr = (float)lib_atoi(array[0]);
	if (array[1])
	{
		tmp = (float)lib_atoi(array[1]);
		len = lib_strlen(array[1]) + 1;
		while (--len > 0)
			tmp = tmp / 10.0;
		nbr += tmp;
	}
	lib_arr_del(array);
	return (nbr);
}
