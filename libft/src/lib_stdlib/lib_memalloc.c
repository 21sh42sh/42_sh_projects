/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_memalloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 13:17:19 by hlequien          #+#    #+#             */
/*   Updated: 2016/05/13 15:48:42 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

void	*lib_memalloc(size_t sz)
{
	void *ret;

	if ((ret = malloc(sz)))
		lib_bzero(ret, sz);
	return (ret);
}
