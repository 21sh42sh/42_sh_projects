/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_perror.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 16:37:39 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/17 14:25:19 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/errno.h>

void	lib_perror(const char *s)
{
	const char	*tmp;

	if (s && *s)
	{
		write(2, s, lib_strlen(s));
		write(2, " : ", 3);
	}
	tmp = lib_strerror(*__error());
	write(2, tmp, lib_strlen(tmp));
	write(2, "\n", 1);
}
