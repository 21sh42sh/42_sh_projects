/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 17:02:24 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/21 16:51:17 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <ctype.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include <limits.h>

# define BUFF_SIZE 128

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_var
{
	char			*tmp;
	char			*buf;
	int				i;
}					t_var;

unsigned int		ft_abs(int nb);

char				*lib_arr_joini(char **arr, char *insert, int flag);
char				**lib_arr_ndup(char **arr, size_t n, int flag);
char				*lib_char_to_str(char c);
char				*lib_str_sanitize(char *str);
void				lib_putnbr_fd(int n, int fd);
char				*lib_arr_to_str(char **arr);
char				**lib_arr_cpy(char **dst, char **src);
void				lib_arr_del(char **array);
void				lib_arr_del_one(char **arr, size_t index);
char				**lib_arr_dup(char **src);
char				**lib_arr_filter(char **arr, char *filter,
	int (*fn_filter)(char *s1, char *s2));
char				**lib_arr_filter_del(char **arr, char *filter,
	int (*fn_filter)(char *s1, char *s2));
char				*lib_arr_find(char **arr, const char *to_find, size_t len);
char				*lib_arr_last(char **arr);
size_t				lib_arr_len(char **array);
char				**lib_arr_new(size_t size);
char				**lib_arr_push_back(char **old_arr, char *new_field);
char				**lib_arr_push_front(char **old_arr, char *new_field);
char				**lib_arr_push_penultimate(char **old_arr, char *new_field);
void				lib_arr_putstr(const char **array);
void				lib_arr_putstr_fd(const char **array, int fd);
void				lib_arr_putstr_str(const char **array, const char *str);
void				lib_arr_putstr_str_fd(const char **array, const char *str,
	int fd);
void				lib_arr_sort(char **arr, int (*cmp_fn)(char *s1, char *s2));
void				lib_arr_swap(char **arr, int i, int j);
char				**lib_arr_join(char **a1, char **a2);
char				**lib_arr_join_free(char **a1, char **a2,
	int flag_arr_to_free);
int					lib_isalpha(int c);
int					lib_isblank(int c);
int					lib_isdigit(int c);
int					lib_islower(int c);
int					lib_ispunct(int c);
int					lib_isspace(int c);
int					lib_isupper(int c);
int					get_next_line(int const fd, char **line);
t_list				*lib_lst_create_elem(void *data, size_t sz);
void				*lib_lst_get(t_list *lst, size_t index);
t_list				*lib_lst_get_elem__(t_list *lst, size_t index);
t_list				*lib_lst_last(t_list *lst);
size_t				lib_lst_len(t_list *lst);
void				lib_lst_push_back(t_list *lst, void *data, size_t sz);
void				lib_lst_rev(t_list **lst);
void				**lib_lst_to_arr(t_list *lst);
void				lib_perror(const char *s);
float				lib_atof(char *str);
int					lib_atoi(const char *str);
void				*lib_memalloc(size_t sz);
void				lib_bzero(void *s, size_t n);
void				*lib_memccpy(void *dst, const void *src, int c, size_t ln);
void				*lib_memchr(const void *s, int c, size_t n);
int					lib_memcmp(const void *s1, const void *s2, size_t n);
void				*lib_memcpy(void *dst, const void *src, size_t n);
void				*lib_memdup(const void *s, size_t n);
void				*lib_memset(void *b, int c, size_t len);
char				*lib_strcat(char *s1, const char *s2);
char				*lib_strchr(const char *s, int c);
int					lib_strcmp(const char *s1, const char *s2);
char				*lib_strcpy(char *dst, const char *src);
char				*lib_strdup(const char *s1);
char				*lib_strerror(int errnum);
char				*lib_strjoin(char const *s1, char const *s2);
char				*lib_strjoinfree(char *s1, char *s2, int flag_str_to_free);
size_t				lib_strlcat(char *dst, const char *src, size_t size);
size_t				lib_strlen(const char *str);
char				*lib_strncat(char *s1, const char *s2, size_t n);
int					lib_strncmp(const char *s1, const char *s2, size_t n);
char				*lib_strncpy(char *dst, const char *src, size_t n);
char				*lib_strndup(const char *s, size_t n);
char				*lib_strnew(size_t size);
char				*lib_strnstr(const char *s1, const char *s2, size_t n);
char				*lib_strrchr(const char *s, int c);
char				*lib_strsub_pos(char *s1, const char *s2, const char *s3, \
	const char *ptr);
char				**lib_strsplit(char const *s, char c);
char				**lib_strsplit_fn(const char *str, int (*is_sth)(int c));
char				*lib_strstr(const char *s1, const char *s2);
void				lib_putendl(const char *s);
void				lib_putendl_fd(const char *s, int fd);
void				lib_putstr(char const *s);
void				lib_putstr_fd(char const *s, int fd);
char				*lib_joinpath(char *path1, char *path2);
char				*lib_itoa(int n);
char				*lib_str_replace(char *s1, const char *s2, const char *s3);
char				*lib_strcstr(const char *s1, const char *s2);
char				*lib_strins_c(char *str, const char c, int pos, int flag);
char				*lib_strins_s(char *str, char *ins, int pos,
	int flag_str_to_free);
char				*lib_strrm_c(char *s, size_t pos, int flag_str_to_free);
char				*lib_strrstr(const char *s1, const char *s2);
t_list				*lib_lst_new(void);
void				lib_putnbr(int n);
void				lib_strdel(char **as);

#endif
