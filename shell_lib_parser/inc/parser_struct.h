/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_struct.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 16:19:20 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/21 12:49:34 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_STRUCT_H
# define PARSER_STRUCT_H

typedef struct	s_sdqh
{
	int			smpl;
	int			dbl;
	int			embrace;
}				t_sdqh;

#endif
