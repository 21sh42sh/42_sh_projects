/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 16:19:20 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 19:36:46 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "parser_struct.h"
# include "libft.h"
# include "cmd_line.h"

int			parser(void);
t_command	*pars_command(char **str, t_shell *sh);
int			pars_check_syntax_error(char *line);
char		pars_check_missing_quote(char *line);
char		*pars_replace_var(char *line, t_shell *sh);
char		*pars_escape_operator(char *str);
int			pars_is_balanced(const char *str, char c);
size_t		pars_count_occurence(const char *str, char c);
int			pars_is_closing(char *s);
int			pars_is_opening(char *s);
char		*pars_space_or_not(char *prev, char *next);
char		*pars_find_next_ending(char *str);
char		*pars_find_next_occurence(const char *str, char c);
char		*pars_get_operator(t_command *cmd, char *str);
char		*pars_get_options(t_command *cmd, char *s);
t_command	*pars_insert_cmd(t_command *cmd, t_shell *sh);
int			pars_is_redir(char *str, const char *str_begining);
void		*pars_print_error(char *target, char *err_message);
char		*pars_get_redir(t_command *cmd, char **str);
t_shell		*pars_parser(t_shell *sh);
int			heredoc(t_command *cmd, char *key, int flag);
int			quote_handler(t_shell *sh);
int			do_target_aggregator(char *target, int redir, t_command *cmd);
char		*pars_bang_history(t_shell *sh, char *line);
void		pars_rm_backslashes(t_command *cmds);

/*
** utils
*/

void		reset_sdqh(t_sdqh *var);
int			quote_verif(t_sdqh *var, char cur, char prev);
int			is_dir(char *target);
int			is_alpha_num_dash(char *str);
int			is_valid_target(char *target, int flag);
char		*create_tmp(char *beg, char *end);
int			set_fds_n(t_command *cmd, char *name, int flags, int n);
void		pars_rm_backslashes(t_command *cmds);
#endif
