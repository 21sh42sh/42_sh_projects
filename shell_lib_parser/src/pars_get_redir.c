/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_get_redir.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 14:06:49 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/20 11:12:29 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <sys/stat.h>
#include <errno.h>

static int	get_fd_1(char **str, char *pos)
{
	if ((pos - *str > 0) && lib_isdigit(*(pos - 1)))
	{
		pos--;
		if ((pos - *str) == 0 && lib_strchr("012", *pos))
			return (*pos - '0');
		else if ((pos - *str) > 0 && lib_strchr("012", *pos) \
			&& (lib_strchr("&|;", *(pos - 1)) || lib_isspace(*(pos - 1))))
			return (*pos - '0');
		else
			return (-1);
	}
	else if (*pos == '<')
		return (0);
	return (1);
}

static int	get_fd_2(t_command *cmd, int redir, char *target)
{
	int		fd;

	if (!target || !is_valid_target(target, cmd->redir[redir]))
		lib_strdel(&target);
	if (!target || !is_valid_target(target, cmd->redir[redir]))
		return (-1);
	if ((cmd->redir[redir] == RED_IN || cmd->redir[redir] == RED_OUT) &&
	do_target_aggregator(target, redir, cmd))
		fd = cmd->fd[redir];
	else if (cmd->redir[redir] == RED_IN)
		fd = set_fds_n(cmd, target, O_RDONLY, redir);
	else if (cmd->redir[redir] == RED_OUT)
		fd = set_fds_n(cmd, target, O_WRONLY | O_CREAT | O_TRUNC, redir);
	else if (cmd->redir[redir] == RED_OUT_ADD)
		fd = set_fds_n(cmd, target, O_WRONLY | O_CREAT | O_APPEND, redir);
	else if (cmd->redir[redir] == RED_IN_ADD)
	{
		if ((fd = heredoc(cmd, target, 1)) == 2)
			fd = -1;
	}
	else
		fd = -1;
	free(target);
	return (fd);
}

static char	*get_end(char *pos)
{
	while (*pos && !lib_isspace(*pos))
	{
		if (*pos == '\'' || *pos == '\"')
			pos = pars_find_next_occurence(pos, *pos);
		else if (*pos == '\\')
			pos++;
		pos++;
	}
	return (pos);
}

static char	*get_redir(t_command *cmd, char **str, char *pos)
{
	char	*end;
	char	*beg;
	int		redir;
	char	*target;

	if ((redir = get_fd_1(str, pos)) == -1)
		return ((char *)pars_print_error("Invalid fd value or syntax", NULL));
	beg = (pos - *str > 0 && lib_strchr("012", *(pos - 1))) ? pos - 1 : pos;
	if (*pos == '>')
		cmd->redir[redir] = (*(pos + 1) == '>') ? RED_OUT_ADD : RED_OUT;
	else if (*pos == '<')
		cmd->redir[redir] = (*(pos + 1) == '<') ? RED_IN_ADD : RED_IN;
	while (*pos && lib_strchr("><", *pos))
		pos++;
	while (lib_isspace(*pos))
		pos++;
	end = get_end(pos);
	if ((cmd->fd[redir] = get_fd_2(cmd, redir, create_tmp(pos, end))) == -1)
		return (NULL);
	if (!(target = create_tmp(beg, end)))
		return (NULL);
	*str = lib_str_replace(*str, target, "");
	if (target)
		free(target);
	return (pars_escape_operator(*str));
}

char		*pars_get_redir(t_command *cmd, char **str)
{
	char	*tmp;

	tmp = pars_escape_operator(*str);
	while (tmp && *tmp)
	{
		if (*tmp == '\'' || *tmp == '\"')
			tmp = pars_find_next_occurence(tmp, *tmp) + 1;
		else if (*tmp == '<' || *tmp == '>')
		{
			if (!(tmp = get_redir(cmd, str, tmp)))
				return (NULL);
		}
		else
			tmp++;
	}
	return (pars_escape_operator(*str));
}
