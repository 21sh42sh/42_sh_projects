/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_find_next_occurence.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 14:15:33 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 16:55:04 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** pars_find_next_occurence() works more or less like strchr().
** The only major difference between those 2 functions:
** pars_find_next_occurence() checks for escapated chars
*/

char		*pars_find_next_occurence(const char *str, char c)
{
	char	*tmp;

	if (!str || !*str)
		return (NULL);
	tmp = (char *)str;
	while (*tmp)
	{
		if ((*tmp == c) && ((tmp != str) ? (*(tmp - 1) != '\\') : 1))
			return (tmp);
		tmp++;
	}
	return (NULL);
}
