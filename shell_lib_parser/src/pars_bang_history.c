/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_bang_history.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 15:04:55 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/22 17:01:14 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "bang.h"

static char	*get_bang_pattern(char *pos, char *end)
{
	char	*bang;

	if (!pos || !*pos)
		return (NULL);
	bang = pos + 1;
	if (!*bang || bang == end)
		return (lib_strdup("!"));
	if (*bang == '!')
		bang = lib_strdup("!!");
	else if (lib_isdigit(*bang) || lib_strchr("+-", *bang))
	{
		while ((lib_isdigit(*bang) || lib_strchr("+-", *bang)) && bang < end)
			bang++;
		bang = lib_strncpy(lib_strnew(bang - pos), pos, bang - pos);
	}
	else
	{
		while (*bang && bang < end && !lib_isspace(*bang))
			bang++;
		bang = lib_strncpy(lib_strnew(bang - pos), pos, bang - pos);
	}
	return (bang);
}

static char	*get_bang_replace(char *pos, char *end)
{
	char	*bang;

	if (!(bang = lib_strchr(pos + 1, '^')) || bang > end)
		return (NULL);
	while (*bang && !lib_isspace(*bang) && bang < end)
		bang++;
	if (!(bang = lib_strncpy(lib_strnew(bang - pos), pos, bang - pos)))
		return (NULL);
	return (bang);
}

static char	*pars_bang_history_sub_1(char *pos, char *end, char **pattern, \
							char *line)
{
	while (pos && *pos && pos < end)
	{
		if (*pos == '\'' && pars_find_next_occurence(pos + 1, '\''))
			pos = pars_find_next_occurence(pos + 1, '\'') + 1;
		else if (*pos == '!' && (lib_isspace(*(pos + 1)) \
				|| lib_strchr("\"(=", *(pos + 1)) || pos + 1 == end))
			pos++;
		else if (*pos == '!' && ((pos > line) ? (*(pos - 1) != '\\') : 1))
		{
			*pattern = get_bang_pattern(pos, end);
			break ;
		}
		else
			pos++;
	}
	return (pos);
}

static char	*pars_bang_history_sub_2(t_shell *sh, char **pattern, char **line, \
							char **bang_ret)
{
	if (!(*bang_ret = bang(sh->history, *pattern)))
	{
		lib_putstr_fd("42sh: ", 2);
		lib_putstr_fd(*pattern, 2);
		lib_putendl_fd(" : event not found ", 2);
		lib_strdel(pattern);
		lib_strdel(line);
		return (NULL);
	}
	*line = lib_str_replace(*line, *pattern, *bang_ret);
	lib_strdel(bang_ret);
	lib_strdel(pattern);
	if (is_bang_exit_status("print"))
	{
		lib_putendl_fd(*line, 2);
		lib_strdel(line);
		return (NULL);
	}
	return (*line);
}

char		*pars_bang_history(t_shell *sh, char *line)
{
	char	*pos;
	char	*end;
	char	*pattern;
	char	*bang_ret;

	if (!line)
		return (NULL);
	pos = line;
	while (pos && *pos)
	{
		while (*pos && lib_isspace(*pos))
			pos++;
		end = pars_find_next_ending(pos);
		pattern = NULL;
		if (*pos == '^')
			pattern = get_bang_replace(pos, end);
		else
			pos = pars_bang_history_sub_1(pos, end, &pattern, line);
		if (pattern)
			return (pars_bang_history_sub_2(sh, &pattern, &line, &bang_ret));
		pos = (*pos) ? pars_escape_operator(pos) : pos;
	}
	return (line);
}
