/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_quote_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 12:51:19 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/22 19:40:02 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** quote_verif() is used to check if the current char is bettew quotes or not.
*/

void	reset_sdqh(t_sdqh *var)
{
	var->dbl = 0;
	var->smpl = 0;
	var->embrace = 0;
}

int		quote_verif(t_sdqh *var, char cur, char prev)
{
	if (cur == '\'' && prev != '\\')
		var->smpl = (var->smpl) ? 0 : 1;
	else if (cur == '\"' && prev != '\\')
		var->dbl = (var->dbl) ? 0 : 1;
	if (var->embrace == 0 && (var->smpl || var->dbl))
	{
		if (var->smpl && var->dbl == 0)
			var->embrace = '\'';
		else if (var->smpl == 0 && var->dbl)
			var->embrace = '\"';
		else
			var->embrace = -1;
	}
	else if (var->embrace && var->smpl == 0 && var->dbl == 0)
		var->embrace = 0;
	return (var->embrace);
}
