/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_handler.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/01 23:56:32 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/10/18 19:44:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** /!\ Needs to be initialized: `is_line_complete(NULL, NULL, 0)` /!\
** check if the key is found in input starting after the first '\n'
**  or after the last search result.
** args:
** _____ flag: 0 => initialise the function, 1 => use it.
** return:
** _______ 0 if the key was found
** _______ 1 otherwise.
*/

static int		is_line_complete(char *input, char *key, int flag)
{
	static int	line;
	int			tmp;
	char		**arr_line;

	if (flag == 0)
	{
		line = 1;
		return (0);
	}
	arr_line = lib_strsplit(input, '\n');
	tmp = line;
	while (arr_line && arr_line[line] && key && lib_strcmp(arr_line[line], key))
		line++;
	if (arr_line && arr_line[line] && key && !lib_strcmp(arr_line[line], key))
	{
		line++;
		lib_arr_del(arr_line);
		return (0);
	}
	line = tmp;
	lib_arr_del(arr_line);
	return (1);
}

/*
** launch the cmd_line so that the user can enter one string and join it
**  to the current line.
** return:
** _______ sh
*/

static t_shell	*line_fill(t_shell *sh)
{
	char	**arr;

	arr = cmd_line(NULL, "$heredoc> ", 2);
	if (!arr)
		return (NULL);
	sh->history[0] = lib_strjoinfree(sh->history[0], "\n", 1);
	sh->history[0] = lib_strjoinfree(sh->history[0], arr[0], 1);
	lib_arr_del(arr);
	return (sh);
}

/*
** /!\ Needs to be initialized: `get_heredoc(NULL, NULL, 0)` /!\
** put the correspondant entries from the sh->history[0]
**  in the string pointed by storage.
** args:
** _____ flag: 0 => initialise the function, 1 => use it.
** return:
** _______ -2 if flag == 0
** _______ 1 on error
** _______ 0 on success
*/

static int		get_heredoc(t_command *cmd, char *key, int flag)
{
	static int	line;
	int			ret;
	char		**arr_line;

	if (flag == 0)
	{
		line = 1;
		return (-2);
	}
	ret = 0;
	cmd->heredoc = lib_strnew(0);
	arr_line = lib_strsplit(cmd->sh->history[0], '\n');
	while (arr_line && arr_line[line] && lib_strcmp(arr_line[line], key))
	{
		cmd->heredoc = lib_strjoinfree(cmd->heredoc, arr_line[line], 1);
		cmd->heredoc = lib_strjoinfree(cmd->heredoc, "\n", 1);
		line++;
	}
	if (!arr_line || !arr_line[line])
		ret = 1;
	line++;
	lib_arr_del(arr_line);
	return (ret);
}

/*
** /!\ Needs to be initialized: `heredoc(NULL, NULL, 0)` /!\
** heredoc main function
** check the line from the beginning or from the last stop
** and return a fd with the words in it.
** args:
** _____ flag: 0 => initialise the function, 1 => use it.
** modify:
** _______ fd
** return:
** _______ -2 if flag == 0
** _______ 1 on error
** _______ 2 on user exit
** _______ 0 on success
*/

int				heredoc_bash4(t_command *cmd, char *key, int flag)
{
	t_shell	*sh;

	if (flag == 0)
	{
		is_line_complete(NULL, NULL, 0);
		get_heredoc(NULL, NULL, 0);
		return (-2);
	}
	sh = cmd->sh;
	while (is_line_complete(cmd->sh->history[0], key, 1) && sh)
		sh = line_fill(cmd->sh);
	if (!sh)
		return (2);
	if (get_heredoc(cmd, key, 1))
		return (1);
	return (0);
}
