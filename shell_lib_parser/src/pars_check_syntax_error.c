/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_check_syntax_error.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/02 02:03:28 by mafagot           #+#    #+#             */
/*   Updated: 2016/08/02 02:16:03 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

int			pars_check_syntax_error(char *line)
{
	while (line && *line)
	{
		if (lib_strchr("<>&|", *line) && lib_strlen(line) > 2 \
			&& *line == *(line + 1) && *line == *(line + 2))
		{
			write(2, "42sh: syntax error near unexpected token `", 42);
			write(2, line, 1);
			write(2, "\'\n", 2);
			return (1);
		}
		if (*line == ';' && lib_strlen(line) > 1 && *line == *(line + 1))
		{
			write(2, "42sh: syntax error near unexpected token `;;'\n", 46);
			return (1);
		}
		line++;
	}
	return (0);
}
