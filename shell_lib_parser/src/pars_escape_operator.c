/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_escape_operator.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 20:02:38 by mafagot           #+#    #+#             */
/*   Updated: 2016/07/01 15:45:09 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <parser.h>

/*
** pars_escape_operator() escape the operator (; || &&)
** at the begining of an instruction
*/

char		*pars_escape_operator(char *str)
{
	if (!str)
		return (NULL);
	if (*str == ';')
		str++;
	else if (*str == '|')
	{
		if (*str == *(str + 1))
			str++;
		str++;
	}
	else if (*str == '&' && *(str + 1) == '&')
		str += 2;
	return (str);
}
