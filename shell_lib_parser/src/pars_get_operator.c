/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_get_operator.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 19:17:07 by mafagot           #+#    #+#             */
/*   Updated: 2016/07/01 15:45:36 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** Put the adequate value of operator in the t_command
** Returns the position of the char* line after the operator.
** Returns NULL if something wrong happens.
*/

char		*pars_get_operator(t_command *cmd, char *str)
{
	if (!str || !*str)
		return (NULL);
	if (*str == '|')
		cmd->type = (*(str + 1) == '|') ? OP_ELSE : OP_PIPE;
	else if (*str == '&')
		cmd->type = (*(str + 1) == '&') ? OP_IF : OP_REG;
	else
		cmd->type = OP_REG;
	return (pars_escape_operator(str));
}
