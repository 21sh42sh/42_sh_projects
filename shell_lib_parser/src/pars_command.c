/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 19:32:17 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/30 17:35:18 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static t_command	*parse_del_cmd(t_command **cmd)
{
	if (cmd)
		del_all_cmds(*cmd);
	*cmd = NULL;
	return (*cmd);
}

t_command			*pars_command(char **str, t_shell *sh)
{
	char		*tmp;
	t_command	*cmd;

	if (!*str || !**str || !sh || !(cmd = new_cmd(sh)))
		return (NULL);
	tmp = *str;
	if (!(tmp = pars_get_operator(cmd, tmp)))
		return (parse_del_cmd(&cmd));
	if (!(tmp = pars_get_redir(cmd, str)))
		return (parse_del_cmd(&cmd));
	if (!(tmp = pars_get_options(cmd, tmp)))
		return (parse_del_cmd(&cmd));
	return (cmd);
}
