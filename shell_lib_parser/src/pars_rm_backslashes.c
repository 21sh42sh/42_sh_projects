/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_rm_backslashes.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 17:34:41 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/22 18:27:37 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static void	rm_backslashes(char **opts)
{
	char	*tmp;
	size_t	i;

	while (opts && *opts)
	{
		i = 0;
		tmp = *opts;
		while (tmp && tmp[i])
		{
			if (tmp[i] == '\\')
			{
				if (tmp[i + 1] == '\\')
				{
					lib_strcpy(tmp + i, tmp + i + 1);
					i += 1;
				}
				else if (lib_strchr("\'\"`", tmp[i + 1]))
					lib_strcpy(tmp + i, tmp + i + 1);
			}
			i += 1;
		}
		opts++;
	}
}

void		pars_rm_backslashes(t_command *cmds)
{
	if (cmds)
	{
		if (cmds->options)
			rm_backslashes(cmds->options);
		if (cmds->left)
			pars_rm_backslashes(cmds->left);
		if (cmds->right)
			pars_rm_backslashes(cmds->right);
	}
}
