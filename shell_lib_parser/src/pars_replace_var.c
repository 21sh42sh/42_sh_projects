/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_replace_var.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 21:35:02 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/22 19:37:58 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <sys/stat.h>

/*
** pars_replace_var() finds all the potential variables and replace them with
** their appropriate values found in the env.
** If the variable has no value, the var is replaced by an empty string.
**
** Usage : line = pars_replace_var(line, sh)
*/

static char	*replace_var(char *line, char *pos, t_shell *sh)
{
	char	*tmp;
	size_t	i;

	i = 1;
	while (*(pos + i) == '_' || lib_isalpha(*(pos + i)))
		i++;
	if (!(tmp = lib_strncpy(lib_strnew(i), pos, i)))
		return (NULL);
	line = lib_str_replace(line, tmp, get_shell_env_by_name(sh, tmp + 1));
	free(tmp);
	return (line);
}

static char	*replace_tilde(t_shell *sh, char **line, char *pos, char **check)
{
	long	len;
	char	*home;
	char	*end;

	home = get_shell_env_by_name(sh, "HOME");
	end = pars_find_next_ending(pos);
	if (!line || !*line || !pos || (len = pos - *line) < 0)
		return (NULL);
	if (pos + 1 == end || *(pos + 1) == '/' || lib_isspace(*(pos + 1)))
	{
		*line = lib_strsub_pos(*line, "~", home, pos);
		*check = *line + len;
	}
	return (*check + 1);
}

static char	*replace_return(char *line, t_shell *sh)
{
	char	*tmp;

	if (!(tmp = lib_itoa(sh->exit_status)))
		return (NULL);
	line = lib_str_replace(line, "$?", tmp);
	free(tmp);
	return (line);
}

static char	*pars_repalce_var_sub(char *line, t_shell *sh, t_sdqh *qt)
{
	char	*pos;
	char	*check;
	int		ret;

	pos = line;
	while (pos && *pos)
	{
		check = pos;
		if ((ret = quote_verif(qt, *pos, (pos > line) ? *(pos - 1) : 0)) == -1)
			return (NULL);
		if (ret != '\'' && *pos == '$' && pos > line && *(pos - 1) != '\\' \
			&& (*(pos + 1) == '_' || lib_isalpha(*(pos + 1))))
			line = replace_var(line, pos, sh);
		else if (!ret && *pos == '~' && pos > line && lib_isspace(*(pos - 1)))
			pos = replace_tilde(sh, &line, pos, &check);
		else if (ret != '\'' && *pos == '$' && *(pos + 1) == '?')
			line = replace_return(line, sh);
		else
			pos++;
		if (!line)
			return (NULL);
		pos = (pos == check + 1) ? pos : line;
	}
	return (line);
}

char		*pars_replace_var(char *line, t_shell *sh)
{
	t_sdqh	qt;

	if (!line || !*line)
		return (line);
	reset_sdqh(&qt);
	return (pars_repalce_var_sub(line, sh, &qt));
}
