/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_parser.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 19:34:56 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/22 20:21:18 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static char	*pars_parser_find_insert_cmd(char *line, t_shell *sh)
{
	char		*ending;
	char		*tmp;
	t_command	*cmd;

	while (lib_isspace(*line))
		line++;
	tmp = NULL;
	if ((ending = pars_find_next_ending(pars_escape_operator(line))))
	{
		tmp = lib_strnew(ending - line + 1);
		lib_strncpy(tmp, line, ending - line);
	}
	else
		tmp = lib_strdup(line);
	if (!(cmd = pars_command(&tmp, sh)))
	{
		lib_strdel(&tmp);
		return (NULL);
	}
	pars_insert_cmd(cmd, sh);
	if (tmp)
		free(tmp);
	return (ending);
}

static int	pars_bang_history_handler(t_shell *sh)
{
	char		*to_be_freed;
	int			flag;

	flag = 0;
	while (1)
	{
		if (!(to_be_freed = pars_bang_history(sh, lib_strdup(sh->history[0]))))
		{
			lib_arr_del_one(sh->history, 0);
			return (1);
		}
		if (lib_strcmp(to_be_freed, sh->history[0]))
		{
			lib_strdel(&sh->history[0]);
			sh->history[0] = to_be_freed;
			flag = 1;
		}
		else
		{
			lib_strdel(&to_be_freed);
			break ;
		}
	}
	(flag) ? lib_putendl_fd(sh->history[0], 1) : (void)flag;
	return (0);
}

t_shell		*pars_parser(t_shell *sh)
{
	char		*line;
	char		*to_be_freed;

	if (!sh)
		return (NULL);
	if (quote_handler(sh))
		return (sh);
	to_be_freed = NULL;
	if (pars_bang_history_handler(sh))
		return (sh);
	line = lib_strdup(sh->history[0]);
	line = pars_replace_var(line, sh);
	if (pars_check_syntax_error(line))
		return (sh);
	heredoc(NULL, NULL, 0);
	to_be_freed = line;
	while (line && *line)
		line = pars_parser_find_insert_cmd(line, sh);
	lib_strdel(&to_be_freed);
	if (sh)
		pars_rm_backslashes(sh->cmds);
	return (sh);
}
