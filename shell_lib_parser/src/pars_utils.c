/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:36:19 by mafagot           #+#    #+#             */
/*   Updated: 2016/10/17 16:59:33 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <sys/stat.h>
#include <errno.h>

int		is_dir(char *target)
{
	struct stat	tmp;

	if (lstat(target, &tmp) != -1 && S_ISDIR(tmp.st_mode))
		return (1);
	return (0);
}

int		is_alpha_num_dash(char *str)
{
	if (!str || !*str)
		return (0);
	while (*str)
	{
		if (!lib_isalpha(*str) && !lib_isdigit(*str) && !lib_strchr("-_", *str))
			return (0);
		str++;
	}
	return (1);
}

int		is_valid_target(char *target, int flag)
{
	if (flag == RED_IN_ADD)
		return (is_alpha_num_dash(target));
	if (is_dir(target))
	{
		pars_print_error(target, "Is a directory");
		return (0);
	}
	else if (access(target, W_OK) == -1 && !access(target, F_OK))
	{
		if (EACCES)
		{
			pars_print_error(target, "Permission denied");
			return (0);
		}
		else if (ENOENT && !ENOTDIR)
			return (1);
		else
		{
			pars_print_error(target, NULL);
			return (0);
		}
	}
	else
		return (1);
}

char	*create_tmp(char *beg, char *end)
{
	char	*ret;

	if ((beg > end) || !(ret = lib_strnew(end - beg)))
		return (NULL);
	ret = lib_strncpy(ret, beg, end - beg);
	return (ret);
}

int		set_fds_n(t_command *cmd, char *name, int flags, int n)
{
	lib_strdel(&(cmd->fds[n].name));
	cmd->fds[n].name = lib_strdup(name);
	cmd->fds[n].flags = flags;
	return (n);
}
