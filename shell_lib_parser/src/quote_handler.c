/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quote_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/01 23:56:32 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/27 16:46:26 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** launch the cmd_line so that the user can enter one string and join it
**  to the current line.
** return:
** _______ sh
*/

static t_shell	*line_fill(t_shell *sh)
{
	char	**arr;

	arr = cmd_line(NULL, "quote$> ", 2);
	if (!arr)
		return (NULL);
	sh->history[0] = lib_strjoinfree(sh->history[0], "\n", 1);
	sh->history[0] = lib_strjoinfree(sh->history[0], arr[0], 1);
	lib_arr_del(arr);
	return (sh);
}

/*
** check if their is a problem with the quote and make the user
** completes the line.
** return:
** _______ 1 if the user canceled (CTRL-C, ...)
** _______ 0 if everything is fine.
*/

int				quote_handler(t_shell *sh)
{
	char	*line;

	line = lib_strdup(sh->history[0]);
	line = pars_replace_var(line, sh);
	while (line && sh && pars_check_missing_quote(line))
	{
		sh = line_fill(sh);
		if (!sh)
			break ;
		free(line);
		line = lib_strdup(sh->history[0]);
		line = pars_replace_var(line, sh);
	}
	if (line)
		free(line);
	if (!sh)
		return (1);
	return (0);
}
