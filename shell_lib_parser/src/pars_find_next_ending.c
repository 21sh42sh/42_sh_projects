/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_find_next_ending.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 14:52:10 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/17 18:54:43 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** look for the nex
*/

static int	good_sequence(char *str)
{
	if (!str || !*str)
		return (0);
	if (*str == '|' || *str == ';')
		return (1);
	else if (*str == '&' && *(str + 1) == '&')
		return (1);
	else
		return (0);
}

static int	is_quote_balanced(char *beg, char *end)
{
	size_t	count_single;
	size_t	count_double;

	if (!beg || !end || (end < beg))
		return (0);
	if (beg == end)
		return (*beg != '\'' && *beg != '\"');
	count_single = 0;
	count_double = 0;
	while (*beg && beg <= end)
	{
		if (*beg == '\'')
			count_single++;
		else if (*beg == '\"')
			count_double++;
		beg++;
	}
	return ((count_single % 2 == 0) && (count_double % 2 == 0));
}

char		*pars_find_next_ending(char *str)
{
	char	*tmp;

	if (!str || !*str)
		return (NULL);
	tmp = (char *)str;
	while (*tmp)
	{
		if (is_quote_balanced(str, tmp) \
			&& good_sequence(tmp) && ((tmp == str) ? 1 : (*(tmp - 1) != '\\')))
			return (tmp);
		tmp++;
	}
	return (tmp);
}
