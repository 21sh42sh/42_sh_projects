/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_is_closing.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 15:38:54 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 16:48:53 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

int		pars_is_closing(char *s)
{
	if (!s)
		return (0);
	if (*s == '\"')
		return ('\"');
	else if (*s == '\'')
		return ('\'');
	else if (*s == ')')
		return ('(');
	else if (*s == ']')
		return ('[');
	else if (*s == '`')
		return ('`');
	else if (*s == '}')
		return ('{');
	else
		return (0);
}
