/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_is_opening.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 15:38:45 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 16:48:46 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

int		pars_is_opening(char *s)
{
	if (!s)
		return (0);
	if (*s == '\"')
		return ('\"');
	else if (*s == '\'')
		return ('\'');
	else if (*s == '(')
		return (')');
	else if (*s == '[')
		return (']');
	else if (*s == '`')
		return ('`');
	else if (*s == '{')
		return ('}');
	else
		return (0);
}
