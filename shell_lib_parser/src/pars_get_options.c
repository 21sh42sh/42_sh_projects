/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_get_options.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 18:33:02 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/23 15:40:53 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static char	*pars_get_options_sub(char *s, char *orig, char **tmp, t_sdqh *sdqh)
{
	int		verif;

	while (s && *s && !lib_isspace(*s))
	{
		if ((verif = quote_verif(sdqh, *s, (s > orig) ? *(s - 1) : 0)) == -1)
			return (NULL);
		if (verif)
		{
			s++;
			while ((verif = quote_verif(sdqh, *s, (s > orig) ? *(s - 1) : 0)) \
				&& verif != -1)
			{
				*tmp = lib_strins_c(*tmp, *s, -1, 1);
				s++;
			}
			if (verif == -1)
				return (NULL);
		}
		else
			*tmp = lib_strins_c(*tmp, *s, -1, 1);
		s++;
	}
	return (s);
}

char		*pars_get_options(t_command *cmd, char *s)
{
	char	*orig;
	char	*tmp;
	char	**opts;
	t_sdqh	sdqh;

	opts = NULL;
	orig = s;
	reset_sdqh(&sdqh);
	while (s && *s && !lib_strchr("<>|&;", *s) && !pars_is_redir(s, orig))
	{
		while (*s && lib_isspace(*s))
			s++;
		tmp = NULL;
		if (!(s = pars_get_options_sub(s, orig, &tmp, &sdqh)))
			return (NULL);
		if (tmp)
		{
			opts = lib_arr_push_back(opts, tmp);
			lib_strdel(&tmp);
		}
	}
	cmd->options = opts;
	return (s);
}
