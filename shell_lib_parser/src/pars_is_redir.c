/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_is_redir.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 19:22:01 by mafagot           #+#    #+#             */
/*   Updated: 2016/06/30 19:25:40 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** Tell if it's a redirection (for example like : 2>/dev/null)
** Be extra careful of the position to be checked. (ie the digit in the middle)
*/

int			pars_is_redir(char *str, const char *str_begining)
{
	if (!str || !*str || !str_begining || str - str_begining < 1)
		return (0);
	if (lib_isdigit(*str) && lib_isspace(*(str - 1)) && *(str + 1) == '>')
		return (1);
	return (0);
}
