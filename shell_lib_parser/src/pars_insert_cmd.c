/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_insert_cmd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 19:32:57 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/17 18:58:05 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** Basic implementation of insertion of the t_command
*/

t_command	*pars_insert_cmd(t_command *cmd, t_shell *sh)
{
	static t_command	*stck;
	t_command			*tmp;

	if (!cmd)
		return (NULL);
	if (cmd->type == OP_PIPE)
	{
		if (!sh->cmds)
			return (NULL);
		stck->right = cmd;
	}
	else if (sh->cmds)
	{
		tmp = sh->cmds;
		while (tmp->left)
			tmp = tmp->left;
		tmp->left = cmd;
	}
	else
		sh->cmds = cmd;
	stck = cmd;
	return (stck);
}
