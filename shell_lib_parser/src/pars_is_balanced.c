/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_is_balanced.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 14:06:01 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 16:48:34 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** pars_is_balanced() indicate if a string has a balanced (ie even)
** number of a char passed as argument.
** Works to a certain limit with some brackets. See code for further details.
**
** It returns 1 (TRUE) if it's balanced and 0 (FALSE) otherwise.
*/

static char	get_opening_char(char c)
{
	if (c == '(' || c == ')')
		return ('(');
	else if (c == '[' || c == ']')
		return ('[');
	else if (c == '{' || c == '}')
		return ('{');
	else
		return (0);
}

static char	get_closing_char(char c)
{
	if (c == '(' || c == ')')
		return (')');
	else if (c == '[' || c == ']')
		return (']');
	else if (c == '{' || c == '}')
		return ('}');
	else
		return (0);
}

int			pars_is_balanced(const char *str, char c)
{
	char	c_open;
	char	c_close;

	if (!(lib_strchr("{}[]()", c)))
		return ((pars_count_occurence(str, c) % 2 == 0) ? 1 : 0);
	c_open = get_opening_char(c);
	c_close = get_closing_char(c);
	if (!c_open || !c_close)
		return (0);
	if (pars_count_occurence(str, c_open) % 2 != 0)
		return (0);
	return ((pars_count_occurence(str, c_close) % 2 == 0) ? 1 : 0);
}
