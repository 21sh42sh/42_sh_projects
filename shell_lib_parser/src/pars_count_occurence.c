/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_count_occurence.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 14:06:23 by mafagot           #+#    #+#             */
/*   Updated: 2016/06/28 15:25:53 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** pars_count_occurence() return the number of occurence of a char inside a
** string. Escaped chars are not counted.
*/

size_t		pars_count_occurence(const char *str, char c)
{
	size_t	count;
	char	*tmp;

	if (!str)
		return (0);
	count = 0;
	tmp = (char *)str;
	while (*tmp)
	{
		if ((*tmp == c) && ((tmp != str) ? (*(tmp - 1) != '\\') : 1))
			count++;
		tmp++;
	}
	return (count);
}
