/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_check_missing_quote.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/17 17:25:49 by mafagot           #+#    #+#             */
/*   Updated: 2016/11/23 15:17:14 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

char	pars_check_missing_quote(char *line)
{
	char	*tmp;
	char	*beg;

	if (!line || !*line)
		return (0);
	beg = line;
	while ((tmp = pars_find_next_ending(pars_escape_operator(line))) && *tmp)
		line = tmp;
	while (*line)
	{
		if ((*line == '\'' || *line == '\"') \
			&& (line > beg && *(line - 1) != '\\'))
		{
			if ((tmp = pars_find_next_occurence(line + 1, *line)))
				line = tmp;
			else
				return (*line);
		}
		line++;
	}
	return (0);
}
