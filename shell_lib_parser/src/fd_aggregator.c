/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fd_aggregator.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 18:19:42 by hlequien          #+#    #+#             */
/*   Updated: 2016/10/19 13:15:03 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <parser.h>
#include <libft.h>

/*
** To be integrated
*/

int	do_target_aggregator(char *target, int redir, t_command *cmd)
{
	if (target && cmd)
		if ((*target == '&') && lib_strchr("012-", *(target + 1)))
		{
			if (*(target + 1) == '-')
				cmd->fd[redir] = 1000000;
			else
				cmd->fd[redir] = *(target + 1) - '0';
			return (1);
		}
	return (0);
}
