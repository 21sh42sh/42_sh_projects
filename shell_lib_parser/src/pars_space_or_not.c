/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_space_or_not.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 15:32:06 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/17 18:58:57 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

/*
** pars_space_or_not() returns an empty string or just " ".
** Strings returned are NOT malloced
*/

char		*pars_space_or_not(char *prev, char *next)
{
	size_t	len_p;

	if (!prev || !*prev || !next || !*next)
		return ("");
	len_p = lib_strlen(prev);
	if (lib_isdigit(*(prev + len_p - 1)) \
		&& lib_strlen(next) == 1 && *next == '>')
		return ("");
	else if (pars_is_opening(prev + len_p - 1))
		return ("");
	else if (lib_strlen(next) == 1 && pars_is_closing(next))
		return ("");
	else
		return (" ");
}
