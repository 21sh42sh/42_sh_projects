/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_handler_bash3.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/01 23:56:32 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/04 16:03:39 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static int		get_return(void)
{
	if (is_cmd_line_exit_status("Ctrl-D"))
		return (1);
	if (is_cmd_line_exit_status("Ctrl-C"))
		lib_putendl("");
	return (2);
}

/*
** /!\ Needs to be initialized: `get_heredoc(NULL, NULL, 0)` /!\
** put the correspondant entries from the sh->history[0]
**  in the string pointed by storage.
** args:
** _____ flag: 0 => initialise the function, 1 => use it.
** return:
** _______ 2 if interrupted by user
** _______ 1 on error
** _______ 0 on success
*/

static int		get_heredoc(t_command *cmd, char *key)
{
	char		**arr;

	cmd->heredoc = lib_strnew(0);
	while (42)
	{
		arr = cmd_line(NULL, "$heredoc> ", 3);
		if (!arr)
		{
			if (get_return() == 1)
				cmd->heredoc = lib_strjoinfree(cmd->heredoc, "\n", 1);
			return (get_return());
		}
		if (!lib_strcmp(arr[0], key))
		{
			if (cmd->heredoc && cmd->heredoc[0])
				cmd->heredoc = lib_strjoinfree(cmd->heredoc, "\n", 1);
			lib_arr_del(arr);
			return (0);
		}
		if (cmd->heredoc[0])
			cmd->heredoc = lib_strjoinfree(cmd->heredoc, "\n", 1);
		cmd->heredoc = lib_strjoinfree(cmd->heredoc, arr[0], 1);
		lib_arr_del(arr);
	}
	return (2);
}

/*
** /!\ Don't needs to be initialized: `heredoc(NULL, NULL, 0)` /!\
** heredoc main function
** check the line from the beginning or from the last stop
** and return a fd with the words in it.
** args:
** _____ flag: 0 => initialise the function, 1 => use it.
** modify:
** _______ fd
** return:
** _______ -2 if flag == 0
** _______ 1 on error
** _______ 2 on user exit
** _______ 0 on success
*/

int				heredoc(t_command *cmd, char *key, int flag)
{
	int ret;

	ret = 1;
	if (flag == 0)
	{
		flag = -2;
		return (flag);
	}
	ret = get_heredoc(cmd, key);
	if (ret == 2)
		return (2);
	if (ret)
		return (1);
	return (0);
}
