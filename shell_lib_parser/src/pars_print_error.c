/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_print_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 15:36:12 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/29 17:35:11 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <libft.h>

void	*pars_print_error(char *target, char *err_message)
{
	write(2, "42sh: ", 6);
	if (err_message && *err_message)
	{
		write(2, target, lib_strlen(target));
		write(2, ": ", 2);
		write(2, err_message, lib_strlen(err_message));
		write(2, "\n", 1);
	}
	else
		lib_putendl_fd(target, 2);
	return (NULL);
}
