/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   posix_bltn.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 14:27:17 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/02 14:27:20 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POSIX_BLTN_H
# define POSIX_BLTN_H

# define POSIX_CD_L 1
# include <shell_wrap.h>

typedef struct	s_cd_data
{
	char	*env_pwd;
	char	*env_old_pwd;
	char	*env_home;
	char	*env_cdpath;
	char	*dest;
	char	mode;
	char	hyphen;

}				t_cd_data;

int				posix_bltn_cd(t_command *cmd);
char			posix_find_mode(t_command *cmd);
char			*posix_find_path(t_command *cmd);
int				posix_bltn_env(t_command *cmd);
#endif
