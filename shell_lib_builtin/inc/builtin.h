/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 21:29:43 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_H
# define BUILTIN_H

# include "sys/param.h"
# include <unistd.h>
# include "libft.h"
# include "shell_wrap_struct.h"
# include "shell_wrap.h"
# include "cmd_line.h"
# include <posix_bltn.h>
# define STDERR 2
# define EXIT_SHELL -3

/*
** Builtin
*/
int		bltn_echo(t_command *cmd);
int		bltn_env(t_command *cmd);
int		bltn_setenv(t_command *cmd);
int		bltn_unsetenv(t_command *cmd);
int		bltn_cd(t_command *cmd);
int		bltn_exit(t_command *cmd);
void	bltn_alias_sort(char **arr);
char	**bltn_alias_set(char **alias, char *line);
int		bltn_alias(t_command *cmd);
int		bltn_unalias(t_command *cmd);
int		is_good_alias(char *alias);
void	env_del_one(t_shell *sh, char *name);
char	*get_getcwd(void);
char	*follow_path_from(char *from, char *path);
int		bltn_where(t_command *cmd);
int		bltn_which(t_command *cmd);

/*
** history builtin
*/

void	print_history(char **history, char **options, int fd);
void	help(t_command *cmd);
int		history_load(t_command *cmd);
int		history_sub_save(t_command *cmd);
int		history_add_args(t_command *cmd);
int		history_del_n(t_command *cmd);
int		history_a(t_command *cmd, int flag);
int		history_n(t_command *cmd, int flag);
int		history_p(t_command *cmd);
int		history_w(t_command *cmd);
int		history_append(char **history, char *file_name, int n, int s_er);
int		history_write(char **history, char *file_name, int s_er);
int		history_retrieve(char ***history, char *file_name, int n, int s_er);
int		bltn_history(t_command *cmd);

/*
** tools
*/

char	**check_options_bltn_cd(char **av, char *options);
char	*bltn_get_options_(char **av, char *opts);
char	**bltn_get_options(char **av, char *opts, char **optf);
int		get_nb_width(int nb);
#endif
