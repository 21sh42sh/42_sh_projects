/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_alias.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/28 14:52:43 by mafagot           #+#    #+#             */
/*   Updated: 2016/09/27 17:47:46 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

void		bltn_alias_sort(char **arr)
{
	size_t	i;
	size_t	j;
	size_t	size;

	size = lib_arr_len(arr);
	if (size < 2)
		return ;
	i = 0;
	while (i < size - 1)
	{
		j = 0;
		while (j < size - 1 - i)
		{
			if (lib_strcmp(arr[j + 1], arr[j]) < 0)
				lib_arr_swap(arr, j + 1, j);
			j++;
		}
		i++;
	}
}

static char	*pars_find_next_occurence(const char *str, char c)
{
	char	*tmp;

	if (!str || !*str)
		return (NULL);
	tmp = (char *)str;
	while (*tmp)
	{
		if ((*tmp == c) && ((tmp != str) ? (*(tmp - 1) != '\\') : 1))
			return (tmp);
		tmp++;
	}
	return (NULL);
}

int			is_good_alias(char *alias)
{
	char	*s;

	if (!alias || !*alias)
		return (0);
	s = alias;
	while (*s && (lib_isalpha(*s) || lib_strchr("!%,@_", *s)
			|| lib_isdigit(*s)))
		s++;
	if (*s != '=')
		return (0);
	s++;
	if ((*s == '\"' || *s == '\''))
	{
		return ((pars_find_next_occurence(s + 1, *s)
			== s + lib_strlen(s) - 1));
	}
	while (*s && (lib_isalpha(*s) || lib_strchr("!%,@_", *s)
			|| lib_isdigit(*s)))
		s++;
	return ((*s == '\0'));
}

static int	is_already_set(char **alias, char *s)
{
	while (alias && *alias)
	{
		if (!lib_strncmp(*alias, s, strchr(s, '=') - s))
		{
			free(*alias);
			*alias = s;
			return (1);
		}
		alias++;
	}
	return (0);
}

char		**bltn_alias_set(char **alias, char *line)
{
	char	**ret;
	char	*dup;

	dup = lib_strdup(line);
	if (*(lib_strchr(dup, '=') + 1) == '"')
	{
		*(lib_strchr(dup, '=') + 1) = '\'';
		dup[lib_strlen(dup) - 1] = '\'';
	}
	if (*(lib_strchr(dup, '=') + 1) != '\'')
	{
		dup = lib_strins_c(dup, '\'', lib_strchr(dup, '=') + 1 - dup, 1);
		dup = lib_strins_c(dup, '\'', lib_strlen(dup), 1);
	}
	ret = (is_already_set(alias, dup)) ? alias : lib_arr_push_back(alias, dup);
	bltn_alias_sort(ret);
	return (ret);
}

int			alias_add(t_command *cmd)
{
	char	**new_alias;

	if (is_good_alias(cmd->options[1]))
		new_alias = bltn_alias_set(cmd->sh->alias, cmd->options[1]);
	else
		return (1);
	cmd->sh->alias = new_alias;
	return (0);
}

static char	*id_alias(t_command *cmd)
{
	char	**alias;
	char	*ret;
	size_t	len;

	alias = cmd->sh->alias;
	ret = NULL;
	if (lib_arr_len(cmd->options) == 2 \
			&& (ret = lib_strchr(cmd->options[1], '=')))
		len = cmd->options[1] - ret;
	else if (lib_arr_len(cmd->options) == 2)
		len = lib_strlen(cmd->options[1]);
	else
		return (NULL);
	while (*alias && lib_strncmp(*alias, cmd->options[1], len))
		alias++;
	ret = *alias;
	return (ret);
}

static int	alias_remove(t_command *cmd)
{
	char	**old_alias;
	char	**new_alias;
	char	*to_remove;
	size_t	i;

	to_remove = id_alias(cmd);
	old_alias = cmd->sh->alias;
	if (!to_remove || !old_alias \
			|| !(new_alias = lib_arr_new(lib_arr_len(old_alias) - 1)))
		return (1);
	i = 0;
	while (old_alias[i] && old_alias[i] != to_remove)
	{
		new_alias[i] = lib_strdup(old_alias[i]);
		i++;
	}
	while (old_alias[i] && old_alias[i + 1])
	{
		new_alias[i] = lib_strdup(old_alias[i + 1]);
		i++;
	}
	new_alias[i] = NULL;
	lib_arr_del(old_alias);
	cmd->sh->alias = new_alias;
	return (0);
}

static int	alias_print(t_command *cmd)
{
	char	*tmp;

	if ((tmp = id_alias(cmd)))
	{
		lib_putendl_fd((const char *)tmp, cmd->fd[1]);
		return (0);
	}
	return (1);
}

int			bltn_alias(t_command *cmd)
{
	size_t	len;

	len = lib_arr_len(cmd->options);
	if (len == 1)
	{
		lib_arr_putstr_fd((const char **)(cmd->sh->alias), cmd->fd[1]);
		return (0);
	}
	else if (len == 2 && lib_strchr(cmd->options[1], '='))
		return (alias_add(cmd));
	else if (len == 2)
		return (alias_print(cmd));
	else
		return (1);
}

int			bltn_unalias(t_command *cmd)
{
	if (lib_arr_len(cmd->options) == 2 \
			&& !lib_strcmp(cmd->options[0], "unalias"))
		return (alias_remove(cmd));
	else
		return (1);
}
