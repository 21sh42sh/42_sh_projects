/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   posix_bltn_cd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/18 16:30:41 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <builtin.h>
#include <sys/stat.h>

static void	posix_init_cd(t_cd_data *data, t_command *cmd)
{
	if (cmd && data)
	{
		data->env_pwd = get_shell_env_by_name(cmd->sh, "PWD");
		data->env_old_pwd = get_shell_env_by_name(cmd->sh, "OLDPWD");
		data->env_home = get_shell_env_by_name(cmd->sh, "HOME");
		data->env_cdpath = get_shell_env_by_name(cmd->sh, "CDPATH");
		data->mode = posix_find_mode(cmd);
		data->hyphen = '\0';
		if (!(data->dest = posix_find_path(cmd)) && cmd->ret != 1)
			data->dest = lib_strdup(data->env_home);
		else if (lib_strcmp(data->dest, "-") == 0)
		{
			data->hyphen = '-';
			lib_strdel(&data->dest);
			if (data->env_old_pwd)
				data->dest = lib_strdup(data->env_old_pwd);
			else
				cmd->ret = 2;
		}
	}
}

static char	*posix_cd_step_5(t_cd_data *data)
{
	char		*ret;
	char		**tmp;
	int			i;
	struct stat	st;

	if (data)
	{
		tmp = lib_strsplit(data->env_cdpath, ':');
		i = 0;
		while (tmp[i])
		{
			if (tmp[i][lib_strlen(tmp[i]) - 1] != '/')
				tmp[i] = lib_strjoinfree(tmp[i], "/", 1);
			ret = lib_strjoin(tmp[i++], data->dest);
			stat(ret, &st);
			if (((st.st_mode & S_IFDIR) == S_IFDIR) && !access(ret, X_OK))
			{
				lib_arr_del(tmp);
				return (ret);
			}
			lib_strdel(&ret);
		}
		lib_arr_del(tmp);
	}
	return (NULL);
}

static int	bltn_cd_error_handling(t_cd_data *data, t_command *cmd)
{
	if (cmd && data)
	{
		if (cmd->ret == 1)
			lib_putendl_fd("42sh: cd: too many arguments", cmd->fd[2]);
		else if (cmd->ret == 2)
			lib_putendl_fd("42sh: cd: OLDPWD not set", cmd->fd[2]);
		else if (!data->env_home && !data->dest)
			lib_putendl_fd("42sh: cd: HOME not set", cmd->fd[2]);
		else
			return (0);
	}
	return (1);
}

static int	bltn_do_cd(t_cd_data *data, t_command *cmd)
{
	if (chdir(data->dest) == -1)
	{
		lib_putendl_fd("42sh: cd: invalid target", cmd->fd[2]);
		lib_strdel(&data->dest);
		return (1);
	}
	if (data->mode == 'P')
	{
		lib_strdel(&data->dest);
		data->dest = get_getcwd();
	}
	if (data->hyphen == '-' && cmd->ret != 2)
		lib_putendl_fd(data->dest, cmd->fd[1]);
	set_shell_env_by_name(cmd->sh, "OLDPWD", data->env_pwd);
	set_shell_env_by_name(cmd->sh, "PWD", data->dest);
	lib_strdel(&data->dest);
	return (0);
}

int			posix_bltn_cd(t_command *cmd)
{
	t_cd_data	data;
	char		*tmp_pwd;

	if (!(cmd && cmd->sh))
		return (1);
	posix_init_cd(&data, cmd);
	if (bltn_cd_error_handling(&data, cmd))
		return (1);
	if (!(data.dest[0] != '/') && !((!lib_strncmp(data.dest, "./", 2) ||
!lib_strcmp(data.dest, ".")) || (!lib_strncmp(data.dest, "../", 3) ||
!lib_strcmp(data.dest, ".."))) && (data.env_cdpath))
		data.dest = posix_cd_step_5(&data);
	if (data.dest[0] != '/' && data.mode == 'L')
	{
		tmp_pwd = lib_strdup(data.dest);
		lib_strdel(&data.dest);
		data.dest = follow_path_from(data.env_pwd, tmp_pwd);
		lib_strdel(&tmp_pwd);
	}
	return (bltn_do_cd(&data, cmd));
}
