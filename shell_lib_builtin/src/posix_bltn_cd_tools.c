/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   posix_bltn_cd_tools.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 14:24:30 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/08 20:56:23 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtin.h>

char	posix_find_mode(t_command *cmd)
{
	int		i;
	char	last;

	last = 'L';
	if (cmd && cmd->options)
	{
		i = 1;
		while (cmd->options[i])
		{
			if (lib_strcmp(cmd->options[i], "-P") == 0)
				last = 'P';
			else if (lib_strcmp(cmd->options[i], "-L") == 0)
				last = 'L';
			i++;
		}
	}
	return (last);
}

char	*posix_find_path(t_command *cmd)
{
	char	*ret;
	int		i;

	ret = NULL;
	if (cmd && cmd->options)
	{
		i = 1;
		while (cmd->options[i])
		{
			if (lib_strcmp(cmd->options[i], "-P") &&
					lib_strcmp(cmd->options[i], "-L"))
			{
				if (ret)
				{
					lib_strdel(&ret);
					cmd->ret = 1;
					return (NULL);
				}
				ret = lib_strdup(cmd->options[i]);
			}
			i++;
		}
	}
	return (ret);
}
