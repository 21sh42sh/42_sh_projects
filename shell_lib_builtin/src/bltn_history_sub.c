/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_history_sub.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/18 15:47:17 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

void	print_history(char **history, char **options, int fd)
{
	int	i;
	int	j;
	int	tmp;
	int	nb_width;

	j = lib_arr_len(history);
	i = (options && options[1]) ? j - lib_atoi(options[1]) : 0;
	if (i - j > 0)
	{
		lib_putstr_fd("42sh: history: ", fd);
		lib_putnbr_fd((i - j) * -1, fd);
		return (lib_putendl_fd(": invalid option", fd));
	}
	nb_width = get_nb_width(j);
	while (i < 0)
		i++;
	while (++i <= j && i >= 0 + (options[1] ? 1 : 0))
	{
		tmp = get_nb_width(i);
		while (tmp++ < nb_width + 4)
			write(1, " ", fd);
		lib_putnbr_fd(i, fd);
		lib_putstr_fd("  ", fd);
		lib_putendl_fd(history[j - i], fd);
	}
}

void	help(t_command *cmd)
{
	lib_putendl_fd("42sh: history: ", cmd->fd[1]);
	lib_putstr_fd(" --load: replace current history by", cmd->fd[1]);
	lib_putstr_fd(" the content of the file HIST_FILE", cmd->fd[1]);
	lib_putendl_fd(" or the file in argument.", cmd->fd[1]);
	lib_putstr_fd(" --save: save the current history in", cmd->fd[1]);
	lib_putstr_fd(" the file HIST_FILE", cmd->fd[1]);
	lib_putendl_fd(" or the file in argument.", cmd->fd[1]);
	lib_putstr_fd("-c	clear the history list by deleting", cmd->fd[1]);
	lib_putendl_fd(" all of the entries", cmd->fd[1]);
	lib_putstr_fd("-d offset	delete the history entry at", cmd->fd[1]);
	lib_putendl_fd(" position OFFSET.", cmd->fd[1]);
	lib_putstr_fd("-a	append history lines from this", cmd->fd[1]);
	lib_putendl_fd(" session to the history file", cmd->fd[1]);
	lib_putstr_fd("-n	read all history lines not already", cmd->fd[1]);
	lib_putendl_fd(" read from the history file and append", cmd->fd[1]);
	lib_putendl_fd(" them to the history list", cmd->fd[1]);
	lib_putstr_fd("-r	read the history file and append the co", cmd->fd[1]);
	lib_putendl_fd("ntents to the history list", cmd->fd[1]);
	lib_putstr_fd("-w	write the current history to the hist", cmd->fd[1]);
	lib_putendl_fd("ory file", cmd->fd[1]);
	lib_putstr_fd("-p	perform history expansion on each ARG", cmd->fd[1]);
	lib_putstr_fd(" and display the result without storing it ", cmd->fd[1]);
	lib_putendl_fd("in the history list", cmd->fd[1]);
	lib_putstr_fd("-s	append the ARGs to the history li", cmd->fd[1]);
	lib_putendl_fd("st as a single entry", cmd->fd[1]);
}

int		history_load(t_command *cmd)
{
	char	**new_history;

	if (cmd->options[2])
		new_history = history_get(cmd->sh, cmd->options[2]);
	else
		new_history = history_get(cmd->sh, NULL);
	if (new_history)
	{
		lib_arr_del(cmd->sh->history);
		cmd->sh->history = new_history;
	}
	return (0);
}

int		history_sub_save(t_command *cmd)
{
	if (cmd->options[2])
		history_save(cmd->sh->history, cmd->options[2]);
	else
		history_save(cmd->sh->history, NULL);
	return (0);
}

int		history_add_args(t_command *cmd)
{
	char	*new_line;
	int		i;

	new_line = NULL;
	i = 2;
	while (cmd->options[i])
	{
		if (!(new_line = lib_strjoinfree(new_line, cmd->options[i], 1)))
			return (1);
		if (cmd->options[i + 1])
			if (!(new_line = lib_strjoinfree(new_line, " ", 1)))
				return (1);
		i++;
	}
	if (!(cmd->sh->history = lib_arr_push_front(cmd->sh->history, new_line)))
	{
		lib_strdel(&new_line);
		return (1);
	}
	lib_arr_del_one(cmd->sh->history, 1);
	lib_strdel(&new_line);
	return (0);
}
