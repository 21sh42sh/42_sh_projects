/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_unsetenv.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/03 15:58:33 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/28 15:42:14 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell_wrap.h>

void		env_del_one(t_shell *sh, char *name)
{
	char	**ret;
	size_t	i;
	size_t	j;

	if (sh ? sh->env && name : 0)
	{
		i = 0;
		j = 0;
		name = lib_strjoin(name, "=");
		ret = (char **)lib_memalloc(
				sizeof(char *) * (lib_arr_len(sh->env) + 1));
		while (sh->env[i])
		{
			if (lib_strncmp(sh->env[i], name, lib_strlen(name)))
				ret[j++] = lib_strdup(sh->env[i]);
			i++;
		}
		lib_arr_del(sh->env);
		free(name);
		sh->env = ret;
	}
}

int			bltn_unsetenv(t_command *cmd)
{
	size_t i;

	if (cmd ? cmd->sh : NULL)
		if (cmd->options)
		{
			i = 0;
			while (cmd->options[++i])
				env_del_one(cmd->sh, cmd->options[i]);
			return (0);
		}
	return (-1);
}
