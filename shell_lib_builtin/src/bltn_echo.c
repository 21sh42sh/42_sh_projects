/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_echo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/27 18:45:46 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int			bltn_echo(t_command *cmd)
{
	int		i;
	int		option_n;

	option_n = 0;
	i = 1;
	if (cmd->options[i] && !lib_strcmp(cmd->options[i], "-n"))
	{
		option_n = 1;
		i++;
	}
	while (cmd->options[i])
	{
		if (i > (1 + option_n))
			lib_putstr_fd(" ", cmd->fd[1]);
		lib_putstr_fd(cmd->options[i++], cmd->fd[1]);
	}
	if (!option_n)
		lib_putendl_fd("", cmd->fd[1]);
	return (0);
}
