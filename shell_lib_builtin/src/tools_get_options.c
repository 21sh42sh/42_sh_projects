/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_get_options.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/16 19:07:15 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

static char	*path_finder(char *from, char *path)
{
	char	**arr_path;
	char	**arr_target;
	char	*target;
	int		i;

	arr_target = lib_strsplit(from, '/');
	arr_path = lib_strsplit(path, '/');
	i = 0;
	while (arr_path && arr_path[i])
	{
		if (!lib_strcmp(arr_path[i], ".."))
			lib_arr_del_one(arr_target, lib_arr_len(arr_target) - 1);
		else if (lib_strcmp(arr_path[i], "."))
			arr_target = lib_arr_push_back(arr_target, arr_path[i]);
		i++;
	}
	target = lib_arr_joini(arr_target, "/", 0);
	if (from && from[0] == '/')
		target = lib_strjoinfree("/", target, 2);
	lib_arr_del(arr_target);
	lib_arr_del(arr_path);
	return (target);
}

char		*follow_path_from(char *from, char *path)
{
	char	*target;

	if (!path)
		return (lib_strdup(from));
	if (path && path[0] == '/')
		return (lib_strdup(path));
	target = path_finder(from, path);
	return (target);
}

/*
** used by:
** ________ history
*/

int			get_nb_width(int nb)
{
	int	width;

	width = 0;
	if (nb < 0)
	{
		width++;
		nb *= -1;
	}
	while (nb > 0)
	{
		width++;
		nb /= 10;
	}
	return (width);
}
