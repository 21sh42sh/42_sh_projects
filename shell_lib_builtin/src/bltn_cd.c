/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_cd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/08 15:27:27 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

char		*get_getcwd(void)
{
	char	*buff;

	buff = lib_strnew(MAXPATHLEN);
	if (getcwd(buff, MAXPATHLEN))
		return (buff);
	lib_strdel(&buff);
	return (NULL);
}

static int	check_path(t_command *cmd, char *path)
{
	if (!path || !path[0] || access(path, F_OK))
	{
		lib_putstr_fd("cd no such file or directory: ", cmd->fd[2]);
		return (1);
	}
	if (access(path, X_OK))
	{
		lib_putstr_fd("cd: permission denied: ", cmd->fd[2]);
		return (1);
	}
	return (0);
}

static int	set_path(t_command *cmd, char *path)
{
	char	*tmp;

	if (check_path(cmd, path))
		return (1);
	tmp = get_getcwd();
	if (tmp && tmp[0])
		set_shell_env_by_name(cmd->sh, "OLDPWD", tmp);
	lib_strdel(&tmp);
	chdir(path);
	tmp = get_getcwd();
	if (tmp && tmp[0])
		set_shell_env_by_name(cmd->sh, "PWD", tmp);
	lib_strdel(&tmp);
	return (0);
}

static int	gear_path(t_command *cmd, char *path)
{
	char	*pwd;
	int		ret;

	ret = 1;
	if (path && path[0] == '/')
		ret = set_path(cmd, path);
	else
	{
		pwd = get_getcwd();
		if (pwd && pwd[0])
		{
			if (pwd[lib_strlen(pwd)] != '/')
				pwd = lib_strjoinfree(pwd, "/", 1);
			pwd = lib_strjoinfree(pwd, path, 1);
			ret = set_path(cmd, pwd);
			lib_strdel(&pwd);
		}
	}
	return (ret);
}

int			bltn_cd(t_command *cmd)
{
	char	**args;
	char	*home;
	int		ret;

	ret = 0;
	args = cmd->options;
	if (lib_arr_len(args) == 2 && args[1] && lib_strcmp(args[1], "-") == 0)
	{
		lib_strdel(&args[1]);
		args[1] = lib_strdup(get_shell_env_by_name(cmd->sh, "OLDPWD"));
		lib_putendl_fd(args[1], cmd->fd[1]);
	}
	home = get_shell_env_by_name(cmd->sh, "HOME");
	if (!args || !args[0] || !args[1])
		if (!home || !home[0])
			return (1);
		else
			ret = set_path(cmd, home);
	else
		ret = gear_path(cmd, args[1]);
	if (ret)
		lib_putendl_fd(args[1], cmd->fd[2]);
	return (ret);
}
