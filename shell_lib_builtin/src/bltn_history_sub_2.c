/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_history_sub_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/18 18:10:34 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int	history_del_n(t_command *cmd)
{
	int	i;
	int	len;

	len = lib_arr_len(cmd->sh->history);
	i = lib_atoi(cmd->options[2]);
	if (len - i > 0 && len - i < len)
	{
		if (1 == 0)
			lib_putendl(cmd->sh->history[len - i]);
		lib_arr_del_one(cmd->sh->history, len - i);
	}
	else
	{
		lib_putstr_fd("42sh: history: ", cmd->fd[2]);
		lib_putnbr_fd(i, cmd->fd[2]);
		lib_putendl_fd(": history position out of range", cmd->fd[2]);
		return (1);
	}
	return (0);
}

/*
** options:
** ________ flags:
** _______________ 1: only new element
** _______________ 0: common
*/

int	history_a(t_command *cmd, int flag)
{
	int	ret;

	ret = 0;
	if (cmd->options[2])
		ret = history_append(cmd->sh->history, cmd->options[2],
				((flag == 1) ? cmd->sh->history_cmd_nb : -1), cmd->fd[2]);
	else
	{
		if (flag == 1)
			cmd->sh->history_file_nb_line += cmd->sh->history_cmd_nb;
		ret = history_append(cmd->sh->history,
				get_shell_env_by_name(cmd->sh, "HIST_FILE"),
				((flag == 1) ? cmd->sh->history_cmd_nb : -1), cmd->fd[2]);
		if (flag == 1)
			cmd->sh->history_cmd_nb = 0;
	}
	return (ret);
}

/*
** options:
** ________ flags:
** _______________ 1: only new element
** _______________ 0: common
*/

int	history_w(t_command *cmd)
{
	int	ret;

	ret = 0;
	if (cmd->options[2])
		ret = history_write(cmd->sh->history, cmd->options[2], cmd->fd[2]);
	else
	{
		cmd->sh->history_file_nb_line += cmd->sh->history_cmd_nb;
		ret = history_write(cmd->sh->history,
				get_shell_env_by_name(cmd->sh, "HIST_FILE"), cmd->fd[2]);
		cmd->sh->history_cmd_nb = 0;
	}
	return (ret);
}

/*
** options:
** ________ flags:
** _______________ 1: only new element
** _______________ 0: common
*/

int	history_n(t_command *cmd, int flag)
{
	int	ret;

	if (cmd->options[2])
		history_retrieve(&(cmd->sh->history), cmd->options[2],
				((flag == 1) ? cmd->sh->history_file_nb_line : -1), cmd->fd[2]);
	else
	{
		ret = history_retrieve(&(cmd->sh->history),
				get_shell_env_by_name(cmd->sh, "HIST_FILE"),
				((flag == 1) ? cmd->sh->history_file_nb_line : -1), cmd->fd[2]);
		if (flag == 1)
			cmd->sh->history_file_nb_line += ret;
	}
	return (0);
}

int	history_p(t_command *cmd)
{
	int	i;

	i = 2;
	while (cmd->options[i])
		lib_putendl_fd(cmd->options[i++], cmd->fd[1]);
	lib_arr_del_one(cmd->sh->history, 0);
	return (0);
}
