/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_setenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/09/27 17:57:25 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell_wrap.h>

static int	check_setenv(char *s)
{
	if (s ? *s : 0)
		return (!lib_strchr(s + 1, '='));
	return (-1);
}

int			bltn_setenv(t_command *cmd)
{
	size_t	i;

	if (cmd ? cmd->sh : NULL)
	{
		i = 0;
		while (cmd->options[++i])
		{
			if (check_setenv(cmd->options[i]) != -1)
			{
				set_shell_env_by_cmd_line(cmd->sh, cmd->options[i]);
			}
		}
		return (0);
	}
	return (127);
}
