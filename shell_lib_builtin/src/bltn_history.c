/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_history.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayh           #+#    #+#             */
/*   Updated: 2016/11/21 12:52:15 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

/*
** check if a filename as arg, if not get the one from the env.
** options:
** ________ n: 0 to append all the history list.
*/

int			history_append(char **history, char *file_name, int n, int s_er)
{
	int		fd;
	int		i;

	if (!file_name)
	{
		lib_putendl_fd("42sh: history: Unable to get history file name", s_er);
		return (1);
	}
	fd = open(file_name, O_WRONLY | O_CREAT | O_APPEND, S_IWUSR | S_IRUSR);
	if (fd < 0)
	{
		lib_putendl_fd("42sh: history: Unable to open history file", s_er);
		return (1);
	}
	if (n < 0)
		i = lib_arr_len(history) - 1;
	else
		i = (n < (int)lib_arr_len(history)) ? n : lib_arr_len(history) - 1;
	while (i >= 0 && history[i])
		lib_putendl_fd(history[i--], fd);
	close(fd);
	return (0);
}

/*
** check if a filename as arg, if not get the one from the env.
** options:
** ________ n: 0 to append all the history list.
*/

int			history_write(char **history, char *file_name, int s_er)
{
	int		fd;
	int		i;

	if (!file_name)
	{
		lib_putendl_fd("42sh: history: Unable to get history file name", s_er);
		return (1);
	}
	fd = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, S_IWUSR | S_IRUSR);
	if (fd < 0)
	{
		lib_putendl_fd("42sh: history: Unable to open history file", s_er);
		return (1);
	}
	i = lib_arr_len(history);
	while (--i >= 0 && history[i])
		lib_putendl_fd(history[i], fd);
	close(fd);
	return (0);
}

/*
** check if a filename as arg, if not get the one from the env.
** options:
** ________ n: 0 to append all the history list.
*/

int			history_retrieve(char ***history, char *file_name, int n, int s_er)
{
	int		fd;
	int		i;
	char	*line;

	if (!file_name)
	{
		lib_putendl_fd("42sh: history: Unable to get history file name", s_er);
		return (1);
	}
	fd = open(file_name, O_RDONLY, S_IWUSR | S_IRUSR);
	if (fd < 0)
	{
		lib_putendl_fd("42sh: history: Unable to open history file", s_er);
		return (1);
	}
	i = 0;
	while (get_next_line(fd, &line) > 0)
	{
		if (i++ >= n)
			*history = lib_arr_push_front(*history, line);
		lib_strdel(&line);
	}
	close(fd);
	return ((i < n) ? 0 : i - n);
}

static int	history_c(t_command *cmd)
{
	lib_arr_del(cmd->sh->history);
	if (!(cmd->sh->history = lib_arr_new(0)))
		return (1);
	return (0);
}

int			bltn_history(t_command *cmd)
{
	if (!lib_strcmp(cmd->options[1], "--load"))
		return (history_load(cmd));
	else if (!lib_strcmp(cmd->options[1], "--save"))
		return (history_sub_save(cmd));
	else if (!lib_strcmp(cmd->options[1], "--help"))
		help(cmd);
	else if (!lib_strcmp(cmd->options[1], "-c"))
		history_c(cmd);
	else if (!lib_strcmp(cmd->options[1], "-w"))
		return (history_w(cmd));
	else if (!lib_strcmp(cmd->options[1], "-a"))
		return (history_a(cmd, 1));
	else if (!lib_strcmp(cmd->options[1], "-r"))
		history_n(cmd, 1);
	else if (!lib_strcmp(cmd->options[1], "-d"))
		return (history_del_n(cmd));
	else if (!lib_strcmp(cmd->options[1], "-s"))
		return (history_add_args(cmd));
	else if (!lib_strcmp(cmd->options[1], "-p"))
		return (history_p(cmd));
	else
		print_history(cmd->sh->history, cmd->options, cmd->fd[1]);
	return (0);
}
