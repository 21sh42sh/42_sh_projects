/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/23 19:10:41 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

/*
** BUILTIN ENV
** env [OPTION]... [NAME=VALUE]... [COMMAND [ARGS]...]
** OPTIONS:
** ________ i use an empty env
** ________ u unsetenv
** How to use:
** ___________ env -i [-u NAME]... [NAME=VALUE]... [COMMAND [ARGS]...]
** ___________ env [-u NAME]... [NAME=VALUE]... [COMMAND [ARGS]...]
*/

static t_command	*make_cmd(t_shell *sh, char **options, int fd)
{
	t_command	*cmd;

	if ((cmd = lib_memalloc(sizeof(t_command))))
	{
		cmd->options = lib_arr_dup(options);
		cmd->sh = sh;
		cmd->fd[1] = fd;
		return (cmd);
	}
	return (NULL);
}

static int			bltn_env_exec(t_command *cmd, int i)
{
	t_command	*sh_cmd;
	int			ret;

	sh_cmd = cmd->sh->cmds;
	if (!(cmd->sh->cmds = make_cmd(cmd->sh, cmd->options + i, 1)))
		return (1);
	ret = exec_shell_cmds(cmd->sh);
	del_all_cmds(cmd->sh->cmds);
	cmd->sh->cmds = sh_cmd;
	cmd->sh->exit_status = ret;
	cmd->ret = ret;
	return (ret);
}

static t_shell		*apply_unset(t_command *cmd, t_shell *sh_cp, int *i)
{
	while (cmd->options && cmd->options[*i])
	{
		if (!lib_strcmp(cmd->options[*i], "-u")
			&& cmd->options[*i + 1]
			&& cmd->options[*i + 1][0])
		{
			env_del_one(sh_cp, cmd->options[*i + 1]);
			*i += 2;
		}
		else
			break ;
	}
	return (sh_cp);
}

static int			bltn_env_return(int i, t_shell *sh_cp, t_command *cmd)
{
	t_shell	*cmd_sh;

	cmd_sh = cmd->sh;
	cmd->sh = sh_cp;
	if (cmd->options && cmd->options[i])
	{
		i = bltn_env_exec(cmd, i);
	}
	else
	{
		lib_arr_putstr_str_fd((const char **)sh_cp->env, "\n", cmd->fd[1]);
		i = 0;
	}
	del_shell(sh_cp);
	cmd->sh = cmd_sh;
	return (i);
}

int					bltn_env(t_command *cmd)
{
	t_shell		*sh_cp;
	int			i;

	i = 1;
	sh_cp = (t_shell *)lib_memalloc(sizeof(t_shell));
	if (cmd->options && cmd->options[i] && !lib_strcmp(cmd->options[i], "-i"))
	{
		i++;
		sh_cp->env = lib_arr_new(0);
	}
	else
		sh_cp->env = lib_arr_dup(cmd->sh->env);
	apply_unset(cmd, sh_cp, &i);
	while (cmd->options && cmd->options[i] && lib_strchr(cmd->options[i], '='))
		set_shell_env_by_cmd_line(sh_cp, cmd->options[i++]);
	return (bltn_env_return(i, sh_cp, cmd));
}
