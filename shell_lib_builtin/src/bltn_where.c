/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_where.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 21:32:21 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "libft.h"
#include <sys/stat.h>

static int	finder(t_command *cmd, char **path, int a, char *find)
{
	size_t		i;
	int			ret;
	char		*check;
	struct stat	st;

	check = NULL;
	ret = 1;
	i = -1;
	while (path[++i])
	{
		check = lib_strjoinfree(lib_strjoinfree(path[i], "/", 0), find, 1);
		if (stat(check, &st) != -1)
		{
			lib_putendl_fd(path[i], cmd->fd[1]);
			ret = 0;
			if (!a)
				break ;
		}
		lib_strdel(&check);
	}
	lib_strdel(&check);
	return (ret);
}

int			bltn_where(t_command *cmd)
{
	int		ret;
	char	*find;
	char	**path;
	int		a;

	if (!cmd || !cmd->options || !cmd->options || !cmd->sh || !cmd->sh->env
			|| lib_strcmp(cmd->options[0], "where") || !cmd->options[1])
		return (1);
	a = 1;
	find = cmd->options[1];
	ret = 1;
	if ((path = lib_strsplit(get_shell_env_by_name(cmd->sh, "PATH"), ':')))
	{
		ret = finder(cmd, path, a, find);
		lib_arr_del(path);
		if (ret == 0)
			return (0);
	}
	lib_putstr_fd(find, cmd->fd[2]);
	lib_putendl_fd(" not found.", cmd->fd[2]);
	return (ret);
}
