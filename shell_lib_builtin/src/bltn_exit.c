/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bltn_exit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/09/29 18:24:08 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int			bltn_exit(t_command *cmd)
{
	int	status;

	status = lib_arr_len(cmd->options);
	if (status == 1)
		status = 0;
	else if (status == 2)
		status = lib_atoi(cmd->options[1]);
	else
	{
		lib_putendl_fd("exit: too many arguments", cmd->fd[1]);
		return (1);
	}
	del_shell(cmd->sh);
	exit(status);
	return (0);
}
