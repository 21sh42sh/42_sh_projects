/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   posix_bltn_env.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/08 15:02:55 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell_wrap.h>
#include <builtin.h>
#include <stdio.h>

static	t_command	*make_cmd(t_shell *sh, char **options, int fd)
{
	t_command *cmd;

	if ((cmd = lib_memalloc(sizeof(t_command))))
	{
		cmd->options = lib_arr_dup(options);
		cmd->sh = sh;
		cmd->fd[1] = fd;
		return (cmd);
	}
	return (NULL);
}

static	int			posix_bltn_env_exec(t_command *cmd, int i)
{
	t_command	*sh_cmd;
	int			ret;

	sh_cmd = cmd->sh->cmds;
	if (!(cmd->sh->cmds = make_cmd(cmd->sh, cmd->options + i, 1)))
		return (1);
	ret = exec_shell_cmds(cmd->sh);
	del_all_cmds(cmd->sh->cmds);
	cmd->sh->cmds = sh_cmd;
	cmd->sh->exit_status = ret;
	cmd->ret = ret;
	printf("ret = %d\n", ret);
	return (ret);
}

static void			posix_bltn_env_next(t_command *cmd, int i, int *ret,
	char **old_env)
{
	if (cmd->options[i])
		*ret = posix_bltn_env_exec(cmd, i);
	else
	{
		lib_arr_putstr_str_fd((const char **)cmd->sh->env, "\n", cmd->fd[1]);
		*ret = 0;
	}
	lib_arr_del(cmd->sh->env);
	cmd->sh->env = old_env;
}

int					posix_bltn_env(t_command *cmd)
{
	char	**old_env;
	int		i;
	int		ret;

	ret = -1;
	if (cmd)
	{
		old_env = cmd->sh->env;
		if (!lib_strcmp(cmd->options[1], "-i"))
			cmd->sh->env = NULL;
		else
			cmd->sh->env = lib_arr_dup(cmd->sh->env);
		i = 1 + (cmd->sh->env == NULL);
		while (cmd->options[i] && lib_strchr(cmd->options[i], '='))
		{
			set_shell_env_by_cmd_line(cmd->sh, cmd->options[i]);
			i++;
		}
		posix_bltn_env_next(cmd, i, &ret, old_env);
		lib_arr_del(cmd->sh->env);
		cmd->sh->env = old_env;
	}
	return (ret);
}
