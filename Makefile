#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/03 19:15:09 by hlequien          #+#    #+#              #
#    Updated: 2016/10/25 17:16:45 by hlequien         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC = gcc
CFLAGS = -Wall -Wextra -Werror           
SRC_PATH = ./shell/src/
OBJ_PATH = ./shell/obj/
SRC_NAME = main.c
OBJ_NAME = $(SRC_NAME:.c=.o)
INC_PATH = ./shell/inc/\
./libft/inc/\
./shell_lib_builtin/inc/\
./shell_lib_cmd_line/inc/\
./shell_lib_operator/inc/\
./shell_lib_parser/inc/\
./shell_wrap/inc/\
./shell_lib_bang/inc/
LIB_PATH = ./libft\
./shell_lib_cmd_line\
./shell_lib_builtin\
./shell_lib_parser\
./shell_lib_operator\
./shell_lib_bang\
./shell_wrap
LIB_TARGET = ./libft/libft.a\
./shell_lib_cmd_line/libcmd_line.a\
./shell_lib_builtin/lib_builtin.a\
./shell_lib_parser/lib_parser.a\
./shell_lib_operator/lib_operator.a\
./shell_wrap/libshell_wrap.a

LIB_NAME = -lft\
-l_builtin\
-l_parser\
-l_operator\
-lshell_wrap\
-lcmd_line\
-ltermcap\
-l_bang
NAME = 42sh

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_PATH))
INC = $(addprefix -I,$(INC_PATH))

all: lib_make $(NAME)

$(NAME):$(OBJ) $(LIB_TARGET)
	$(CC) $(CFLAGS) $(OBJ) $(LIB) $(LIB_NAME) $(INC) -o $(NAME)

$(OBJ_PATH)%.o:$(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean_shell:
	rm -rfv $(OBJ_PATH)

fclean_shell: clean_shell
	rm -fv $(NAME)

clean: clean_shell clean_lib

fclean: fclean_shell fclean_lib

re: fclean all

lib_make:
	for lib in $(LIB_PATH); do \
	make -C $$lib -j 8;\
	done

clean_lib:
	for lib in $(LIB_PATH); do \
	make clean -C $$lib  -j 8;\
	done

fclean_lib:
	for lib in $(LIB_PATH); do \
	make fclean -C $$lib  -j 8;\
	done

relib:
	for lib in $(LIB_PATH); do \
	make re -C $$lib  -j 8;\
	done

