/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_not_last_word.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/15 17:39:45 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** exemple of pattern:
** __ !foo
** __ !foo:p
*/

static void	lib_strndup_cleared_sub(char *str, char **tmp, int i, int n)
{
	int		quote_s;
	int		quote_d;
	int		tilde;
	int		j;

	quote_s = 0;
	quote_d = 0;
	tilde = 0;
	j = 0;
	while (str && str[i] && i < n && j < n)
	{
		if (str[i] == 34)
			quote_d++;
		else if (str[i] == 39)
			quote_s++;
		else if (str[i] == 96)
			tilde++;
		if ((!is_even(quote_s) || !is_even(quote_d) || !is_even(tilde))
		|| (lib_isspace(str[i]) && str[i + 1] && !lib_isspace(str[i + 1]))
		|| (!lib_isspace(str[i])))
			(*tmp)[j++] = str[i];
		i++;
	}
	(*tmp)[j] = 0;
}

static char	*lib_strndup_cleared(char *str, int n)
{
	char	*tmp;
	char	*new_str;
	int		i;

	if (!str)
		return (NULL);
	if (n < 0 || n > (int)lib_strlen(str))
		n = lib_strlen(str);
	i = 0;
	while (str && str[i] && lib_isspace(*(str + i)))
		i++;
	if (!(tmp = lib_strnew(n + 1)))
		return (NULL);
	lib_strndup_cleared_sub(str, &tmp, i, n);
	new_str = lib_strdup(tmp);
	lib_strdel(&tmp);
	return (new_str);
}

static int	get_word_beginning(char *history, int i)
{
	int		quote_s;
	int		quote_d;
	int		tilde;

	quote_s = 0;
	quote_d = 0;
	tilde = 0;
	while (history && history[i])
	{
		if (history[i] == 34)
			quote_d++;
		if (history[i] == 39)
			quote_s++;
		if (history[i] == 96)
			tilde++;
		if ((is_even(quote_s) && is_even(quote_d) && is_even(tilde))
		&& (lib_isspace(history[i])))
			return (i + 1);
		if ((is_even(quote_s) && is_even(quote_d) && is_even(tilde))
		&& ((history[i] == '|' || history[i] == ';')
				|| (is_delimiter(history + i))))
			return (i);
		i++;
	}
	return (-1);
}

static char	*find_word(char **history)
{
	char	*word;
	int		i;

	word = NULL;
	if (!history)
		return (NULL);
	i = 0;
	while (history && history[1] && i >= 0 && history[1][i]
			&& lib_isspace(history[1][i]))
		i++;
	if ((i = get_word_beginning(history[1], i)) >= 0)
		if (!(word = lib_strndup_cleared(history[1] + i, -1)))
			return (NULL);
	return (word);
}

char		*bang_not_last_word(char **history, char *pattern)
{
	char	*word;

	bang_return_status(0, "error");
	if (!history || !pattern)
		return (NULL);
	if (pattern && !lib_strcmp(pattern, "!*:p"))
		bang_return_status(0, "print");
	else if (pattern && !lib_strcmp(pattern, "!*"))
		bang_return_status(0, "run");
	else
		return (NULL);
	word = NULL;
	if (!(word = find_word(history)))
		bang_return_status(0, "error");
	return (word);
}
