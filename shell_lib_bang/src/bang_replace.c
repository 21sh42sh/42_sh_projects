/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_replace.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/15 20:10:31 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <bang.h>

char	*bang_replace(char **history, char *pattern)
{
	char *ret;
	char *tmp[2];
	char *search;

	ret = NULL;
	if (pattern && history && pattern[0] == '^')
	{
		if ((tmp[0] = lib_strchr(pattern + 1, '^')))
		{
			if (tmp[0] == &(pattern[1]))
				return (NULL);
			search = lib_strndup(pattern + 1, tmp[0] - pattern - 1);
			if ((ret = lib_strstr(history[1], search)))
			{
				ret = lib_strdup(history[1]);
				tmp[1] = lib_str_replace(ret, search, tmp[0] + 1);
				ret = tmp[1];
				lib_strdel(&search);
			}
		}
	}
	return (ret);
}
