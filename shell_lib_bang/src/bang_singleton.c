/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_singleton.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/14 18:05:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** contain the exit status of the bang
** status != 0 will save status as the return value.
** common:
** _______ NULL (and status == 0) retrieve ret status
** _______ "error" => 0
** _______ "run" => 0
** _______ "print" => 1
** _______ "not found" => 2
*/

int			get_status(char *msg, int ret)
{
	if (!lib_strcmp(msg, "error"))
		return (-1);
	if (!lib_strcmp(msg, "run"))
		return (0);
	if (!lib_strcmp(msg, "print"))
		return (1);
	if (!lib_strcmp(msg, "not found"))
		return (2);
	return (ret);
}

char		*print_status(int status)
{
	if (status == -1)
		return ("error");
	if (status == 0)
		return ("run");
	if (status == 1)
		return ("print");
	if (status == 2)
		return ("not found");
	return ("unknown status");
}

int			bang_return_status(int status, char *common)
{
	static int	ret;

	if (!common && status == 0)
		return (ret);
	if (status != 0)
		ret = status;
	ret = get_status(common, ret);
	return (ret);
}

/*
** Boolean
** return:
** _______ 1 if the return status is set to your expectation
** _______ 0 otherwise
*/

int			is_bang_exit_status(char *status)
{
	int test;
	int ret;

	test = get_status(status, -2);
	ret = bang_return_status(0, NULL);
	return (ret == test);
}
