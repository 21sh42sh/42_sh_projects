/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_bang.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by hlequien          #+#    #+#             */
/*   Updated: 2016/11/15 20:10:54 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <bang.h>

char	*bang_double_bang(char **history, char *pattern)
{
	char *ret;

	ret = NULL;
	if (history && pattern)
	{
		if (lib_strcmp(pattern, "!!") == 0 && history[1])
			ret = lib_strdup(history[1]);
		else if (lib_strncmp(pattern, "!!", 2) == 0 && history[1])
			ret = lib_strjoin(history[1], pattern + 2);
	}
	return (ret);
}
