/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/15 21:43:02 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

int	is_even(int nb)
{
	if (nb == ((nb / 2) * 2))
		return (1);
	return (0);
}

int	is_delimiter(char *str)
{
	return (!lib_strncmp(str, "||", 2)
			|| !lib_strncmp(str, "&&", 2)
			|| !lib_strncmp(str, "|", 1)
			|| !lib_strncmp(str, ";", 1));
}

int	lib_is_number(char *str)
{
	if (!str)
		return (0);
	if (*str == '-')
		str += 1;
	while (str && *str)
	{
		if (!lib_isdigit(*str))
			return (0);
		str += 1;
	}
	return (1);
}
