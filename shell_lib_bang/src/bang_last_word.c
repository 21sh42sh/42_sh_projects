/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_last_word.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/16 16:52:08 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** exemple of pattern:
** __ !foo
** __ !foo:p
*/

static int	get_word_beginning(char *history, int i)
{
	int		quote_s;
	int		quote_d;
	int		tilde;

	quote_s = 0;
	quote_d = 0;
	tilde = 0;
	while (history && i > 0 && history[i])
	{
		if (history[i] == 34)
			quote_d++;
		else if (history[i] == 39)
			quote_s++;
		else if (history[i] == 96)
			tilde++;
		if ((is_even(quote_s) && is_even(quote_d) && is_even(tilde))
		&& ((lib_isspace(history[i]))
				|| (history[i] == '|' || history[i] == ';')))
			return (i + 1);
		if ((is_even(quote_s) && is_even(quote_d) && is_even(tilde))
		&& (is_delimiter(history + i)))
			return (i + 2);
		i--;
	}
	return ((i == 0) ? 0 : -1);
}

static char	*find_word(char **history)
{
	char	*word;
	int		end;
	int		i;

	word = NULL;
	if (!history)
		return (NULL);
	i = lib_strlen(history[1]) - 1;
	while (history && history[1] && i >= 0 && history[1][i]
			&& lib_isspace(history[1][i]))
		i--;
	end = i;
	if ((i = get_word_beginning(history[1], i)) >= 0)
		if (!(word = lib_strndup(history[1] + i,
		((i == 0) ? end + 1 : end))))
			return (NULL);
	return (word);
}

char		*bang_last_word(char **history, char *pattern)
{
	char	*word;

	bang_return_status(0, "error");
	if (!history || !pattern)
		return (NULL);
	if (pattern && !lib_strcmp(pattern, "!$:p"))
		bang_return_status(0, "print");
	else if (pattern && !lib_strcmp(pattern, "!$"))
		bang_return_status(0, "run");
	else
		return (NULL);
	word = NULL;
	if (!(word = find_word(history)))
		bang_return_status(0, "error");
	return (word);
}
