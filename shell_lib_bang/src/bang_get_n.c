/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_get_n.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/17 15:44:54 by hlequien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** exemple of pattern:
** __ !-n
** __ !-n:p
** __ !n
** __ !n:p
*/

static int	extract_n(char *pattern)
{
	char	*look_up;
	int		n;

	look_up = NULL;
	if (lib_strlen(pattern) > 2
			&& !lib_strcmp(pattern + lib_strlen(pattern) - 2, ":p"))
	{
		if (!(look_up = lib_strndup(pattern + 1, lib_strlen(pattern) - 3)))
			return (0);
		bang_return_status(0, "print");
	}
	else
	{
		if (!(look_up = lib_strdup(pattern + 1)))
			return (0);
		bang_return_status(0, "run");
	}
	n = lib_atoi(look_up);
	lib_strdel(&look_up);
	return (n);
}

/*
** n is negative
*/

static char	*find_last_n_cmd(char **history, int n)
{
	char	*cmd;

	cmd = NULL;
	n = n * -1;
	if (n > 0 && history[n] && n < (int)lib_arr_len(history)
			&& !(cmd = lib_strdup(history[n])))
	{
		return (NULL);
		bang_return_status(0, "error");
	}
	return (cmd);
}

/*
** n is positive
*/

static char	*find_nth_cmd(char **history, int n)
{
	char	*cmd;

	cmd = NULL;
	n = lib_arr_len(history) - n;
	if (n > 0 && history[n] && n < (int)lib_arr_len(history)
			&& !(cmd = lib_strdup(history[n])))
	{
		return (NULL);
		bang_return_status(0, "error");
	}
	return (cmd);
}

char		*bang_get_nth(char **history, char *pattern)
{
	char	*cmd;
	int		n;

	bang_return_status(0, "error");
	if (!history || !pattern)
		return (NULL);
	if (!(n = extract_n(pattern)))
	{
		bang_return_status(0, "not found");
		return (NULL);
	}
	cmd = NULL;
	if (n < 0)
	{
		if (!(cmd = find_last_n_cmd(history, n)))
			bang_return_status(0, "not found");
	}
	else
	{
		if (!(cmd = find_nth_cmd(history, n)))
			bang_return_status(0, "not found");
	}
	return (cmd);
}
