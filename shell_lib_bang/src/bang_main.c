/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_main.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/15 20:09:33 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** exemple of pattern:
** __ !!
** __ !foo
** __ !foo:print
** __ ...
*/

char	*bang(char **history, char *pattern)
{
	if (!history || !pattern)
		return (NULL);
	if (!lib_strncmp(pattern, "!!", 2))
		return (bang_double_bang(history, pattern));
	if (!lib_strncmp(pattern, "!$", 2))
		return (bang_last_word(history, pattern));
	if (!lib_strncmp(pattern, "!*", 2))
		return (bang_not_last_word(history, pattern));
	if (!lib_strncmp(pattern, "^", 1))
		return (bang_replace(history, pattern));
	if (lib_is_number(pattern + 1))
		return (bang_get_nth(history, pattern));
	return (bang_last_with(history, pattern));
}
