/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang_last_with.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/15 15:02:27 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bang.h"

/*
** exemple of pattern:
** __ !foo
** __ !foo:p
*/

static char	*extract_look_up(char *pattern)
{
	char	*look_up;

	look_up = NULL;
	if (lib_strlen(pattern) > 2
			&& !lib_strcmp(pattern + lib_strlen(pattern) - 2, ":p"))
	{
		if (!(look_up = lib_strndup(pattern + 1, lib_strlen(pattern) - 3)))
			return (NULL);
		bang_return_status(0, "print");
	}
	else
	{
		if (!(look_up = lib_strdup(pattern + 1)))
			return (NULL);
		bang_return_status(0, "run");
	}
	return (look_up);
}

static char	*find_cmd(char **history, char *look_up, int look_up_len)
{
	char	*cmd;

	cmd = NULL;
	if (history)
		history += 1;
	while (history && *history)
	{
		if (!lib_strncmp(look_up, *history, look_up_len))
		{
			cmd = lib_strdup(*history);
			break ;
		}
		history += 1;
	}
	return (cmd);
}

char		*bang_last_with(char **history, char *pattern)
{
	char	*cmd;
	char	*look_up;
	int		look_up_len;

	bang_return_status(0, "error");
	if (!history || !pattern)
		return (NULL);
	if (!(look_up = extract_look_up(pattern)))
	{
		bang_return_status(0, "error");
		return (NULL);
	}
	look_up_len = lib_strlen(look_up);
	if (!(cmd = find_cmd(history, look_up, look_up_len)))
		bang_return_status(0, "not found");
	lib_strdel(&look_up);
	return (cmd);
}
