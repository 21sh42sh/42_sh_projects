/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bang.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 16:38:12 by jsebayhi          #+#    #+#             */
/*   Updated: 2016/11/22 15:32:14 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BANG_H
# define BANG_H

# include "libft.h"

/*
** Bang
*/

int		bang_return_status(int status, char *common);
int		is_bang_exit_status(char *status);
int		get_status(char *msg, int ret);
char	*print_status(int status);
char	*bang(char **history, char *pattern);
char	*bang_last_with(char **history, char *pattern);
char	*bang_last_word(char **history, char *pattern);
char	*bang_not_last_word(char **history, char *pattern);
char	*double_bang(char	**history, char *pattern);
char	*bang_get_nth(char **history, char *pattern);
char	*bang_double_bang(char	**history, char *pattern);
char	*bang_replace(char **history, char *pattern);

/*
** tools
*/

int		is_even(int nb);
int		is_delimiter(char *str);
int		lib_is_number(char *str);

#endif
