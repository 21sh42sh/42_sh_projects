# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    change_name.sh                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/25 12:46:32 by hlequien          #+#    #+#              #
#    Updated: 2016/09/27 19:34:42 by hlequien         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


# $1 : filename
# $2 : username
function replace_mail(){
	HLEQUIEN_MAIL="\/\*   By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+        \*\/"
	JSEBAYHI_MAIL="\/\*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        \*\/"
	MAFAGOT_MAIL="\/\*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        \*\/"

	if [ $2 = "mafagot " ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*\$/$MAFAGOT_MAIL/g" $1
	elif [ $2 = "hlequien" ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*$/$HLEQUIEN_MAIL/g" $1
	elif [ $2 = "jsebayhi" ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*\$/$JSEBAYHI_MAIL/g" $1
	else
		echo ERROR 1>&2
		exit 255
	fi
	return 0;
}

function replace_mail_makefile(){
	HLEQUIEN_MAIL="#    By: hlequien <hlequien@student.42.fr>          +#+  +:+       +#+         #"
	JSEBAYHI_MAIL="#    By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+         #"
	MAFAGOT_MAIL="#    By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+         #"

	if [ $2 = "mafagot " ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*\$/$MAFAGOT_MAIL/g" $1
	elif [ $2 = "hlequien" ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*$/$HLEQUIEN_MAIL/g" $1
	elif [ $2 = "jsebayhi" ];then
		sed -i.bak -E "s/^.*By: [a-z]{7,8}.*\$/$JSEBAYHI_MAIL/g" $1
	else
		echo ERROR 1&>2
		exit 255
	fi
	return 0;
}

function replace_update(){
	DATE_STR=$(date "+%Y\/%m\/%d %H:%M:%S")
	sed -i.dat -E "s/(^.*Updated: )[0-9]{4}(\/[0-9]{2}){2} ([0-9]{2}\:){2}[0-9]{2}(.*\$)/\1$DATE_STR\4/g"  $1
}

NAME=$(whoami)
echo $NAME > auteur
if [ $NAME = "mafagot" ]
then
	NAME="mafagot "
fi

# .[CH]

FILE_LIST=$(git ls-files | grep "\.[ch]$")

for i in $FILE_LIST;do
	echo Moma $i
	replace_mail $i $NAME
	replace_update $i
	sed -i.bak -E "s/by ([a-z]{7} |[a-z]{8})/by $NAME/g" $i
done

# MAKEFILE

FILE_LIST=$(git ls-files | grep "Makefile$")

for i in $FILE_LIST;do
	echo Moma $i
	replace_mail_makefile $i $NAME
	replace_update $i
	sed -i.bak -E "s/by ([a-z]{7} |[a-z]{8})/by $NAME/g" $i
done

#norminette $FILE_LIST
